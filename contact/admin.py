from django.contrib import admin
from models import NewsletterSubscriber, UsersContactingCapego

admin.site.register(NewsletterSubscriber)
admin.site.register(UsersContactingCapego)
