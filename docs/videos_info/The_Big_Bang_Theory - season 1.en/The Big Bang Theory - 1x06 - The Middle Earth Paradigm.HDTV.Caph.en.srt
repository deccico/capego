1
00:00:01,300 --> 00:00:03,800
Okay, if no one else was...

2
00:00:03,800 --> 00:00:07,200
We really sucked at paintball

3
00:00:07,200 --> 00:00:09,200
Oh, that was
absolutely humiliating

4
00:00:09,200 --> 00:00:12,300
Oh, come on. Some battles you win,
some battles you lose.

5
00:00:12,300 --> 00:00:18,100
Yes, but but you don't have to lose
to Kyle Burnsteen Barmistva party.

6
00:00:18,100 --> 00:00:19,600
I think we have to acknowledge,

7
00:00:19,600 --> 00:00:24,000
those was some fairly savage
preadolescent jews.

8
00:00:25,600 --> 00:00:28,200
No, we were annihilated
by our own incompetence

9
00:00:28,200 --> 00:00:31,300
and the inability of some people
to follow the chain of command.

10
00:00:31,300 --> 00:00:32,900
Sheldon, let it go!

11
00:00:32,900 --> 00:00:37,500
No, I want to talk about the fact
that Wolowitz shot me in the ck.

12
00:00:37,800 --> 00:00:39,100
I shot you for good reason.

13
00:00:39,100 --> 00:00:42,300
You were leading us into disaster.

14
00:00:42,300 --> 00:00:44,800
I was giving clear, concise orders.

15
00:00:44,800 --> 00:00:47,200
You hid behind a tree yelling,
"Get the kid in the yarmulke!

16
00:00:47,200 --> 00:00:49,400
Get the kid in the yarmulke!"

17
00:00:49,400 --> 00:00:50,600
Oh, hey, guys.

18
00:00:50,600 --> 00:00:53,500
- Oh, hey, Penny. Hello.
- Morning, ma'am.

19
00:00:53,500 --> 00:00:55,900
So, how was paintball?
Did you have fun?

20
00:00:55,900 --> 00:01:00,200
Sure, if you consider being fragged
by your own troops fun.

21
00:01:00,500 --> 00:01:04,400
You clear space on your calendar--
there will be an inquiry.

22
00:01:04,400 --> 00:01:05,800
Okay.

23
00:01:05,800 --> 00:01:07,000
Um, hey, I'm having a party on Saturday, 

24
00:01:07,000 --> 00:01:08,700
so if you guys are around,
you should come on by.

25
00:01:08,700 --> 00:01:09,900
- A party?
- Yeah.

26
00:01:09,900 --> 00:01:13,500
A... "boy-girl" party?

27
00:01:14,300 --> 00:01:18,300
Well, there will be boys and there will
be girls and it is a party, so...

28
00:01:18,300 --> 00:01:19,400
It'll just be a bunch of my friends.

29
00:01:19,400 --> 00:01:21,400
We'll have some beer, do a little dancing.

30
00:01:21,400 --> 00:01:22,300
Dancing?

31
00:01:22,300 --> 00:01:23,700
Yeah, I don't know, Penny...

32
00:01:23,700 --> 00:01:24,700
The thing is, we're not...

33
00:01:24,700 --> 00:01:28,100
- No, we're really more of a... -No.

34
00:01:28,100 --> 00:01:30,200
But thanks.Thanks for thinking of us.

35
00:01:30,200 --> 00:01:32,300
Are you sure? Come on, it's Halloween.

36
00:01:32,300 --> 00:01:33,700
A Halloween party?

37
00:01:33,700 --> 00:01:35,300
As in... costumes?

38
00:01:35,300 --> 00:01:38,300
Well, yeah.

39
00:01:45,600 --> 00:01:47,100
Is there a theme?

40
00:01:47,100 --> 00:01:49,500
Um, yeah, Halloween.

41
00:01:49,500 --> 00:01:53,000
Yes, but are the costumes
random, or genre-specific?

42
00:01:53,000 --> 00:01:55,500
As usual, I'm not following.

43
00:01:55,500 --> 00:01:59,800
He's asking if we can come as
anyone from science fiction, fantasy...

44
00:01:59,800 --> 00:02:01,200
- Sure.
- What about comic books?

45
00:02:01,200 --> 00:02:02,200
- Fine.
- Anime?

46
00:02:02,200 --> 00:02:03,600
- Of course.
- TV, film, 

47
00:02:03,600 --> 00:02:06,000
D- and-D, manga, Greek gods,
Roman gods, Norse gods--

48
00:02:06,000 --> 00:02:08,000
Anything you want! Okay?

49
00:02:08,000 --> 00:02:11,100
Any costume you want.

50
00:02:11,100 --> 00:02:13,500
Bye.

51
00:02:14,500 --> 00:02:17,100
Gentlemen, to the sewing machines.

52
00:02:44,500 --> 00:02:46,600
I'll get it.

53
00:03:01,000 --> 00:03:03,700
Oh, no.

54
00:03:03,900 --> 00:03:06,300
Oh, no.

55
00:03:06,300 --> 00:03:10,000
Make way for the fastest man alive!

56
00:03:10,000 --> 00:03:11,300
Oh, no.

57
00:03:11,300 --> 00:03:14,200
See, this is why I wanted to
have a costume meeting.

58
00:03:14,200 --> 00:03:17,200
We all have other costumes:
We can change.

59
00:03:17,200 --> 00:03:19,800
Or we could walk right
behind each other all night

60
00:03:19,800 --> 00:03:23,400
and look like one person
going really fast.

61
00:03:29,800 --> 00:03:31,300
No, no, no.

62
00:03:31,300 --> 00:03:35,700
It's a boy-girl party, this flash runs solo.

63
00:03:36,000 --> 00:03:37,100
Okay, how about this?

64
00:03:37,100 --> 00:03:38,300
Nobody gets to be The Flash.

65
00:03:38,300 --> 00:03:41,100
We all change. Agreed?

66
00:03:41,100 --> 00:03:43,500
Agreed.

67
00:03:44,300 --> 00:03:45,800
I call Frodo!

68
00:03:45,800 --> 00:03:48,200
Damn!

69
00:03:53,900 --> 00:03:55,200
Hey.

70
00:03:57,400 --> 00:04:01,700
Sorry I'm late, but my hammer got
stuck in the door on the bus.

71
00:04:02,000 --> 00:04:03,000
You went with Thor?

72
00:04:03,000 --> 00:04:06,800
What, just because I'm Indian
I can't be a Norse god?

73
00:04:06,800 --> 00:04:10,000
"No, no, Raj has to be an Indian god."

74
00:04:10,000 --> 00:04:11,400
That's racism.

75
00:04:11,400 --> 00:04:13,500
I mean look at Wolowitz.He's not English, 

76
00:04:13,500 --> 00:04:16,300
but he's dressed like Peter Pan.

77
00:04:16,700 --> 00:04:18,400
Sheldon is neither sound nor light, 

78
00:04:18,400 --> 00:04:21,400
but he's obviously the Doppler effect.

79
00:04:25,100 --> 00:04:26,300
I'm not Peter Pan.

80
00:04:26,300 --> 00:04:28,100
I'm Robin Hood.

81
00:04:28,100 --> 00:04:29,000
Really?

82
00:04:29,000 --> 00:04:33,300
Because I sawPeter Pan, and
you're dressed exactly like Cathy Rigby.

83
00:04:33,800 --> 00:04:37,800
She was a little bigger than you,
but it's basically the same look, man.

84
00:04:38,600 --> 00:04:41,500
Hey, Sheldon, there's something
I want to talk to you about before
we go to the party.

85
00:04:41,500 --> 00:04:43,100
I don't care if anybody gets it.

86
00:04:43,100 --> 00:04:45,800
I'm going as the Doppler effect.

87
00:04:46,400 --> 00:04:49,700
- No, it's not that.
- If I have to, I can demonstrate.

88
00:04:53,700 --> 00:04:55,700
Terrific.

89
00:04:56,700 --> 00:05:02,400
this party is my first chance for Penny
to see me in the context of her social group, 

90
00:05:02,400 --> 00:05:05,700
and I need you not to
embarrass me tonight.

91
00:05:06,400 --> 00:05:09,300
Well, what exactly do you
mean by embarrass you?

92
00:05:09,300 --> 00:05:11,400
Well, for example, tonight, 

93
00:05:11,400 --> 00:05:14,700
no one needs to know that
my middle name is Leakey.

94
00:05:14,700 --> 00:05:16,800
But there's nothing embarrassing about that.

95
00:05:16,800 --> 00:05:18,600
Your father worked with Louis Leakey, 

96
00:05:18,600 --> 00:05:20,200
a great anthropologist.

97
00:05:20,200 --> 00:05:22,900
It had nothing to do with
your bed-wetting.

98
00:05:24,300 --> 00:05:28,200
All m saying is that this party is the
perfect opportunity for Penny

99
00:05:28,200 --> 00:05:30,600
to see me as a member of her peer group, 

100
00:05:30,600 --> 00:05:35,200
a potential close friend, and perhaps more, 

101
00:05:35,200 --> 00:05:38,300
and I don't want to look like a dork.

102
00:05:48,100 --> 00:05:49,100
Just a heads up, fellas.

103
00:05:49,100 --> 00:05:53,100
If anyone gets lucky, I've got a dozen
condoms in my quiver.

104
00:05:54,500 --> 00:05:56,300
Oh, hey, guys.

105
00:05:56,300 --> 00:05:58,000
Hey. Sorry we're late.

106
00:05:58,000 --> 00:05:58,900
Late?

107
00:05:58,900 --> 00:06:00,100
It's 7:05.

108
00:06:00,100 --> 00:06:02,300
And you said the party starts at 7:00.

109
00:06:02,300 --> 00:06:04,400
Well, yeah, I mean, when you
start a party at 7:00, 

110
00:06:04,400 --> 00:06:08,100
no one shows up at, you know, 7:00.

111
00:06:08,600 --> 00:06:11,200
It's 7:05.

112
00:06:11,200 --> 00:06:13,000
Yes. Yes, it is.

113
00:06:13,000 --> 00:06:15,800
Okay. Well, um, come on in.

114
00:06:23,300 --> 00:06:26,200
So, what, are all the girls in the bathroom?

115
00:06:26,200 --> 00:06:29,500
Probably, but in their own homes.

116
00:06:30,000 --> 00:06:33,000
So, what time does the costume parade start?

117
00:06:33,500 --> 00:06:34,900
The parade?

118
00:06:34,900 --> 00:06:38,000
Yeah, so the judges can give out the
prizes for best costume.

119
00:06:38,000 --> 00:06:39,900
You know, most frightening,
most authentic, 

120
00:06:39,900 --> 00:06:43,700
most accurate visualization of
a scientific principal.

121
00:06:45,400 --> 00:06:46,900
Oh, Sheldon, I'm sorry, 

122
00:06:46,900 --> 00:06:48,300
but there aren't going to be any parades

123
00:06:48,300 --> 00:06:51,800
or judges or prizes.

124
00:06:52,300 --> 00:06:54,800
This party is just going to suck.

125
00:06:55,000 --> 00:06:56,900
No! Come on, it's going to be fun, 

126
00:06:56,900 --> 00:06:58,000
and you all look great.

127
00:06:58,000 --> 00:06:59,600
I mean, look at you, Thor, 

128
00:06:59,600 --> 00:07:01,000
and, oh, Peter Pan.

129
00:07:01,000 --> 00:07:03,300
That's so cute.

130
00:07:03,300 --> 00:07:06,400
- Actually, Penny, he's Robin Hood.
- I'm Peter Pan.

131
00:07:07,200 --> 00:07:11,400
And I got a handful of pixie dust
with your name on it.

132
00:07:12,200 --> 00:07:14,500
No, you don't.

133
00:07:15,900 --> 00:07:17,500
Uh, hey, what's Sheldon supposed to be?

134
00:07:17,500 --> 00:07:19,800
Oh, he's the Doppler effect.

135
00:07:19,800 --> 00:07:22,700
Yes. It's the apparent change in
the frequency of a wave

136
00:07:22,700 --> 00:07:27,200
caused by relative motion between
the source of the wave and the observer.

137
00:07:28,400 --> 00:07:30,100
Oh, sure, I see it now.

138
00:07:30,100 --> 00:07:32,300
The Doppler effect.

139
00:07:32,300 --> 00:07:33,200
All right, I got to shower.

140
00:07:33,200 --> 00:07:36,400
You guys, um, make yourselves comfortable.

141
00:07:36,400 --> 00:07:38,100
Okay.

142
00:07:39,000 --> 00:07:39,800
See?

143
00:07:39,800 --> 00:07:42,300
People get it.

144
00:07:55,000 --> 00:07:58,800
By Odin's beard, this is good Chex mix.

145
00:07:58,800 --> 00:08:00,400
No, thanks. Peanuts.

146
00:08:00,400 --> 00:08:03,500
I can't afford to swell up in these tights.

147
00:08:03,500 --> 00:08:04,900
I'm confused.

148
00:08:04,900 --> 00:08:08,500
If there's no costume parade,
what are we doing here?

149
00:08:08,500 --> 00:08:11,800
We're socializing, meeting new people.

150
00:08:11,800 --> 00:08:14,400
Telepathically?

151
00:08:14,400 --> 00:08:17,100
Oh, hey, when did you get here?

152
00:08:17,100 --> 00:08:18,100
Hi.

153
00:08:18,100 --> 00:08:20,900
Penny is wearing the worst Catwoman
costume I've ever seen.

154
00:08:20,900 --> 00:08:23,300
And that includes Halle Berry's.

155
00:08:23,300 --> 00:08:26,200
She's not Catwoman.
She's just a generic cat.

156
00:08:26,200 --> 00:08:28,200
And that's the kind of
sloppy costuming

157
00:08:28,200 --> 00:08:32,100
which results from a lack of
rules and competion.

158
00:08:33,800 --> 00:08:36,300
Hey, guys, check out the sexy nurse.

159
00:08:36,300 --> 00:08:41,600
I believe it's time for me to turn
my head and cough.

160
00:08:42,400 --> 00:08:43,600
What is your move?

161
00:08:43,600 --> 00:08:45,800
I'm going to use the mirror technique.

162
00:08:45,800 --> 00:08:47,800
She brushes her hair back, I brush my hair back...

163
00:08:47,800 --> 00:08:48,800
She shrugs, I shrug.

164
00:08:48,800 --> 00:08:52,600
Subconsciously she's thinking,
"We're in sync. We belong together."

165
00:08:52,600 --> 00:08:55,000
Where do you get this stuff?

166
00:08:55,000 --> 00:08:57,900
You know, psychology journals,
Internet research, 

167
00:08:57,900 --> 00:09:02,700
and there's this great show on VH-1
about how to pick up girls.

168
00:09:04,400 --> 00:09:06,600
Oh, if only I had his confidence.

169
00:09:06,600 --> 00:09:10,600
I have such difficulty speaking to
women, or around women...

170
00:09:10,600 --> 00:09:14,700
or at times even effeminate men.

171
00:09:14,700 --> 00:09:16,400
If that's a working stethoscope, 

172
00:09:16,400 --> 00:09:20,200
maybe you'd like to hear my
heart skip a beat?

173
00:09:20,200 --> 00:09:21,500
No, thanks.

174
00:09:21,500 --> 00:09:25,500
No, seriously, you can.I have transient
idiopathic arrhythmia.

175
00:09:31,800 --> 00:09:33,400
I want to get to know Penny's friends, 

176
00:09:33,400 --> 00:09:37,300
I just don't know how to talk to these people.

177
00:09:37,300 --> 00:09:40,500
Well, I actually might be able to help.

178
00:09:40,500 --> 00:09:41,600
How so?

179
00:09:41,600 --> 00:09:44,100
Like Jane Goodall observing the apes, 

180
00:09:44,100 --> 00:09:47,800
I initially saw their interactions as confusing
and unstructured.

181
00:09:47,800 --> 00:09:49,200
But patterns emerge.

182
00:09:49,200 --> 00:09:53,200
They have their own language, if you will.

183
00:09:53,500 --> 00:09:55,100
Go on.

184
00:09:55,100 --> 00:09:59,400
Well, it seems that the newcomer
approaches the existing group
with the greeting, 

185
00:09:59,400 --> 00:10:01,300
"How wasted am I?"

186
00:10:01,300 --> 00:10:05,300
Which is met with an approving
chorus of "Dude."

187
00:10:05,800 --> 00:10:07,200
Then what happens?

188
00:10:07,200 --> 00:10:09,900
That's as far as I've gotten.

189
00:10:10,200 --> 00:10:11,700
This is ridiculous.

190
00:10:11,700 --> 00:10:13,000
I'm jumping in.

191
00:10:13,000 --> 00:10:13,900
Good luck.

192
00:10:13,900 --> 00:10:15,400
No, you're coming with me.

193
00:10:15,400 --> 00:10:17,800
Oh, I hardly think so.

194
00:10:17,800 --> 00:10:19,300
Oh, come on.

195
00:10:19,300 --> 00:10:21,100
Aren't you afraid I'll embarrass you?

196
00:10:21,100 --> 00:10:23,700
Yes; but I need a wing man.

197
00:10:23,700 --> 00:10:26,600
All right, but if we're going to use flight metaphors, 

198
00:10:26,600 --> 00:10:28,200
I'm much more suited to being the guy

199
00:10:28,200 --> 00:10:31,800
from the FAA analyzing wreckage.

200
00:10:34,100 --> 00:10:35,000
Hi.

201
00:10:35,000 --> 00:10:36,600
Hi. Hello.

202
00:10:36,600 --> 00:10:38,600
So what are you supposed to be?

203
00:10:38,600 --> 00:10:42,100
Me? I'll give you a hint.

204
00:10:45,100 --> 00:10:46,900
A choo-choo train?

205
00:10:46,900 --> 00:10:48,700
Close!

206
00:10:50,900 --> 00:10:54,200
A brain damaged choo-choo train?

207
00:10:55,300 --> 00:10:59,500
How wasted am I?

208
00:11:11,000 --> 00:11:12,500
I still don't get it.

209
00:11:12,500 --> 00:11:14,500
I'm the Doppler effect.

210
00:11:14,500 --> 00:11:16,600
Okay, if that's some
sort of learning disability, 

211
00:11:16,600 --> 00:11:19,500
I think it's very insensitive.

212
00:11:20,400 --> 00:11:23,800
Why don't you just tell
people you're a zebra?

213
00:11:23,800 --> 00:11:27,100
Well, why don't you just tell people
you're one of the seven dwarves?

214
00:11:27,100 --> 00:11:28,300
Because I'm Frodo.

215
00:11:28,300 --> 00:11:31,600
Yes, well, I'm the Doppler effect.

216
00:11:32,300 --> 00:11:34,700
- Oh, no.
- What?

217
00:11:34,700 --> 00:11:36,600
That's Penny's ex-boyfriend.

218
00:11:36,600 --> 00:11:38,600
What do you suppose he's doing here?

219
00:11:38,600 --> 00:11:42,200
Besides disrupting the local gravity field.

220
00:11:42,700 --> 00:11:45,600
If he were any bigger, he'd
have moons orbiting him.

221
00:11:45,600 --> 00:11:48,400
Oh, snap.

222
00:11:48,700 --> 00:11:50,900
So, I guess we'll be leaving now.

223
00:11:50,900 --> 00:11:52,800
Why should we leave?

224
00:11:52,800 --> 00:11:57,600
For all we know he crashed the
party and Penny doesn't even want him here.

225
00:11:58,200 --> 00:12:00,700
You have a backup hypothesis?

226
00:12:00,700 --> 00:12:03,200
Maybe they want to be friends.

227
00:12:03,200 --> 00:12:07,100
Or maybe she wants to be friends
and he wants something more.

228
00:12:07,100 --> 00:12:09,300
Then he and I are on equal ground.

229
00:12:09,300 --> 00:12:12,600
Yes, but you're much closer to it than he is.

230
00:12:13,700 --> 00:12:16,300
Look, if this was 1, 500 years ago, 

231
00:12:16,300 --> 00:12:18,300
by virtue of his size and strength, 

232
00:12:18,300 --> 00:12:20,800
Kurt would be entitled to his choice
of female partners.

233
00:12:20,800 --> 00:12:22,700
And male partners, animal partners, 

234
00:12:22,700 --> 00:12:24,100
large primordial eggplants--

235
00:12:24,100 --> 00:12:26,800
pretty much whatever tickled his fancy.

236
00:12:27,200 --> 00:12:30,000
Yes, but our society has
undergone a paradigm shift.

237
00:12:30,000 --> 00:12:32,000
In the Information Age, Sheldon, 

238
00:12:32,000 --> 00:12:34,500
you and I are the alpha males.

239
00:12:34,500 --> 00:12:36,300
We shouldn't have to back down.

240
00:12:36,300 --> 00:12:37,200
True.

241
00:12:37,200 --> 00:12:40,400
Why don't you text him that and
see if he backs down?

242
00:12:41,000 --> 00:12:42,700
No.

243
00:12:43,600 --> 00:12:46,900
I'm going to assert my
dominance face-to-face.

244
00:12:46,900 --> 00:12:49,300
Face-to-face? Are you going to
wait for him to sit down, 

245
00:12:49,300 --> 00:12:52,100
or are you going to stand on the coffee table?

246
00:12:53,300 --> 00:12:54,400
Hello, Penny.

247
00:12:54,400 --> 00:12:55,300
Hello, Kurt.

248
00:12:55,300 --> 00:12:57,400
Oh, hey, guys, are you having a good time?

249
00:12:57,400 --> 00:12:58,600
Given the reaction to my costume, 

250
00:12:58,600 --> 00:13:03,200
this party is a scathing indictment of
the American education system.

251
00:13:03,200 --> 00:13:06,000
What, you're a zebra, right?

252
00:13:06,000 --> 00:13:08,900
Yet another child left behind.

253
00:13:09,200 --> 00:13:11,400
And what are you supposed to be, an elf?

254
00:13:11,400 --> 00:13:12,900
No, I'm a hobbit.

255
00:13:12,900 --> 00:13:14,500
What's the difference?

256
00:13:14,500 --> 00:13:17,400
A hobbit is a mortal halfling
inhabitant of Middle Earth, 

257
00:13:17,400 --> 00:13:21,000
whereas an elf is an immortal, tall warrior.

258
00:13:21,000 --> 00:13:24,100
So why the hell would you want to be a hobbit?

259
00:13:24,100 --> 00:13:26,100
Because he's neither tall nor immortal

260
00:13:26,100 --> 00:13:28,700
and none of us could be The Flash.

261
00:13:29,300 --> 00:13:30,300
Well, whatever.

262
00:13:30,300 --> 00:13:32,300
Why don't you go hop off on a quest?

263
00:13:32,300 --> 00:13:33,800
I'm talking to Penny here.

264
00:13:33,800 --> 00:13:35,700
I think we're all talking to Penny here.

265
00:13:35,700 --> 00:13:38,300
I'm not. No offense.

266
00:13:38,300 --> 00:13:40,000
Okay, maybe you didn't hear me.

267
00:13:40,000 --> 00:13:42,200
- Go away.
- All right, Kurt, be nice.

268
00:13:42,200 --> 00:13:45,000
Oh, I am being nice.

269
00:13:45,000 --> 00:13:46,700
Right, little buddy?

270
00:13:46,700 --> 00:13:48,900
- Kurt.
- Okay.

271
00:13:49,300 --> 00:13:53,200
I understand your impulse to try to
physically intimidate me.

272
00:13:53,200 --> 00:13:55,800
I mean, you can't compete with
me on an intellectual level

273
00:13:55,800 --> 00:13:58,700
so you're driven to animalistic puffery.

274
00:13:58,700 --> 00:14:01,200
You calling me a puffy animal?

275
00:14:01,200 --> 00:14:03,200
Of course not. No, he's not.

276
00:14:03,200 --> 00:14:05,500
You're not, right, Leonard?

277
00:14:05,500 --> 00:14:08,200
No, I said "animalistic."

278
00:14:08,200 --> 00:14:09,800
Of course we're all animals, 

279
00:14:09,800 --> 00:14:13,300
but some of us have climbed a
little higher on the evolutionary tree.

280
00:14:13,300 --> 00:14:15,600
If he understands that, you're in trouble.

281
00:14:15,600 --> 00:14:17,700
So, what, I'm unevolved?

282
00:14:17,700 --> 00:14:19,900
You're in trouble.

283
00:14:19,900 --> 00:14:23,200
You know, you use a lot of big words
for such a little dwarf.

284
00:14:23,200 --> 00:14:24,900
Okay, Kurt, please.

285
00:14:24,900 --> 00:14:28,000
Penny, it's okay. I can handle this.

286
00:14:28,000 --> 00:14:29,700
I am not a dwarf, 

287
00:14:29,700 --> 00:14:31,400
I'm a hobbit.

288
00:14:31,400 --> 00:14:32,700
A hobbit.

289
00:14:32,700 --> 00:14:35,600
Are misfiring neurons in your
hippocampus preventing the conversion

290
00:14:35,600 --> 00:14:39,100
from short-term to long-term memory?

291
00:14:40,500 --> 00:14:43,700
Okay, now you're starting to make me mad.

292
00:14:43,700 --> 00:14:48,000
A homo habilis discovering his
opposable thumbs says what?

293
00:14:48,500 --> 00:14:50,200
What?

294
00:14:54,400 --> 00:14:55,900
I think I've made my point.

295
00:14:55,900 --> 00:14:59,300
Yeah? How about I make a point out of
your pointy little head?

296
00:14:59,300 --> 00:15:02,000
Let me remind you, while my
moral support is absolute, 

297
00:15:02,000 --> 00:15:05,200
in a physical confrontation,
I will be less than useless.

298
00:15:05,200 --> 00:15:07,300
There's not going to be a confrontation.

299
00:15:07,300 --> 00:15:11,600
In fact, I doubt if he can even
spell "confrontation."

300
00:15:13,100 --> 00:15:16,400
C- O-N...

301
00:15:16,400 --> 00:15:18,700
frontation!

302
00:15:18,700 --> 00:15:20,600
Kurt, put him down this instant!

303
00:15:20,600 --> 00:15:21,600
He started it!

304
00:15:21,600 --> 00:15:24,800
I don't care. I'm finishing it.Put him down!

305
00:15:24,800 --> 00:15:26,800
Fine.

306
00:15:27,900 --> 00:15:30,800
You're one lucky little leprechaun.

307
00:15:31,900 --> 00:15:33,800
He's a hobbit!

308
00:15:33,800 --> 00:15:36,100
I got your back.

309
00:15:36,500 --> 00:15:37,600
Leonard, are you okay?

310
00:15:37,600 --> 00:15:40,000
Yeah, I'm fine.

311
00:15:42,500 --> 00:15:44,400
It's good, it's a good party, thanks for having us.

312
00:15:44,400 --> 00:15:46,500
It's just getting a little late, so...

313
00:15:46,500 --> 00:15:48,100
Oh, okay.

314
00:15:48,100 --> 00:15:50,800
All right, well, thank you for coming.

315
00:15:53,000 --> 00:15:56,000
Happy Halloween.

316
00:15:58,100 --> 00:15:59,200
If it's any consolation, 

317
00:15:59,200 --> 00:16:03,500
I thought that homo habilis line
really put him in his place.

318
00:16:11,900 --> 00:16:13,200
What's that?

319
00:16:13,200 --> 00:16:15,000
Tea.

320
00:16:15,400 --> 00:16:16,500
When people are upset, 

321
00:16:16,500 --> 00:16:20,500
the cultural convention is to
bring them hot beverages.

322
00:16:26,200 --> 00:16:29,000
There, there.

323
00:16:30,800 --> 00:16:32,200
You want to talk about it?

324
00:16:32,200 --> 00:16:34,400
- No.
- Good.

325
00:16:35,000 --> 00:16:38,300
"There, there" was really all I had.

326
00:16:38,700 --> 00:16:39,800
Good night, Sheldon.

327
00:16:39,800 --> 00:16:41,800
Good night, Leonard.

328
00:16:48,000 --> 00:16:49,200
Hey, Leonard?

329
00:16:49,200 --> 00:16:50,500
Hi, Penny.

330
00:16:50,500 --> 00:16:52,500
Hey, I just wanted to make
sure you were okay.

331
00:16:52,500 --> 00:16:55,100
I'm fine.

332
00:16:55,800 --> 00:16:58,400
I am so sorry about what happened.

333
00:16:58,400 --> 00:17:00,200
It's not your fault.

334
00:17:00,200 --> 00:17:01,400
Yes, it is.

335
00:17:01,400 --> 00:17:02,800
That's why I broke up with him.

336
00:17:02,800 --> 00:17:06,000
He always does stuff like that.

337
00:17:06,000 --> 00:17:09,300
So why was he at your party?

338
00:17:09,900 --> 00:17:17,100
Well, I ran into him last week and he
was just all apologetic about how he's changed.

339
00:17:17,100 --> 00:17:19,400
And he was just going on and on and...

340
00:17:19,400 --> 00:17:21,100
I believed him and I'm an idiot

341
00:17:21,100 --> 00:17:24,100
because I always believe guys like that.

342
00:17:24,100 --> 00:17:26,500
And I can't go back to my party
because he's there.

343
00:17:26,500 --> 00:17:28,200
And know you don't want to
hear this and I'm upset

344
00:17:28,200 --> 00:17:31,900
and I'm really drunk and I just want to...

345
00:17:40,000 --> 00:17:42,700
There, there.

346
00:17:45,300 --> 00:17:47,100
God, what is wrong with me?

347
00:17:47,100 --> 00:17:48,700
Nothing, you're perfect.

348
00:17:49,100 --> 00:17:50,300
I'm not perfect.

349
00:17:50,300 --> 00:17:52,700
Yes, you are.

350
00:17:54,500 --> 00:17:58,000
You really think so, don't you?

351
00:18:10,600 --> 00:18:11,900
Penny?

352
00:18:11,900 --> 00:18:14,000
Yeah?

353
00:18:14,000 --> 00:18:17,500
How much have you had to drink tonight?

354
00:18:18,700 --> 00:18:22,500
Just... a lot.

355
00:18:22,800 --> 00:18:26,900
Are you sure that your being drunk
and your being angry with Kurt

356
00:18:26,900 --> 00:18:30,900
doesn't have something to do
with what's going on here?

357
00:18:31,100 --> 00:18:34,300
It might.

358
00:18:35,500 --> 00:18:37,400
Boy, you're really smart.

359
00:18:37,400 --> 00:18:41,100
Yeah, I'm a frickin' genius.

360
00:18:44,700 --> 00:18:46,700
Leonard, you are so great.

361
00:18:46,700 --> 00:18:49,700
Why can't all guys be like you?

362
00:18:49,700 --> 00:18:51,200
Because if all guys were like me, 

363
00:18:51,200 --> 00:18:54,900
the human race couldn't survive.

364
00:18:58,000 --> 00:18:59,800
I should probably go.

365
00:18:59,800 --> 00:19:02,400
Probably.

366
00:19:10,200 --> 00:19:12,700
Thank you.

367
00:19:22,000 --> 00:19:25,400
That's right, you saw what you saw.

368
00:19:26,600 --> 00:19:30,100
That's how we roll in the Shire.

369
00:19:37,600 --> 00:19:39,600
Coming.

370
00:19:44,000 --> 00:19:46,700
Hey, have you seen Koothrappali?

371
00:19:46,700 --> 00:19:48,400
He's not here.

372
00:19:48,400 --> 00:19:52,100
Maybe the Avengers summoned him.

373
00:19:52,100 --> 00:19:55,300
He's not the Marvel Comics Thor,
he's the original Norse god.

374
00:19:55,300 --> 00:19:57,800
Thank you for the clarification.

375
00:19:57,800 --> 00:19:59,100
I'm supposed to give him a ride home.

376
00:19:59,100 --> 00:20:00,700
Well, I'm sure he'll be fine.

377
00:20:00,700 --> 00:20:04,100
He has his hammer.

378
00:20:10,100 --> 00:20:14,500
I have to say, you are an amazing man.

379
00:20:14,500 --> 00:20:18,000
You're gentle, and passionate.

380
00:20:18,000 --> 00:20:22,900
And, my God, you are such a good listener.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
