1
00:00:10,627 --> 00:00:14,461
All right, just a few more feet and...

2
00:00:14,631 --> 00:00:17,862
Here we are, gentlemen,
the Gates of Elzebub.

3
00:00:18,334 --> 00:00:19,528
Good Lord.
Oh.

4
00:00:19,703 --> 00:00:22,934
Don't panic. This is what the last
97 hours have been about.

5
00:00:23,106 --> 00:00:24,130
Stay frosty.

6
00:00:24,307 --> 00:00:28,835
There's a horde of armed goblins
on that gate guarding the Sword of Azeroth.

7
00:00:29,012 --> 00:00:32,106
Warriors, unsheathe your weapons.
Magic wielders, raise your wands.

8
00:00:32,282 --> 00:00:33,374
Lock and load.

9
00:00:33,550 --> 00:00:35,711
Raj, blow the gates.

10
00:00:35,885 --> 00:00:37,113
Blowing the gates.

11
00:00:37,787 --> 00:00:41,814
Control, shift B.

12
00:00:43,993 --> 00:00:46,757
- Oh, my God, so many goblins.
Aah!

13
00:00:46,930 --> 00:00:48,989
Don't just stand there. Slash and move.

14
00:00:49,165 --> 00:00:50,325
Stay in formation.

15
00:00:50,500 --> 00:00:54,095
- Leonard, you got one on your tail.
- That's all right, my tail's prehensile.

16
00:00:54,270 --> 00:00:55,430
I got him, Leonard.

17
00:00:55,605 --> 00:00:59,302
Tonight, I spice my meat
with goblin blood.

18
00:00:59,476 --> 00:01:01,307
Raj, it's a trap. They're flanking us.

19
00:01:01,478 --> 00:01:03,036
- Oh, he's got me.
Oh!

20
00:01:03,213 --> 00:01:05,704
Sheldon, he's got Raj.
Use your sleep spell.

21
00:01:06,116 --> 00:01:07,640
Sheldon. Sheldon.

22
00:01:07,817 --> 00:01:11,878
I've got the Sword of Azeroth.

23
00:01:12,055 --> 00:01:15,115
- Forget the sword, Sheldon, help Raj.
- There is no more Sheldon.

24
00:01:15,291 --> 00:01:17,486
I am the sword master.

25
00:01:17,660 --> 00:01:20,094
- Leonard, look out.
- Damn it, man, we're dying here.

26
00:01:20,263 --> 00:01:21,662
Goodbye, peasants.

27
00:01:23,032 --> 00:01:24,158
The bastard teleported.

28
00:01:26,002 --> 00:01:29,267
He's selling the Sword of Azeroth
on eBay.

29
00:01:29,773 --> 00:01:32,298
You betrayed us for money?
Who are you?

30
00:01:32,475 --> 00:01:33,737
I'm a rogue knight elf.

31
00:01:33,910 --> 00:01:36,401
Don't you people
read character descriptions?

32
00:01:36,579 --> 00:01:39,047
Wait, wait, wait.
Somebody just clicked "Buy It Now. "

33
00:01:39,215 --> 00:01:42,446
I am the sword master.

34
00:02:07,377 --> 00:02:08,776
Whoo! I'm all sweaty.

35
00:02:08,945 --> 00:02:11,038
Wanna log on to Second Life
and go swimming?

36
00:02:11,214 --> 00:02:13,512
I just built a virtual pool.

37
00:02:13,817 --> 00:02:17,082
No. I can't look at you
or your avatar right now.

38
00:02:21,191 --> 00:02:23,352
Sounds like your neighbor's home.

39
00:02:24,727 --> 00:02:26,285
Excuse me.

40
00:02:26,462 --> 00:02:29,863
Don't forget the mail you took
so you'd have an excuse to talk to her.

41
00:02:30,033 --> 00:02:32,365
Oh, right, right, right.

42
00:02:32,535 --> 00:02:35,971
Stealing snail mail, very old school.
I like it.

43
00:02:36,773 --> 00:02:40,641
Penny, the mailman did it again. He...
Oh, sorry.

44
00:02:40,810 --> 00:02:42,641
Um, no, hi, Leonard. This is Doug.

45
00:02:42,812 --> 00:02:46,009
- Doug, this is my neighbor, Leonard.
- What's up, bro?

46
00:02:47,784 --> 00:02:49,649
Not much.

47
00:02:51,187 --> 00:02:52,711
Bro. Heh.

48
00:02:53,256 --> 00:02:54,484
Is everything okay?

49
00:02:54,657 --> 00:02:56,955
Yeah, no. I just...
I got your mail again, here.

50
00:02:57,126 --> 00:02:59,651
Oh, thank you.
I've got to talk to that mailman.

51
00:02:59,829 --> 00:03:02,059
Oh, no, that's probably
not such a good idea.

52
00:03:03,132 --> 00:03:08,069
Civil servants have a documented
propensity to, you know, snap, so...

53
00:03:08,872 --> 00:03:10,863
Okay. Well, thank you again.

54
00:03:11,040 --> 00:03:13,031
No problem. Bye.

55
00:03:13,409 --> 00:03:16,469
Oh, and bye, bro. Heh.

56
00:03:23,753 --> 00:03:25,983
Penny for your thoughts.

57
00:03:27,156 --> 00:03:30,023
- What's the matter?
- No, I'm fine.

58
00:03:30,193 --> 00:03:33,026
Penny's fine,
the guy she's kissing is really fine.

59
00:03:34,130 --> 00:03:37,497
Kissing? What kind of kissing?
Cheeks, lips, chase, French?

60
00:03:39,202 --> 00:03:41,193
What is wrong with you?

61
00:03:42,105 --> 00:03:43,834
I'm a romantic.

62
00:03:45,642 --> 00:03:47,803
Don't tell me
that your hopeless infatuation...

63
00:03:47,977 --> 00:03:49,877
...is devolving into pointless jealousy.

64
00:03:50,046 --> 00:03:52,913
Look, I'm not jealous.
I'm just a little concerned for her.

65
00:03:53,082 --> 00:03:55,141
Didn't like the look of the guy
she was with.

66
00:03:55,318 --> 00:03:57,115
Because he looks better than you?

67
00:03:58,288 --> 00:03:59,812
Yeah.

68
00:04:00,556 --> 00:04:02,353
He was kind of dreamy.

69
00:04:02,992 --> 00:04:05,085
At least now
you can retrieve the black box...

70
00:04:05,261 --> 00:04:08,719
...from the smoldering wreckage
that was once your fantasy of dating her...

71
00:04:08,898 --> 00:04:12,766
...and analyze the data so that
you don't crash into geek mountain again.

72
00:04:14,070 --> 00:04:18,131
I disagree. Love is not a sprint.
It's a marathon.

73
00:04:18,308 --> 00:04:23,245
A relentless pursuit that only ends
when she falls into your arms...

74
00:04:23,413 --> 00:04:26,382
...or hits you with the pepper spray.

75
00:04:26,983 --> 00:04:28,245
No, I'm done with Penny.

76
00:04:28,851 --> 00:04:31,786
I'm gonna be more realistic
and go after someone my own speed.

77
00:04:31,955 --> 00:04:34,014
- Like who?
- I don't know.

78
00:04:34,190 --> 00:04:35,214
Olivia Geiger?

79
00:04:35,391 --> 00:04:39,259
The dietitian at the cafeteria
with the limp and the lazy eye?

80
00:04:40,863 --> 00:04:43,331
- Yeah.
- Well, I don't think you have a shot there.

81
00:04:44,834 --> 00:04:47,962
I have noticed that Leslie Winkle
recently started shaving her legs.

82
00:04:48,137 --> 00:04:51,197
Now, given that winter is coming,
one can assume she's signaling...

83
00:04:51,374 --> 00:04:52,568
...sexual availability.

84
00:04:53,476 --> 00:04:55,467
I don't know.
You guys work in the same lab.

85
00:04:55,845 --> 00:04:57,938
- So?
- There are pitfalls.

86
00:04:58,114 --> 00:04:59,604
Trust me, I know.

87
00:04:59,782 --> 00:05:02,910
When it comes
to sexual-harassment law...

88
00:05:03,086 --> 00:05:06,055
...I'm a bit of a self-taught expert.

89
00:05:07,590 --> 00:05:10,923
Howard, if I were to ask Leslie Winkle out,
it would just be for dinner.

90
00:05:11,094 --> 00:05:14,928
I'm not gonna walk into the lab,
ask her to strip naked and dance for me.

91
00:05:15,098 --> 00:05:17,362
Oh, then you're probably okay.

92
00:05:21,070 --> 00:05:23,038
- Hello, Leslie.
- Hi, Leonard.

93
00:05:23,206 --> 00:05:25,299
- I'd like to propose an experiment...
- Goggles.

94
00:05:25,475 --> 00:05:26,772
Right.

95
00:05:28,878 --> 00:05:31,904
- I would like to propose an experiment.
- Hang on.

96
00:05:32,081 --> 00:05:35,448
I'm trying to see how long it takes
a 500-kilowatt oxygen-iodine laser...

97
00:05:35,618 --> 00:05:37,745
...to heat up my cup of noodles.

98
00:05:38,855 --> 00:05:41,653
I've done it.
About two seconds, 2.6 for minestrone.

99
00:05:48,431 --> 00:05:51,628
Anyway, I was thinking more
of a biosocial exploration...

100
00:05:51,801 --> 00:05:53,666
...with a neurochemical overlay.

101
00:05:55,338 --> 00:05:57,670
Wait, are you asking me out?

102
00:05:58,574 --> 00:06:01,008
I was going to characterize it
as the modification...

103
00:06:01,177 --> 00:06:05,307
...of our colleague/friendship paradigm
with the addition of a date-like component.

104
00:06:05,481 --> 00:06:09,315
But we don't need to quibble
over terminology.

105
00:06:09,485 --> 00:06:11,385
What sort of experiment
would you propose?

106
00:06:11,554 --> 00:06:14,079
There is a generally accepted
pattern in this area.

107
00:06:14,257 --> 00:06:17,488
I pick you up, take you to a restaurant,
then we would see a movie.

108
00:06:17,660 --> 00:06:19,992
Probably a romantic comedy
featuring the talents...

109
00:06:20,163 --> 00:06:22,256
...of Hugh Grant or Sandra Bullock.

110
00:06:22,899 --> 00:06:23,923
Interesting.

111
00:06:24,100 --> 00:06:26,660
And would you agree
the primary way we would evaluate...

112
00:06:26,836 --> 00:06:30,704
...the success or failure of the date would
be based on the biochemical reaction...

113
00:06:30,873 --> 00:06:34,309
...during the good-night kiss?
- Heart rate, pheromones, etcetera, yes.

114
00:06:34,944 --> 00:06:36,935
Why don't we stipulate
the date goes well...

115
00:06:37,113 --> 00:06:38,910
...and move to the key variable?

116
00:06:39,082 --> 00:06:40,777
- You mean kiss you now?
- Yes.

117
00:06:40,950 --> 00:06:44,010
- Define the parameters of the kiss.
- Closed mouth, but romantic.

118
00:06:44,187 --> 00:06:45,950
- Mint?
- Thank you.

119
00:06:53,096 --> 00:06:56,930
- Shall I count down from three?
- No, I think it needs to be spontaneous.

120
00:07:04,040 --> 00:07:06,304
- What do you think?
- You proposed the experiment.

121
00:07:06,476 --> 00:07:08,876
I think you should present
your findings first.

122
00:07:09,045 --> 00:07:10,603
Fair enough.

123
00:07:10,780 --> 00:07:15,774
On the plus side, it was a good kiss,
reasonable technique, no extraneous spittle.

124
00:07:17,053 --> 00:07:19,283
On the other hand, no arousal.

125
00:07:20,423 --> 00:07:21,913
- None?
- None.

126
00:07:22,892 --> 00:07:24,120
Ah.

127
00:07:25,495 --> 00:07:27,588
Well, thank you for your time.

128
00:07:27,763 --> 00:07:29,458
- Thank you.
- Hmm.

129
00:07:33,603 --> 00:07:35,070
None at all?

130
00:07:39,208 --> 00:07:45,169
Sheldon, if you were a robot
and I knew and you didn't...

131
00:07:46,849 --> 00:07:49,010
...would you want me to tell you?

132
00:07:50,419 --> 00:07:51,943
That depends.

133
00:07:52,121 --> 00:07:57,787
When I learn that I'm a robot,
will I be able to handle it?

134
00:07:59,061 --> 00:08:03,088
Maybe, although the history
of science fiction is not on your side.

135
00:08:03,666 --> 00:08:05,634
Okay. Uh, let me ask you this.

136
00:08:06,035 --> 00:08:08,026
When I learn that I'm a robot...

137
00:08:08,204 --> 00:08:11,935
...would I be bound by
Asimov's Three Laws of Robotics?

138
00:08:12,375 --> 00:08:16,368
You might be bound by them right now.

139
00:08:19,148 --> 00:08:20,172
That's true.

140
00:08:20,349 --> 00:08:24,786
- Have you ever harmed a human being?
- Of course not.

141
00:08:24,954 --> 00:08:28,151
Have you ever harmed yourself,
except in cases where a human being...

142
00:08:28,324 --> 00:08:29,985
...would have been endangered?

143
00:08:30,159 --> 00:08:32,650
- Well, no.
- I smell robot.

144
00:08:36,365 --> 00:08:40,267
- Hey, what's going on?
- Internet's been down for half an hour.

145
00:08:42,572 --> 00:08:44,130
Also, Sheldon may be a robot.

146
00:08:46,209 --> 00:08:48,769
So how did it go with Leslie?

147
00:08:48,945 --> 00:08:53,279
Oh, we tried kissing,
but the Earth didn't move.

148
00:08:53,449 --> 00:08:57,886
I mean, any more than the 383 miles
that it was gonna move anyway.

149
00:08:59,722 --> 00:09:01,815
Oh, I've seen that look before.

150
00:09:01,991 --> 00:09:05,358
This is just gonna be two weeks
of moping and tedious emo songs...

151
00:09:05,528 --> 00:09:08,986
...and calling me to come down
to pet stores to look at cats.

152
00:09:10,833 --> 00:09:12,562
I don't know if I can take it.

153
00:09:13,469 --> 00:09:15,369
You could power down.

154
00:09:17,673 --> 00:09:21,439
Well, as usual,
Wolowitz has the solution.

155
00:09:22,144 --> 00:09:25,272
I happen to know a place
where there are plenty of eligible women...

156
00:09:25,448 --> 00:09:27,245
...and Leonard could have his pick.

157
00:09:37,159 --> 00:09:39,059
Remember the Latin hips.

158
00:09:39,428 --> 00:09:42,295
Shoulders stay still and we sway.

159
00:09:47,536 --> 00:09:49,834
One, two, three.

160
00:09:50,506 --> 00:09:52,599
Five, six, seven.

161
00:09:53,175 --> 00:09:56,201
I think Mrs. Tishman's
got her eye on you.

162
00:09:56,646 --> 00:09:58,978
I've been there. You're in for a treat.

163
00:10:15,331 --> 00:10:16,355
Oh, good Lord.

164
00:10:27,143 --> 00:10:29,134
God, that's a good song.

165
00:10:29,712 --> 00:10:32,875
If you're compiling a mix CD
for a double suicide.

166
00:10:33,649 --> 00:10:37,210
Oh, I hope that scratching post
is for you.

167
00:10:37,386 --> 00:10:40,378
I know what you're thinking.
I've taken your asthma into account.

168
00:10:40,556 --> 00:10:42,547
There's a feline geneticist in San Diego...

169
00:10:42,725 --> 00:10:45,751
...who's developed
the cutest little hypoallergenic calicos.

170
00:10:45,928 --> 00:10:47,828
- Listen...
- I've been thinking about names.

171
00:10:47,997 --> 00:10:52,491
I'm kind of torn between Einstein,
Newton and Sergeant Fuzzy Boots.

172
00:10:53,002 --> 00:10:55,766
Do you think you can satisfy
your need for a relationship...

173
00:10:55,938 --> 00:10:58,168
...with a genetically-altered cat?

174
00:10:58,341 --> 00:11:02,175
Maybe, if it's a cute little cuddly cat.

175
00:11:02,345 --> 00:11:04,870
Oh, come on, Leonard...

176
00:11:06,549 --> 00:11:08,346
This is obviously about Penny.

177
00:11:10,619 --> 00:11:12,018
It doesn't matter.

178
00:11:12,188 --> 00:11:15,089
The woman's not interested in me.
The woman rejected me.

179
00:11:15,257 --> 00:11:17,623
Okay, look. Ahem.

180
00:11:18,327 --> 00:11:21,819
You have as much of a chance
of having a sexual relationship with Penny...

181
00:11:21,997 --> 00:11:25,660
...as the Hubble telescope does
of discovering at the center of a black hole...

182
00:11:25,835 --> 00:11:29,236
...is a little man with a flashlight
searching for a circuit breaker.

183
00:11:31,540 --> 00:11:36,603
Nevertheless, I do feel obligated
to point out to you...

184
00:11:36,779 --> 00:11:38,804
...that she did not reject you.

185
00:11:39,281 --> 00:11:40,908
You did not ask her out.

186
00:11:42,485 --> 00:11:43,884
You're right.

187
00:11:44,286 --> 00:11:46,117
I didn't ask her. I should ask her out.

188
00:11:46,288 --> 00:11:48,483
No, no, no. That was not my point.

189
00:11:49,058 --> 00:11:51,891
My point was, don't buy a cat.

190
00:11:52,061 --> 00:11:54,029
No, but you're right.

191
00:11:54,196 --> 00:11:56,391
I should march over there
and ask her out.

192
00:11:56,932 --> 00:11:59,958
Oh, goody, we're getting a cat.

193
00:12:01,904 --> 00:12:03,872
Oh, yeah... No.

194
00:12:07,109 --> 00:12:09,077
- Oh, hey, Leonard.
- Good afternoon, Penny.

195
00:12:09,245 --> 00:12:11,611
So, hi, hey. Uh...

196
00:12:12,748 --> 00:12:14,739
I was wondering
if you had plans for dinner.

197
00:12:14,917 --> 00:12:16,282
Uh, you mean, dinner tonight?

198
00:12:17,787 --> 00:12:20,347
There is an inherent ambiguity
in the word "dinner. "

199
00:12:20,523 --> 00:12:23,390
Technically,
it refers to the largest meal of the day.

200
00:12:23,559 --> 00:12:26,084
So to clarify here,
by dinner, I mean supper.

201
00:12:26,462 --> 00:12:28,259
- Supper?
- Or dinner, you know.

202
00:12:29,565 --> 00:12:32,159
I was thinking 6:30 if you can go,
or a different time.

203
00:12:32,334 --> 00:12:33,494
Uh, 6:30 is great.

204
00:12:33,669 --> 00:12:35,136
Really?

205
00:12:37,072 --> 00:12:39,267
- Great.
- Yeah, I like hanging out with you guys.

206
00:12:39,442 --> 00:12:40,466
Us guys?

207
00:12:40,643 --> 00:12:43,510
Oh, you know, Sheldon, Howard, Raj.
Who all is coming?

208
00:12:44,580 --> 00:12:45,979
They...

209
00:12:46,816 --> 00:12:49,114
...might all be there.

210
00:12:50,252 --> 00:12:52,948
Or a subset of them might be there.

211
00:12:53,122 --> 00:12:55,386
Algebraically speaking,
there are too many unknowns.

212
00:12:55,558 --> 00:12:57,549
For example,
Sheldon had Quizno's for lunch.

213
00:12:57,726 --> 00:13:00,286
Sometimes he finds that filling,
other times he doesn't.

214
00:13:00,463 --> 00:13:03,762
It's no fault of Quizno's.
They have a varied menu.

215
00:13:05,234 --> 00:13:06,963
Okay, whatever, it sounds like fun.

216
00:13:07,703 --> 00:13:09,068
Great.

217
00:13:09,238 --> 00:13:10,762
- Did we say a time?
- Six-thirty.

218
00:13:10,940 --> 00:13:11,964
Still good?

219
00:13:12,141 --> 00:13:13,768
- It's fine.
- It's not carved in stone.

220
00:13:13,943 --> 00:13:15,205
No. Six-thirty is great.

221
00:13:15,377 --> 00:13:17,470
I'll get my chisel. Heh-heh.

222
00:13:18,514 --> 00:13:20,175
Why?

223
00:13:22,151 --> 00:13:26,247
To carve the...
Okay, I'll see you at 6:30.

224
00:13:36,899 --> 00:13:38,730
How do I look?

225
00:13:43,172 --> 00:13:45,003
Could you be more specific?

226
00:13:46,709 --> 00:13:49,109
Can you tell I'm perspiring a little?

227
00:13:49,945 --> 00:13:54,382
No. The dark crescent-shaped patterns
under your arms conceal it nicely.

228
00:13:56,619 --> 00:13:58,416
- What time is your date?
- Six-thirty.

229
00:13:58,621 --> 00:14:00,987
Perfect, that gives you
two hours and 15 minutes...

230
00:14:01,156 --> 00:14:04,353
...for that dense molecular cloud
of Aramis to dissipate.

231
00:14:05,327 --> 00:14:08,694
- Is it too much?
- Not if you're a rugby team.

232
00:14:10,366 --> 00:14:13,301
By the way, you didn't join us
because you stuffed yourself...

233
00:14:13,469 --> 00:14:15,699
...with a chicken carbonara sub
at Quizno's.

234
00:14:16,138 --> 00:14:17,264
Why would I join you?

235
00:14:18,274 --> 00:14:19,764
No reason.

236
00:14:22,177 --> 00:14:24,611
Oh, you know what?
Maybe this isn't such a good idea.

237
00:14:24,780 --> 00:14:27,180
Oh, no, well, now,
there's always the possibility...

238
00:14:27,349 --> 00:14:29,544
...that alcohol and poor judgment
on her part...

239
00:14:29,718 --> 00:14:32,084
...might lead to a nice romantic evening.

240
00:14:33,622 --> 00:14:36,887
You're right. Alcohol, poor judgment.
It could go well.

241
00:14:37,493 --> 00:14:40,553
Of course, there's the other possibility
that this day kicks off...

242
00:14:40,729 --> 00:14:43,789
...an unpleasant six months of you
passing awkwardly in the hall...

243
00:14:43,966 --> 00:14:46,833
...until one of you breaks down
and moves to another zip code.

244
00:14:47,269 --> 00:14:50,295
You could have stopped at,
"It could go well. "

245
00:14:50,906 --> 00:14:52,931
If I could have, I would have.

246
00:14:53,342 --> 00:14:55,936
I mean, I'm a perfectly nice guy.
There's no reason...

247
00:14:56,111 --> 00:14:58,909
...we couldn't go to the restaurant
and have a lovely dinner.

248
00:14:59,081 --> 00:15:03,711
Maybe take a walk afterwards,
talk about things we have in common.

249
00:15:03,886 --> 00:15:06,684
"You love pottery? I love pottery. "

250
00:15:07,122 --> 00:15:09,750
You know, there's a pause.
We both know what's happening.

251
00:15:09,925 --> 00:15:11,893
We kiss. It's a little tentative at first.

252
00:15:12,061 --> 00:15:15,053
But I realize, she's kissing me back,
she's biting my lower lip.

253
00:15:15,230 --> 00:15:17,357
She wants me.
This thing is going the distance.

254
00:15:17,533 --> 00:15:19,524
We're gonna have sex.
Oh, God, oh, my God.

255
00:15:22,271 --> 00:15:24,865
Is the sex starting now?

256
00:15:25,574 --> 00:15:26,939
I'm having a panic attack.

257
00:15:27,109 --> 00:15:30,442
Oh, okay. Well, then, calm down.

258
00:15:30,613 --> 00:15:32,478
If I could,
I wouldn't be having an attack.

259
00:15:32,648 --> 00:15:35,048
- That's why they call it a panic attack.
- All right.

260
00:15:35,217 --> 00:15:36,775
Would you just sit down?

261
00:15:36,952 --> 00:15:39,512
- Yeah, sit down. Now close your eyes.
- Why?

262
00:15:39,688 --> 00:15:40,882
- Well, just do it.
- Okay.

263
00:15:41,056 --> 00:15:44,048
Now, try to increase
your alpha-wave activity.

264
00:15:44,226 --> 00:15:46,990
- What?
- It's a biofeedback technique.

265
00:15:47,162 --> 00:15:49,289
It's relaxation
through brainwave manipulation.

266
00:15:49,465 --> 00:15:51,626
<i>I read it
in Journal of American Neuroscience.</i>

267
00:15:51,800 --> 00:15:54,496
It was sparsely sourced,
but I think the science is valid.

268
00:15:54,670 --> 00:15:57,230
I probably have it here somewhere.

269
00:15:57,406 --> 00:15:59,533
Who am I kidding?
I can't go through with this.

270
00:15:59,708 --> 00:16:01,437
- You need to call her and cancel.
- Me?

271
00:16:01,610 --> 00:16:03,134
- Yes.
- What should I tell her?

272
00:16:03,312 --> 00:16:05,712
- I don't know. Tell her I'm sick.
- Okay.

273
00:16:05,881 --> 00:16:08,406
Not the kind that would make her
wanna take care of me.

274
00:16:08,584 --> 00:16:12,816
But nothing so critical that she'll feel
uncomfortable if I wanna try this again.

275
00:16:12,988 --> 00:16:15,479
Got it. So I'm assuming nothing venereal.

276
00:16:20,162 --> 00:16:22,722
I'll just tell her that you had
a routine colonoscopy...

277
00:16:22,898 --> 00:16:24,729
...and haven't quite bounced back.

278
00:16:24,900 --> 00:16:27,300
- Give me the phone.
- I thought you wanted to cancel.

279
00:16:27,469 --> 00:16:30,267
I can't. If I don't show up,
she'll still be expecting you.

280
00:16:30,973 --> 00:16:32,838
Why would she be expecting me?

281
00:16:33,842 --> 00:16:37,869
Stop asking me all these questions.
I need to take another shower.

282
00:16:41,116 --> 00:16:43,778
- Are the rest of the guys meeting us here?
- Oh, yeah.

283
00:16:43,952 --> 00:16:45,283
No.

284
00:16:45,888 --> 00:16:47,879
Turns out that Raj and Howard
had to work...

285
00:16:48,057 --> 00:16:53,518
...and Sheldon had a colonoscopy,
and he hasn't quite bounced back yet.

286
00:16:54,496 --> 00:16:56,760
Ooh, my uncle just had
a colonoscopy.

287
00:16:56,932 --> 00:17:01,028
You're kidding. Well, then,
that's something we have in common.

288
00:17:01,570 --> 00:17:03,265
How?

289
00:17:03,439 --> 00:17:09,935
We both have people in our lives
who wanna nip intestinal polyps in the bud.

290
00:17:15,818 --> 00:17:17,911
So, what's new in the world of physics?

291
00:17:19,021 --> 00:17:20,579
Nothing.

292
00:17:21,490 --> 00:17:22,980
Really? Nothing?

293
00:17:23,158 --> 00:17:28,653
Well, with the exception of string theory,
not much has happened since the 1930s...

294
00:17:28,831 --> 00:17:30,560
...and you can't prove string theory.

295
00:17:30,733 --> 00:17:36,171
At best you can say, "Hey, look, my idea
has an internal logical consistency. "

296
00:17:37,306 --> 00:17:40,969
Ah. Well, I'm sure things will pick up.

297
00:17:45,080 --> 00:17:47,071
What's new at the Cheesecake Factory?

298
00:17:47,249 --> 00:17:49,683
Oh, uh, not much.

299
00:17:49,852 --> 00:17:54,255
Oh, we do have a chocolate key lime
that's moving pretty well.

300
00:17:54,423 --> 00:17:55,947
Good.

301
00:17:56,458 --> 00:17:57,857
Good.

302
00:18:00,796 --> 00:18:04,960
Uh, what about
your, uh, hallway friend?

303
00:18:05,134 --> 00:18:09,537
Doug? Oh, yeah. I don't know. I mean,
you know, he's nice and funny, but...

304
00:18:09,705 --> 00:18:11,969
- Can I get you started with some drinks?
- No. Bzzz.

305
00:18:13,242 --> 00:18:15,369
- You were saying, "But... "?
- I'd like a drink.

306
00:18:15,544 --> 00:18:18,809
Just say the "but" thing about Doug,
and then I'll get her back.

307
00:18:19,414 --> 00:18:22,247
Okay, well, it...
I don't know. It's just me.

308
00:18:22,417 --> 00:18:24,578
I'm still getting over this break-up
with Kurt.

309
00:18:24,753 --> 00:18:27,551
And this thing with Doug
would just be rebound sex.

310
00:18:28,123 --> 00:18:32,321
Ugh. Don't get me started
on rebound sex.

311
00:18:32,694 --> 00:18:34,161
It's just... It's my pattern.

312
00:18:34,329 --> 00:18:36,524
I break up, then I find some cute guy...

313
00:18:36,698 --> 00:18:40,429
...and then it's just
36 meaningless hours of...

314
00:18:40,602 --> 00:18:41,830
You know.

315
00:18:43,872 --> 00:18:45,305
I'm not sure that I do. Um...

316
00:18:46,642 --> 00:18:51,477
Is that one 36-hour experience
or is that 36 hours spread out over...

317
00:18:51,647 --> 00:18:55,105
...say, one glorious summer?

318
00:18:55,284 --> 00:18:56,751
No. It's usually over a weekend.

319
00:18:56,919 --> 00:18:59,683
And trust me,
you do not feel good after it.

320
00:19:00,055 --> 00:19:02,285
Well, chafing, right?

321
00:19:03,458 --> 00:19:04,516
Emotionally.

322
00:19:04,693 --> 00:19:07,594
Of course, yeah. Emotional chafing.

323
00:19:14,670 --> 00:19:16,831
Hey, do you wanna see something cool?

324
00:19:18,240 --> 00:19:22,438
I can make this olive go into this glass
without touching it.

325
00:19:22,711 --> 00:19:25,339
- How?
- Physics.

326
00:19:30,619 --> 00:19:32,849
Wow, centrifugal force.

327
00:19:33,021 --> 00:19:36,320
Actually, it's centripetal force,
which is an inward force...

328
00:19:36,491 --> 00:19:39,790
...generated by the glass
acting on the olive. Ooh.

329
00:19:40,629 --> 00:19:41,891
Excuse me.

330
00:19:42,064 --> 00:19:45,192
Now, if you were biting on the olive...

331
00:19:45,367 --> 00:19:48,894
...you'd be on a non-inertial
reference frame and would...

332
00:19:50,305 --> 00:19:53,502
- Are you okay?
- Yeah, I'm okay.

333
00:19:54,176 --> 00:19:55,700
Did you spill ketchup?

334
00:19:55,878 --> 00:19:58,210
- No.
- I'm not okay.

335
00:20:01,516 --> 00:20:03,381
You don't wanna go
to the emergency room?

336
00:20:03,552 --> 00:20:04,576
No, no. I'm okay.

337
00:20:04,753 --> 00:20:06,152
It's stopped bleeding.

338
00:20:06,321 --> 00:20:09,290
Anyway, you did throw up.
Isn't that a sign of a concussion?

339
00:20:09,458 --> 00:20:12,723
Yes, but I get carsick too, so...

340
00:20:12,895 --> 00:20:13,919
Okay.

341
00:20:14,096 --> 00:20:15,791
Sorry about your car, by the way.

342
00:20:15,964 --> 00:20:19,593
Oh, no, it's fine.
You got most of it out the window.

343
00:20:20,202 --> 00:20:22,102
The poor guy on the bike.

344
00:20:24,840 --> 00:20:25,864
I had a nice time.

345
00:20:26,341 --> 00:20:27,968
Yeah, me too.

346
00:20:29,044 --> 00:20:30,341
Well, um, good night.

347
00:20:31,079 --> 00:20:32,410
Good night.

348
00:20:33,382 --> 00:20:35,009
- Leonard.
- Yeah?

349
00:20:35,784 --> 00:20:38,048
Was this supposed to be a date?

350
00:20:38,854 --> 00:20:40,515
This?

351
00:20:41,123 --> 00:20:43,114
No.

352
00:20:43,825 --> 00:20:45,383
No, of course not.

353
00:20:45,560 --> 00:20:48,791
This was just you and me hanging out
with guys who didn't show up...

354
00:20:48,964 --> 00:20:52,092
...because of work and a colonoscopy.

355
00:20:53,402 --> 00:20:54,892
Okay. I was just checking.

356
00:20:56,104 --> 00:21:00,507
When I take a girl on a date,
and I do, heh...

357
00:21:01,910 --> 00:21:03,844
...she knows she's been dated.

358
00:21:05,480 --> 00:21:07,539
Capital D. Heh.

359
00:21:07,983 --> 00:21:12,511
Bold face, underlined, like, dated.

360
00:21:13,488 --> 00:21:16,321
I think I might have a concussion.
I'm gonna go lay down.

361
00:21:16,491 --> 00:21:17,924
Good night.

362
00:21:21,530 --> 00:21:24,863
- So how was your date?
- Awesome. Heh.

363
00:21:29,071 --> 00:21:32,097
Score one for liquor and poor judgment.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
