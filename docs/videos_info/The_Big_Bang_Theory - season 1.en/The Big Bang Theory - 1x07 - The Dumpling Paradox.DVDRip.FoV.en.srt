1
00:00:01,718 --> 00:00:04,209
Watch this, it's really cool. Ahem.

2
00:00:04,387 --> 00:00:06,446
Call Leonard Hofstadter.

3
00:00:06,623 --> 00:00:10,559
<i>Did you say, "Call Helen Boxleitner"?</i>

4
00:00:12,462 --> 00:00:16,421
No. Call Leonard Hofstadter.

5
00:00:16,599 --> 00:00:20,296
<i>Did you say, "Call Temple Beth Seder"?</i>

6
00:00:21,538 --> 00:00:23,870
- No.
- Here, let me try.

7
00:00:24,541 --> 00:00:28,637
Call M'kflono M'kflooniloo. Ha-ha. Ha.

8
00:00:29,312 --> 00:00:31,439
<i>Calling Rajesh Koothrappali.</i>

9
00:00:37,454 --> 00:00:40,252
Oh. It's very impressive.

10
00:00:40,890 --> 00:00:42,619
And a little racist.

11
00:00:43,727 --> 00:00:46,195
If we're through playing
Mock the Flawed Technology...

12
00:00:46,362 --> 00:00:47,886
...can we get on with "Halo" night?

13
00:00:48,064 --> 00:00:50,157
We were supposed to start at 8.
It's now 8:06.

14
00:00:50,333 --> 00:00:51,732
So we'll start now.

15
00:00:51,901 --> 00:00:53,994
We have to decide
if those lost six minutes...

16
00:00:54,170 --> 00:00:57,105
...will come out of game time,
bathroom time or the pizza break.

17
00:00:57,273 --> 00:00:59,036
We could split it two, two and two.

18
00:00:59,209 --> 00:01:02,269
If we're having anchovies on the pizza,
we can't take it out of bathroom time.

19
00:01:03,613 --> 00:01:05,513
What fresh hell is this?

20
00:01:06,316 --> 00:01:08,477
- Hey, Penny. Come on in.
- Hey, guys.

21
00:01:08,651 --> 00:01:13,111
See a Penny, pick her up,
and all the day, you'll have good luck.

22
00:01:15,425 --> 00:01:17,518
No, you won't. Uh...

23
00:01:17,694 --> 00:01:20,527
- Can I hide out here for a while?
- Sure. What's going on?

24
00:01:20,697 --> 00:01:23,564
Well, there's this girl I know
from back in Nebraska, Christy.

25
00:01:23,733 --> 00:01:26,258
She called me up,
she's like, "Hey, how's California?"

26
00:01:26,436 --> 00:01:28,768
And I'm like, "Awesome,"
because it's not Nebraska.

27
00:01:28,972 --> 00:01:31,839
The next thing I know,
she's invited herself to stay with me.

28
00:01:32,008 --> 00:01:34,704
- 8:08.
- Shh.

29
00:01:35,445 --> 00:01:38,073
She got here today
and she's just been in my apartment...

30
00:01:38,248 --> 00:01:42,344
...yakking about every guy she slept with
in Omaha, which is every guy in Omaha...

31
00:01:42,519 --> 00:01:45,044
...and washing
the sluttiest collection of underwear...

32
00:01:45,221 --> 00:01:47,121
...you have ever seen
in my bathroom sink.

33
00:01:47,290 --> 00:01:50,555
Is she doing it one thong at a time
or does she throw it all in?

34
00:01:50,727 --> 00:01:53,457
Like some sort of erotic bouillabaisse?

35
00:01:55,031 --> 00:01:57,556
- He really needs to dial it down.
- I know.

36
00:01:59,102 --> 00:02:02,037
So if you don't like this Christy,
why are you letting her stay?

37
00:02:02,205 --> 00:02:05,072
She was engaged to my cousin
while sleeping with my brother...

38
00:02:05,241 --> 00:02:06,538
...so she's kind of family.

39
00:02:08,011 --> 00:02:10,309
Yeah, I apologize for my earlier outburst.

40
00:02:10,480 --> 00:02:12,380
Who needs "Halo"
when we can be regaled...

41
00:02:12,549 --> 00:02:15,575
...with the delightfully folksy tale
of the whore of Omaha?

42
00:02:16,619 --> 00:02:18,587
Oh... I don't think she's a whore.

43
00:02:18,755 --> 00:02:20,382
No, yeah, she's definitely a whore.

44
00:02:20,557 --> 00:02:22,957
I mean, she has absolutely no standards.

45
00:02:23,126 --> 00:02:25,390
This one time, she was at...

46
00:02:25,562 --> 00:02:26,722
Where's Howard?

47
00:02:26,896 --> 00:02:28,124
<i>Bonjour, mademoiselle.</i>

48
00:02:28,298 --> 00:02:30,095
I understand you're new in town.

49
00:02:31,634 --> 00:02:32,931
Oh, good grief.

50
00:03:03,132 --> 00:03:08,001
Ugh. I cannot believe Christy let Howard
into my apartment.

51
00:03:08,404 --> 00:03:10,668
And I cannot believe people pay
for horoscopes...

52
00:03:10,840 --> 00:03:15,174
...but on a more serious note,
it's 8:13 and we're still not playing "Halo. "

53
00:03:15,345 --> 00:03:18,246
Okay, fine. We'll just play one-on-one
until he gets back.

54
00:03:18,414 --> 00:03:20,143
One-on-one? We don't play one-on-one.

55
00:03:20,316 --> 00:03:24,116
We play teams, not one-on-one.
One-on-one.

56
00:03:24,587 --> 00:03:28,751
Well, the only way we can play teams
at this point is if we cut Raj in half.

57
00:03:29,025 --> 00:03:32,984
Sure, cut the foreigner in half.
There's a billion more where he came from.

58
00:03:33,363 --> 00:03:35,422
Hey, if you guys need a fourth, I'll play.

59
00:03:35,598 --> 00:03:38,123
- Great idea.
- Uh, no.

60
00:03:39,168 --> 00:03:43,229
The wheel was a great idea.
Relativity was a great idea.

61
00:03:43,406 --> 00:03:46,307
This is a notion,
and a rather sucky one at that.

62
00:03:47,176 --> 00:03:49,201
- Why?
- Why? Oh...

63
00:03:49,379 --> 00:03:52,815
- Penny, Penny, Penny.
- What, what, what?

64
00:03:52,982 --> 00:03:56,145
This is a complex battle simulation
with a steep learning curve.

65
00:03:56,319 --> 00:03:58,719
There are myriad weapons,
vehicles and strategies to master...

66
00:03:58,888 --> 00:04:00,913
...not to mention
an extremely intricate back-story.

67
00:04:02,292 --> 00:04:05,557
Cool! Whose head did I just blow off?

68
00:04:06,195 --> 00:04:07,219
Mine.

69
00:04:08,398 --> 00:04:11,561
Okay, I got this. Lock and load, boys.

70
00:04:12,168 --> 00:04:14,193
It's the only way we can play teams.

71
00:04:14,370 --> 00:04:17,806
But whoever's her partner will be hamstrung
by her lack of experience, not to mention...

72
00:04:19,175 --> 00:04:20,972
Ha-ha! There goes your head again.

73
00:04:21,611 --> 00:04:25,069
Okay, it's not good sportsmanship
to shoot somebody who's just re-spawned.

74
00:04:25,248 --> 00:04:27,148
You need to give them a chance to...
Now, come on.

75
00:04:28,651 --> 00:04:30,551
Raj, Raj, she's got me cornered.
Cover me.

76
00:04:30,720 --> 00:04:32,847
Cover this, suckers.

77
00:04:34,223 --> 00:04:35,315
Penny, you are on fire.

78
00:04:35,491 --> 00:04:37,584
Yeah, and so is Sheldon.

79
00:04:39,462 --> 00:04:42,226
Okay, that's it.
I don't know how, but she is cheating.

80
00:04:42,398 --> 00:04:45,993
No one can be that attractive
and this skilled at a video game.

81
00:04:46,169 --> 00:04:48,364
Wait, Sheldon, come back.
You forgot something.

82
00:04:48,538 --> 00:04:49,596
What?

83
00:04:49,772 --> 00:04:51,501
This plasma grenade.

84
00:04:53,910 --> 00:04:56,344
Ha! Look, it's raining you.

85
00:04:57,580 --> 00:05:01,016
You laugh now,
you just wait until you need tech support.

86
00:05:02,685 --> 00:05:04,983
Gosh. He's kind of a sore loser, isn't he?

87
00:05:05,154 --> 00:05:08,612
Well, to be fair,
he's also a rather unpleasant winner.

88
00:05:08,791 --> 00:05:10,656
Well, it's been fun.

89
00:05:10,827 --> 00:05:12,590
Penny, we make such a good team.

90
00:05:12,762 --> 00:05:15,560
Maybe we could enter
a couple of "Halo" tournaments sometime.

91
00:05:16,099 --> 00:05:18,533
Or we could just have a life.

92
00:05:19,602 --> 00:05:22,196
I guess for you, that's an option.
Ha-ha. Ha.

93
00:05:22,638 --> 00:05:24,162
- Good night, Leonard.
- Good night.

94
00:05:24,340 --> 00:05:26,831
As usual, nice talking to you, Raj.

95
00:05:28,211 --> 00:05:30,270
What do you suppose she meant by that?

96
00:05:31,280 --> 00:05:32,907
She's an enigma, Raj.

97
00:05:33,716 --> 00:05:36,651
There's a certain ethic to the game,
Penny, a well-established...

98
00:05:36,819 --> 00:05:39,344
- She's gone, Sheldon.
- Oh.

99
00:05:39,522 --> 00:05:41,387
Well, she could have said goodbye.

100
00:05:42,458 --> 00:05:44,619
Okay, I have a problem.

101
00:05:44,794 --> 00:05:48,059
It's called carpal tunnel syndrome,
and quite frankly, you deserve it.

102
00:05:49,766 --> 00:05:50,790
What's wrong?

103
00:05:50,967 --> 00:05:56,769
Well, um, Howard and Christy
are kind of hooking up in my bedroom.

104
00:05:57,774 --> 00:06:00,299
- Are you sure?
- Look, I grew up on a farm, okay?

105
00:06:00,476 --> 00:06:04,412
From what I heard, they're either having sex
or Howard's caught in a milking machine.

106
00:06:07,216 --> 00:06:08,911
Do you mind if I stay here tonight?

107
00:06:09,085 --> 00:06:12,282
No. Yeah, take the couch, or my bed.
I just got new pillows.

108
00:06:12,455 --> 00:06:14,719
Hypoallergenic.

109
00:06:15,258 --> 00:06:16,589
Uh, the couch is good.

110
00:06:16,759 --> 00:06:19,023
Hold that thought. Leonard, a moment.

111
00:06:23,132 --> 00:06:24,599
You have a problem with this.

112
00:06:24,767 --> 00:06:25,791
Where do I begin?

113
00:06:25,968 --> 00:06:28,801
It's up to you. Crazy person's choice.

114
00:06:28,971 --> 00:06:31,303
Well, first, we don't have house guests.

115
00:06:31,474 --> 00:06:34,500
Frankly, if I could afford the rent,
I'd ask you to leave.

116
00:06:35,278 --> 00:06:37,940
Your friendship means a lot
to me as well. What else?

117
00:06:38,514 --> 00:06:40,778
Well, our earthquake supplies.

118
00:06:40,950 --> 00:06:43,350
- We have a two-man, two-day kit.
- So?

119
00:06:43,519 --> 00:06:46,784
So if there's an earthquake
and the three of us are trapped here...

120
00:06:46,956 --> 00:06:49,948
...we could be out of food
by tomorrow afternoon.

121
00:06:50,693 --> 00:06:52,820
Are you suggesting
that if we let Penny stay...

122
00:06:52,995 --> 00:06:55,122
...we might succumb to cannibalism?

123
00:06:55,298 --> 00:06:58,165
No one ever thinks it'll happen
until it does.

124
00:06:59,469 --> 00:07:04,304
Penny, if you promise not to chew the flesh
off our bones while we sleep, you can stay.

125
00:07:04,740 --> 00:07:09,575
- Ha. What?
- He's engaging in reductio ad absurdum.

126
00:07:10,446 --> 00:07:14,007
It's the fallacy of extending someone's
argument to ridiculous proportions...

127
00:07:14,183 --> 00:07:17,277
...and then criticizing the result.
And I don't appreciate it.

128
00:07:18,154 --> 00:07:20,520
I'll get you a blanket and a pillow.

129
00:07:20,823 --> 00:07:25,157
Okay, since I'm obviously being ignored
here, let's go over the morning schedule.

130
00:07:25,328 --> 00:07:27,853
I use the bathroom from 7 to 7:20.

131
00:07:28,030 --> 00:07:30,498
Plan your ablutions
and bodily functions accordingly.

132
00:07:32,001 --> 00:07:34,265
How am I supposed to
plan my bodily functions?

133
00:07:34,437 --> 00:07:36,701
I suggest no liquids after 11 p. m.

134
00:07:37,673 --> 00:07:40,369
- Here you go.
- Thanks, Leonard.

135
00:07:41,410 --> 00:07:43,503
Hm-hm. Wrong.

136
00:07:46,349 --> 00:07:47,373
I'm listening.

137
00:07:47,550 --> 00:07:49,518
Your head goes on the other end.

138
00:07:49,685 --> 00:07:51,676
- Why?
- It's culturally universal.

139
00:07:51,854 --> 00:07:53,617
A bed, even a temporary bed...

140
00:07:53,789 --> 00:07:56,553
...is always oriented with the headboard
away from the door.

141
00:07:56,726 --> 00:08:01,493
It serves the ancient imperative
of protecting oneself against marauders.

142
00:08:04,534 --> 00:08:06,331
- I'll risk it.
- Ooh.

143
00:08:06,502 --> 00:08:07,867
Anything else I should know?

144
00:08:08,037 --> 00:08:11,973
Yes. If you use my toothbrush,
I'll jump out that window.

145
00:08:13,476 --> 00:08:16,343
Please don't come to my funeral.
Have a good night.

146
00:08:18,881 --> 00:08:20,974
- Sorry about that.
- That's okay.

147
00:08:21,150 --> 00:08:25,553
FYI, his toothbrush is the red one
in the Plexiglas case under the UV light.

148
00:08:25,721 --> 00:08:29,122
- Ha. Got it.
- Well, sleep tight.

149
00:08:29,292 --> 00:08:31,089
Thanks.

150
00:08:33,829 --> 00:08:36,855
Funny expression, sleep tight. Ha-ha. Ha.

151
00:08:38,267 --> 00:08:40,428
It refers to
the early construction of beds...

152
00:08:40,603 --> 00:08:44,403
...which featured a mattress suspended
on interlocking ropes, which occasionally...

153
00:08:45,141 --> 00:08:47,109
Sleep tight.

154
00:10:01,884 --> 00:10:03,181
What are you doing?

155
00:10:06,689 --> 00:10:10,250
Every Saturday since we've lived in
this apartment I have awakened at 6:15...

156
00:10:10,426 --> 00:10:13,088
...poured a bowl of cereal,
added a quarter cup of milk...

157
00:10:13,262 --> 00:10:17,995
<i>...sat on this end of this couch, turned on
BBC America, and watched Doctor Who.</i>

158
00:10:18,801 --> 00:10:19,859
Penny's still sleeping.

159
00:10:21,504 --> 00:10:23,938
Every Saturday
since we've lived in this apartment...

160
00:10:24,106 --> 00:10:26,074
...I have awakened at 6:15,
poured myself...

161
00:10:26,242 --> 00:10:28,369
I know.
Look, you have a TV in your room.

162
00:10:28,544 --> 00:10:30,205
Why don't you have breakfast in bed?

163
00:10:30,613 --> 00:10:34,413
Because I am neither an invalid
nor a woman celebrating Mother's Day.

164
00:10:36,352 --> 00:10:37,580
What time is it?

165
00:10:38,020 --> 00:10:39,544
Almost 6:30.

166
00:10:39,722 --> 00:10:41,690
I slept all day?

167
00:10:42,291 --> 00:10:44,122
No, it's 6:30 in the morning.

168
00:10:44,860 --> 00:10:47,294
What the hell is your problem?

169
00:10:48,464 --> 00:10:50,989
Okay, this cereal
has lost all its molecular integrity.

170
00:10:51,167 --> 00:10:54,000
I now have a bowl
of shredded wheat paste.

171
00:10:54,970 --> 00:10:57,632
<i>Hola, nerd-migos.</i>

172
00:10:59,975 --> 00:11:02,205
Why do you people hate sleep?

173
00:11:03,179 --> 00:11:04,942
Are you wearing my robe?

174
00:11:06,449 --> 00:11:08,576
Oh, yeah. Sorry, I'll have it cleaned.

175
00:11:08,751 --> 00:11:11,083
That's okay, keep it.

176
00:11:11,854 --> 00:11:14,288
- Where's Christy?
- In the shower.

177
00:11:14,557 --> 00:11:16,787
By the way,
where did you get that loofa mitt?

178
00:11:16,959 --> 00:11:19,553
Yours reaches places
that mine just won't.

179
00:11:23,733 --> 00:11:25,928
You used my loofa?

180
00:11:26,102 --> 00:11:28,502
More precisely, we used your loofa.

181
00:11:28,671 --> 00:11:31,299
I exfoliated her brains out.

182
00:11:33,476 --> 00:11:35,410
You can keep that too.

183
00:11:35,878 --> 00:11:40,076
Ah. Well, then, we'll probably need to talk
about your stuffed-bear collection.

184
00:11:41,484 --> 00:11:43,645
Howard?
- In here, milady.

185
00:11:44,887 --> 00:11:48,482
Mm. There's my little engine that could.

186
00:11:53,095 --> 00:11:55,996
There's one beloved children's book
I'll never read again.

187
00:11:56,599 --> 00:11:58,590
Hi. Christy.

188
00:11:58,768 --> 00:12:00,395
- Hey, Leonard.
- I'm Sheldon.

189
00:12:00,569 --> 00:12:02,764
Right. You're Howard's entourage.

190
00:12:05,274 --> 00:12:07,174
So, Christy, what are your plans?

191
00:12:07,343 --> 00:12:10,779
Well, Howard said he'd take me shopping
in Beverly Hills.

192
00:12:10,946 --> 00:12:14,643
Yeah, no, I meant plans to find
someplace to live other than with me.

193
00:12:14,817 --> 00:12:17,786
Not that I don't love having you,
but it's a little crowded.

194
00:12:17,953 --> 00:12:20,012
Penny, you're always welcome
to stay with us.

195
00:12:20,189 --> 00:12:23,249
Oh, terrific.
Now we're running a cute little B & B.

196
00:12:24,026 --> 00:12:25,857
Let me offer a little thinking here.

197
00:12:26,028 --> 00:12:29,156
- Why doesn't Christy stay with me?
- You live with your mother.

198
00:12:29,331 --> 00:12:31,993
I do not. My mother lives with me.

199
00:12:33,002 --> 00:12:36,699
It's settled. Christy will stay with Howard,
Penny can go back to her apartment.

200
00:12:36,872 --> 00:12:39,306
<i>And I'll watch the last 24 minutes
of Doctor Who...</i>

201
00:12:39,475 --> 00:12:42,535
<i>...although at this point,
it's more like Doctor Why Bother.</i>

202
00:12:43,679 --> 00:12:47,240
- Sheldon, you just can't dictate...
- No more talking. Everybody go.

203
00:12:48,250 --> 00:12:51,845
So, what do you say?
Wanna repair to Casa Wolowitz?

204
00:12:52,021 --> 00:12:54,546
What is that, like a Mexican deli?

205
00:12:54,723 --> 00:12:56,918
I'm sorry,
I should have mentioned this earlier.

206
00:12:57,092 --> 00:12:58,821
My last name is Wolowitz.

207
00:12:58,994 --> 00:13:01,554
Oh. That's so cool.

208
00:13:01,730 --> 00:13:04,130
My first Jew.

209
00:13:07,236 --> 00:13:10,672
I imagine there aren't
very many kosher cornhuskers.

210
00:13:13,008 --> 00:13:15,704
- But you're still taking me shopping?
- Anything you want.

211
00:13:15,878 --> 00:13:18,210
Okay, I'll go pack my stuff.

212
00:13:20,783 --> 00:13:24,344
When they perfect human cloning,
I'm gonna order 12 of those.

213
00:13:26,388 --> 00:13:28,185
Howard, can't you see she's using you?

214
00:13:28,357 --> 00:13:31,622
Who cares? Last night,
she pulled off her blouse and I wept.

215
00:13:31,794 --> 00:13:33,455
Look, Howard, I know her, okay?

216
00:13:33,629 --> 00:13:37,065
She'll have sex with anyone
as long as they keep buying her things.

217
00:13:37,466 --> 00:13:38,933
- Really?
- Yeah.

218
00:13:39,101 --> 00:13:40,466
Yay.

219
00:13:43,038 --> 00:13:47,566
If you'll excuse me,
I have some bar mitzvah bonds to cash.

220
00:13:55,518 --> 00:13:58,419
I'm sorry,
we cannot do this without Wolowitz.

221
00:13:58,587 --> 00:14:01,522
We can't order Chinese food
without Wolowitz?

222
00:14:01,690 --> 00:14:02,952
Let me walk you through it.

223
00:14:03,125 --> 00:14:05,821
Our standard order is,
the steamed dumpling appetizer...

224
00:14:05,995 --> 00:14:08,122
...General Tso's chicken,
beef with broccoli...

225
00:14:08,297 --> 00:14:10,697
...shrimp with lobster sauce,
and vegetable lo mein.

226
00:14:10,866 --> 00:14:12,766
Do you see the problem?

227
00:14:14,236 --> 00:14:16,204
I see a problem.

228
00:14:16,372 --> 00:14:19,967
Our entire order is predicated
on four dumplings and four entr�es...

229
00:14:20,142 --> 00:14:22,633
...divided amongst four people.

230
00:14:24,480 --> 00:14:26,641
So we'll just order three entr�es.

231
00:14:26,815 --> 00:14:28,407
Fine. What do you wanna eliminate?

232
00:14:28,584 --> 00:14:31,485
- And who gets the extra dumpling?
- We could cut it into thirds.

233
00:14:31,654 --> 00:14:33,121
Then it's no longer a dumpling.

234
00:14:33,289 --> 00:14:37,089
Once you cut it open, it is, at best,
a very small open-faced sandwich.

235
00:14:37,560 --> 00:14:38,822
Hi, fellas.

236
00:14:38,994 --> 00:14:43,624
Oh. Where's your annoying little friend
who thinks he speaks Mandarin?

237
00:14:43,799 --> 00:14:46,199
He's putting his needs
ahead of the collective good.

238
00:14:46,368 --> 00:14:48,768
Where he comes from,
that's punishable by death.

239
00:14:49,405 --> 00:14:52,135
I come from Sacramento.

240
00:14:55,544 --> 00:14:58,479
Can we get an order of dumplings,
but with three instead of four?

241
00:14:58,647 --> 00:15:00,171
No substitutions.

242
00:15:00,349 --> 00:15:02,749
This isn't a substitution.
It's a reduction.

243
00:15:02,918 --> 00:15:05,318
Okay. No reductions.

244
00:15:06,088 --> 00:15:08,556
Fine. Bring us three orders of dumplings.

245
00:15:08,724 --> 00:15:11,192
- That's 12, we'll each have four.
- That works.

246
00:15:11,360 --> 00:15:13,851
No. Because we'll need
to eliminate another entr�e.

247
00:15:14,029 --> 00:15:15,257
No eliminations.

248
00:15:16,665 --> 00:15:19,099
If we have extra,
we'll just take the leftovers home.

249
00:15:19,268 --> 00:15:22,863
And divide it how? I'm telling you,
we cannot do this without Wolowitz.

250
00:15:23,038 --> 00:15:24,505
Wolowitz is with his girlfriend.

251
00:15:24,673 --> 00:15:27,233
If you'd let me invite Penny,
you would've had a fourth.

252
00:15:27,409 --> 00:15:29,138
Have you seen Penny eat Chinese food?

253
00:15:29,311 --> 00:15:31,575
She uses a fork
and she double-dips her egg rolls.

254
00:15:32,114 --> 00:15:35,481
- We don't order egg rolls.
- Exactly. But we'd have to if she was here.

255
00:15:35,651 --> 00:15:37,118
Can we please make a decision?

256
00:15:37,286 --> 00:15:41,689
Not only are there children starving in India,
there's an Indian starving right here.

257
00:15:43,158 --> 00:15:45,388
There's an idea.
Why don't we go out for Indian food?

258
00:15:45,561 --> 00:15:47,188
Ecch.

259
00:15:47,730 --> 00:15:49,129
You're nice boys.

260
00:15:49,298 --> 00:15:52,563
Tell you what I'm going to do.
I'm gonna bring you the four dumplings.

261
00:15:52,735 --> 00:15:54,464
When I'm walking over to the table...

262
00:15:54,637 --> 00:15:57,504
...maybe I get bumped,
one of the dumplings fall to the floor.

263
00:15:57,673 --> 00:15:58,697
No one has to know.

264
00:16:00,542 --> 00:16:01,668
I'll know.

265
00:16:07,650 --> 00:16:10,141
- How about soup?
- Yeah, we can always divide soup.

266
00:16:10,319 --> 00:16:11,809
What about the won tons?

267
00:16:18,093 --> 00:16:19,822
Oh. Hey, guys, what's up?

268
00:16:19,995 --> 00:16:21,986
It's "Halo" night.

269
00:16:22,831 --> 00:16:24,230
Yeah? Okay.

270
00:16:25,968 --> 00:16:27,435
So?

271
00:16:27,936 --> 00:16:30,928
With Wolowitz spending all of his time
with your friend Christy...

272
00:16:31,106 --> 00:16:35,099
She's not my friend. Friends do not
get their friends' Care Bears all sweaty.

273
00:16:36,111 --> 00:16:39,911
Right, anyway, uh,
with Wolowitz occupied elsewhere...

274
00:16:40,082 --> 00:16:42,516
...we had something
we wanted to ask you.

275
00:16:42,685 --> 00:16:44,585
- Sheldon?
- Yes. Ahem.

276
00:16:47,389 --> 00:16:50,381
Penny,
we would very much appreciate it...

277
00:16:50,559 --> 00:16:53,187
...if you would be the fourth member
of our "Halo" team.

278
00:16:54,129 --> 00:16:58,031
I don't think I need to tell you
what an honor this is.

279
00:17:00,235 --> 00:17:01,600
Oh. That's so sweet.

280
00:17:01,770 --> 00:17:03,795
But I'm going out dancing
with a girlfriend.

281
00:17:03,972 --> 00:17:06,270
You can't go out, it's "Halo" night.

282
00:17:06,642 --> 00:17:08,803
Well, for Penny, it's dancing night.

283
00:17:08,977 --> 00:17:10,945
- You go dancing every Wednesday?
- No.

284
00:17:11,113 --> 00:17:12,410
Then it's not dancing night.

285
00:17:14,316 --> 00:17:15,908
Look, why don't we play tomorrow?

286
00:17:16,085 --> 00:17:19,543
Tonight is "Halo" night.
It's like talking to a wall.

287
00:17:20,489 --> 00:17:24,084
All right, now, Sheldon,
you and I are about to have a problem.

288
00:17:25,894 --> 00:17:27,987
Sheldon, remember, we role-played this.

289
00:17:28,163 --> 00:17:30,927
Yes, but you didn't portray her
as completely irrational.

290
00:17:31,433 --> 00:17:33,367
All right, fellas, I gotta go.

291
00:17:33,535 --> 00:17:35,628
But good luck.

292
00:17:38,340 --> 00:17:42,003
Maybe we should've asked if we could go
dancing with her and her girlfriend.

293
00:17:42,177 --> 00:17:44,372
Okay, assuming we could dance,
which we can't...

294
00:17:44,546 --> 00:17:46,446
...there are three of us and two of them.

295
00:17:46,615 --> 00:17:50,915
- So?
- It's the Chinese restaurant all over again.

296
00:17:51,086 --> 00:17:54,078
I assure you that cutting
a dumpling in thirds is child's play...

297
00:17:54,256 --> 00:17:58,488
...compared with three men each attempting
to dance with 67 percent of a woman.

298
00:17:59,661 --> 00:18:03,119
For God's sakes, Sheldon,
you are driving me crazy.

299
00:18:03,298 --> 00:18:05,630
Your anger is not with me,
but with mathematics.

300
00:18:05,801 --> 00:18:07,530
I'm pretty sure my anger is with you.

301
00:18:07,703 --> 00:18:10,399
What is happening to us?
We're falling apart.

302
00:18:12,141 --> 00:18:14,609
- Who are you calling?
- The only man who can restore...

303
00:18:14,777 --> 00:18:17,268
...any semblance of balance
to our universe.

304
00:18:17,446 --> 00:18:18,913
<i>Hi, this is Howard Wolowitz.</i>

305
00:18:19,081 --> 00:18:20,605
<i>And this is Christy vanderbelt.</i>

306
00:18:20,783 --> 00:18:24,514
<i>We can't get to the phone
right now because we're having sex.</i>

307
00:18:26,622 --> 00:18:29,090
<i>You're not gonna put
that on your message, are you?</i>

308
00:18:29,258 --> 00:18:30,850
<i>No, I'm just kidding. I'll re-record it.</i>

309
00:18:34,797 --> 00:18:36,094
Sheldon, think this through.

310
00:18:36,265 --> 00:18:39,291
You're going to ask Howard
to choose between sex and "Halo. "

311
00:18:39,468 --> 00:18:43,131
No, I'm going to ask him
to choose between sex and "Halo 3."

312
00:18:43,605 --> 00:18:45,698
As far as I know,
sex has not been upgraded...

313
00:18:45,874 --> 00:18:49,037
...to include hi-def graphics
and enhanced weapon systems.

314
00:18:49,211 --> 00:18:52,840
You're right, all sex has is nudity,
orgasms and human contact.

315
00:18:53,315 --> 00:18:54,976
My point.

316
00:18:55,484 --> 00:18:59,181
I'm just saying you can take the
damn plastic off the couch once in a while.

317
00:18:59,354 --> 00:19:01,481
Why, so you
and Howard can hump on it?

318
00:19:03,358 --> 00:19:05,622
Ladies, ladies,
I'm sure there's a middle ground.

319
00:19:05,794 --> 00:19:08,058
- Shut up, Howard!
- Shut up, Howard!

320
00:19:08,797 --> 00:19:11,493
You guys talk.
I'll take my scooter out for a little spin.

321
00:19:11,667 --> 00:19:13,999
Are you happy?
You drove your son out of the house.

322
00:19:14,169 --> 00:19:15,796
Why don't you stop butting in?

323
00:19:15,971 --> 00:19:18,997
- What are you guys doing here?
- It's "Halo" night.

324
00:19:19,408 --> 00:19:23,674
He's not a man, he's a putz. And don't you
take that tone with me, you gold digger.

325
00:19:23,846 --> 00:19:25,507
- What did you call me?
- You heard me.

326
00:19:25,681 --> 00:19:28,343
And I'll tell you,
you're barking up the wrong tree.

327
00:19:28,517 --> 00:19:31,577
Because as long as you're around,
Howard is out of the will.

328
00:19:31,753 --> 00:19:34,984
You know what, I got better offers.
I'm out of here.

329
00:19:35,157 --> 00:19:38,991
That's right.
Go back to Babylon, you whore.

330
00:19:42,264 --> 00:19:44,425
So "Halo" night, huh?

331
00:19:46,335 --> 00:19:49,532
- I thought she was the whore of Omaha.
- Shh.

332
00:19:55,510 --> 00:19:58,946
Sheldon, you got him in your sights. Fire!
He's charging his plasma rifle.

333
00:19:59,114 --> 00:20:01,878
- I can't shoot now, I'm cloaking.
- Now, Raj, kill Sheldon.

334
00:20:02,050 --> 00:20:03,108
I can't see him.

335
00:20:03,285 --> 00:20:05,776
That's why they call it cloaking,
dead man.

336
00:20:05,954 --> 00:20:08,684
- Start throwing grenades.
- I'm all out.

337
00:20:09,291 --> 00:20:11,623
Hey, guys, my friends and I
got tired of dancing...

338
00:20:11,793 --> 00:20:14,023
...so we came over to have sex
with you.

339
00:20:14,196 --> 00:20:15,686
Now, Raj, jump in the tank.

340
00:20:15,864 --> 00:20:18,890
- We said no tanks.
- There are no rules in hell.

341
00:20:19,067 --> 00:20:22,264
Son of a bitch.
Med-pack! I need a med-pack!

342
00:20:23,972 --> 00:20:25,371
Told you.

343
00:20:25,540 --> 00:20:27,770
There's a sniper.
Use your rocket launcher.

344
00:20:27,943 --> 00:20:30,207
All I've got is a needler
and I'm all out of ammo!

345
00:20:30,379 --> 00:20:33,075
And now you're out of life.

346
00:20:34,683 --> 00:20:38,619
- Why did you hit pause?
- I thought I heard something.

347
00:20:38,787 --> 00:20:40,778
- What?
- No, it... Never mind.

348
00:20:40,956 --> 00:20:42,651
Sorry, go.

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
