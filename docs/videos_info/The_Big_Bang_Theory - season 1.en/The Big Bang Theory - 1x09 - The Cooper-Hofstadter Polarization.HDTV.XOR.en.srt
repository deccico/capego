1
00:00:00,529 --> 00:00:01,279
Ok...

2
00:00:03,147 --> 00:00:04,875
The X10's are online

3
00:00:05,262 --> 00:00:11,220
Gentlemen, I am now about to send a
signal from this laptop through our
local ISP

4
00:00:11,340 --> 00:00:15,373
racing down fibre optic cable at
the speed of light to San Fransisco,

5
00:00:15,493 --> 00:00:18,546
bouncing off a satellite in
geosynchronous orbit to
Lisbon, Portugal,

6
00:00:18,666 --> 00:00:21,576
where the data packets will be headed
off to submerge transatlantic cables,

7
00:00:21,606 --> 00:00:23,292
terminating in Halifax,
Nova Scotia

8
00:00:23,384 --> 00:00:26,697
and transfered across the continent
via microwave relays back to our ISP

9
00:00:26,817 --> 00:00:30,024
And the extend receiver
attached to this...

10
00:00:31,007 --> 00:00:31,860
Lamp.

11
00:00:34,844 --> 00:00:36,899
Look at me, look at me.
I've got goosebumps.

12
00:00:37,019 --> 00:00:39,804
- Are we ready for the stereo?
- Go for stereo.

13
00:01:01,556 --> 00:01:04,061
- Hey guys.
- Hello.

14
00:01:04,439 --> 00:01:05,763
It's a little loud.

15
00:01:05,888 --> 00:01:07,790
No problem... Turning it down.

16
00:01:08,436 --> 00:01:12,291
San Francisco, Lisbon, Halifax...
... et voil?

17
00:01:13,487 --> 00:01:17,282
- OK, thanks.
- Hang on, hang on. Do you not
realise what we just did?

18
00:01:17,537 --> 00:01:19,671
Yeah, you turned your stereo
down with your laptop.

19
00:01:19,996 --> 00:01:24,561
No, we turned our stereo down, by
sending a signal around the world via
the Internet.

20
00:01:25,887 --> 00:01:28,263
Oh. You know you can get one of those
universal remotes from Radioshack?

21
00:01:28,298 --> 00:01:29,742
They're really cheap.

22
00:01:31,097 --> 00:01:32,772
No, you don't get it. Howard,

23
00:01:32,898 --> 00:01:36,032
- Enable public access.
- Public access enabled.

24
00:01:39,945 --> 00:01:43,071
- Boy, that's terrific. I'll see ya.
- No hang on, hang on...

25
00:01:45,577 --> 00:01:46,462
See?

26
00:01:48,098 --> 00:01:48,731
No.

27
00:01:49,618 --> 00:01:53,431
Someone in Sichuan province,
China, is using his computer

28
00:01:53,556 --> 00:01:55,062
to turn our lights on and off.

29
00:01:56,438 --> 00:01:58,043
Oh, that's handy.

30
00:01:59,405 --> 00:02:01,392
Here's a question... Why?

31
00:02:03,098 --> 00:02:04,963
Because we can.

32
00:02:06,337 --> 00:02:08,653
They found our
remote control cars!

33
00:02:11,166 --> 00:02:14,471
- Wait, what's on top of that?
- Wireless webcams, wave hello!

34
00:02:15,446 --> 00:02:18,203
The monster truck is
out of Austin, Texas,

35
00:02:18,326 --> 00:02:21,893
and the blue Viper is being
operated from suburban Tel Aviv.

36
00:02:24,445 --> 00:02:27,071
- You may want to put on slacks.
- What?

37
00:02:29,808 --> 00:02:32,982
Ew, stop it! No!
Leave me alone!

38
00:02:36,467 --> 00:02:39,150
- Who's running the red Corvette?
- That would be me.

39
00:02:40,237 --> 00:02:46,272
<font color="#ffff00">-=www. Ydy.
com/bbs=- proudly presents</font>

40
00:03:02,305 --> 00:03:05,690
You know in the future,
when we're disembodied brains in jars,

41
00:03:05,818 --> 00:03:08,521
we're going to look at this
as eight hours well wasted.

42
00:03:10,446 --> 00:03:13,720
I don't want to be in a jar.
I want my brain in an android body.

43
00:03:15,078 --> 00:03:16,833
Eight feet tall and ribbed.

44
00:03:18,137 --> 00:03:19,241
I'm with you.

45
00:03:19,367 --> 00:03:22,772
I just have to make sure if I'm a
synthetic human I'd still be Jewish.

46
00:03:24,936 --> 00:03:26,013
I promised my mother.

47
00:03:28,287 --> 00:03:31,812
I suppose you could have your
android penis circumcised.

48
00:03:33,867 --> 00:03:37,073
But that's something your rabbi would
have to discuss with the manufacturer.

49
00:03:37,597 --> 00:03:40,370
Not to mention you'd have
to power down on saturdays.

50
00:03:42,406 --> 00:03:44,721
Sheldon, why is this
letter in the trash?

51
00:03:44,845 --> 00:03:46,481
Well, there's always
the possibility that

52
00:03:46,516 --> 00:03:49,413
a trash can spontaneously
formed around the letter

53
00:03:50,105 --> 00:03:52,882
but Occam's rasor would suggest
that someone threw it out.

54
00:03:53,966 --> 00:03:57,073
It's from the Institute of
Experimental Physics, they want us to

55
00:03:57,137 --> 00:03:59,321
present our paper on the
properties of supersolids

56
00:03:59,357 --> 00:04:02,310
at the topical conference of
Bose-Einstein condensates.

57
00:04:02,438 --> 00:04:04,573
I know.
I read it before I threw it out.

58
00:04:06,278 --> 00:04:08,490
Ok, if I may drill down to
the bedrock of my question:

59
00:04:08,617 --> 00:04:09,911
why did you throw it out?

60
00:04:10,036 --> 00:04:13,791
Because I have no interest in standing
in the Rose room of the
Pasadena Marriot,

61
00:04:13,917 --> 00:04:16,083
in front of a group of
judgmental strangers

62
00:04:16,208 --> 00:04:19,640
who wouldn't recognise true genius if it
was standing in front of them giving
a speech.

63
00:04:20,605 --> 00:04:22,171
Which if I were there,
would be.

64
00:04:23,488 --> 00:04:24,621
I don't know Sheldon.

65
00:04:24,625 --> 00:04:28,003
Those topical conferences on
Bose-Einstein condensates' parties
are legendary

66
00:04:28,838 --> 00:04:32,251
- Forget the parties!
- Forget the parties? What a nerd.

67
00:04:34,317 --> 00:04:36,933
Are there any other honours I've
got that I don't know about?

68
00:04:37,057 --> 00:04:39,863
Did the UPS dropped off a
Nobel prize with my name on it?

69
00:04:40,146 --> 00:04:43,223
Leonard, please don't take this the
wrong way, but the day you win a
Nobel prize,

70
00:04:43,256 --> 00:04:47,331
is the day I begin my research on the
drag coefficient of tassles on
flying carpets.

71
00:04:49,656 --> 00:04:52,182
The only thing missing from
that insult was: "your mama".

72
00:04:53,977 --> 00:04:54,960
I got one... Hey Leonard,

73
00:04:55,087 --> 00:04:58,392
- Your mama's research methodology is so flawed...
- Shut up, Howard.

74
00:04:59,686 --> 00:05:02,010
- Sheldon, we have to do this.
- No, we don't.

75
00:05:02,068 --> 00:05:04,410
We have to take a nourishment,
to expel waste,

76
00:05:04,476 --> 00:05:07,062
and inhale enough oxygen to
keep our cells from dying.

77
00:05:07,157 --> 00:05:08,572
Everything else is optional.

78
00:05:09,627 --> 00:05:11,641
Ok, let me put it this way:
I'm doing it.

79
00:05:11,737 --> 00:05:13,972
You can't. I'm the lead author.

80
00:05:14,178 --> 00:05:17,823
Come on, the only reason you're the lead
author is because we
went alphabetically.

81
00:05:17,855 --> 00:05:20,370
I let you think we went
alphabetically to spare you

82
00:05:20,408 --> 00:05:22,800
the humiliation with dealing with
the fact that it was my idea.

83
00:05:22,836 --> 00:05:25,923
Now to put too fine a point to it,
but I was throwing you a bone.

84
00:05:26,805 --> 00:05:27,800
You're welcome.

85
00:05:28,328 --> 00:05:31,773
Excuse me, I designed the experiment
that proved the hypothesis.

86
00:05:31,907 --> 00:05:33,652
That doesn't mean proving.

87
00:05:33,945 --> 00:05:37,320
So the entire scientific community
is just supposed to take your word?

88
00:05:37,387 --> 00:05:39,342
They're not supposed to,
but they should.

89
00:05:40,428 --> 00:05:42,050
Alright, I don't
care what you say.

90
00:05:42,118 --> 00:05:44,432
I'm going to the conference
and I'm presenting our findings

91
00:05:44,488 --> 00:05:46,352
And I forbid it.

92
00:05:47,118 --> 00:05:48,343
You forbid it?

93
00:05:48,405 --> 00:05:50,973
If I'm not taking credit
for our work then nobody is.

94
00:05:51,036 --> 00:05:53,262
- So, you admit that it's our work.
- No.

95
00:05:53,337 --> 00:05:55,402
Once again,
I'm throwing you a bone.

96
00:05:56,216 --> 00:05:57,920
And once again,
you are welcome.

97
00:06:07,076 --> 00:06:10,152
So, how's it going with Sheldon? Are you
guys still not talking to each other?

98
00:06:10,247 --> 00:06:11,882
Not only is he still
not talking to me,

99
00:06:11,946 --> 00:06:15,660
but there's this thing he does where he
stares at you and tries to get your
brain to explode.

100
00:06:17,547 --> 00:06:20,720
You know, like in the classic
sci-fi movie "Scanners"?

101
00:06:21,428 --> 00:06:22,943
Like... . Bzzz.

102
00:06:24,767 --> 00:06:25,973
Nevermind.

103
00:06:26,168 --> 00:06:27,672
How about this one?

104
00:06:28,146 --> 00:06:31,710
It says, I know my physics,
but I'm still a fun guy.

105
00:06:32,388 --> 00:06:35,523
Oh, I didn't know they
still made corduroy suits.

106
00:06:35,597 --> 00:06:37,810
They don't,
that's why I saved this one.

107
00:06:38,657 --> 00:06:41,782
Ok, well,
let's just see what else you have.

108
00:06:42,145 --> 00:06:44,040
Ok, here. Take this.

109
00:06:44,216 --> 00:06:48,103
And this, and this,
and this, and these...

110
00:06:48,208 --> 00:06:49,882
Is this all stuff you
want me to try on?

111
00:06:49,918 --> 00:06:51,721
No, this is stuff I
want you to throw out.

112
00:06:52,877 --> 00:06:56,483
Seriously, don't even give it to
charity, you won't be helping anyone.

113
00:06:57,348 --> 00:06:58,892
What's this?

114
00:06:59,235 --> 00:07:01,710
That's the bottled
city of Kandor.

115
00:07:04,575 --> 00:07:08,593
You see, Kandor was the capital
city of the planet Krypton.

116
00:07:08,745 --> 00:07:11,413
It was miniaturised by Brainiac
before Krypton exploded

117
00:07:11,456 --> 00:07:13,310
and then rescued by Superman.

118
00:07:14,478 --> 00:07:16,493
Oh, nice.

119
00:07:17,668 --> 00:07:20,393
It's a lot cooler when
girls aren't looking at it.

120
00:07:21,336 --> 00:07:24,550
Here, why don't you put these while
I find a shirt and a sport cut match.

121
00:07:24,586 --> 00:07:25,593
Right, be right back.

122
00:07:25,637 --> 00:07:27,370
Where are you going?
Just put them on.

123
00:07:27,425 --> 00:07:28,390
Here?

124
00:07:28,465 --> 00:07:30,202
Oh, are you shy?

125
00:07:30,366 --> 00:07:32,873
- No, I'm not shy.
- Don't worry I won't look.

126
00:07:33,038 --> 00:07:34,721
I know you won't look.
Why would you look?

127
00:07:34,775 --> 00:07:36,250
There's nothing to see...
Well, not 'nothing'...

128
00:07:36,288 --> 00:07:38,733
Sweetie, put the pants on.

129
00:07:40,507 --> 00:07:43,193
So, you know, isn't there maybe
some way you and Sheldon could

130
00:07:43,236 --> 00:07:46,673
- compromise on this whole presentation thing?
- No, No.

131
00:07:46,816 --> 00:07:48,493
Scientists do not compromise.

132
00:07:48,528 --> 00:07:50,612
Our minds are trained
to synthetize facts

133
00:07:50,655 --> 00:07:52,533
and come to
inarguable conclusions.

134
00:07:52,798 --> 00:07:55,701
Not to mention,
Sheldon is back-crap crazy.

135
00:07:58,817 --> 00:08:00,382
What is this?

136
00:08:00,527 --> 00:08:02,480
Oh, careful.

137
00:08:02,937 --> 00:08:06,431
That's my original series
Battlestar Galactica flight suit.

138
00:08:06,897 --> 00:08:09,130
Oh, why didn't you
wear it on Halloween?

139
00:08:09,508 --> 00:08:11,501
Because it's not a costume,
it's a flight suit.

140
00:08:14,815 --> 00:08:16,910
Ok, alright, moving on.

141
00:08:17,017 --> 00:08:19,243
Oh, wow. A paisley shirt.

142
00:08:19,286 --> 00:08:21,953
It goes with my corduroy suit.

143
00:08:22,308 --> 00:08:25,200
If you mean it should end up in
the same place, then I agree.

144
00:08:26,604 --> 00:08:28,759
Is this your only tie?

145
00:08:29,799 --> 00:08:32,674
Technically, yes.
But, if you'll notice...

146
00:08:33,526 --> 00:08:35,371
It's reversible!

147
00:08:37,539 --> 00:08:39,220
So it works as two.

148
00:08:39,297 --> 00:08:42,193
Sweetie, I don't even
think it works as one.

149
00:08:44,005 --> 00:08:45,662
- Is this all your clothes?
- Eh, yeah.

150
00:08:45,695 --> 00:08:47,580
Everything since
the eighth grade.

151
00:08:47,705 --> 00:08:50,523
- The eighth grade?
- My last growth sprout

152
00:08:52,708 --> 00:08:55,722
Ok, well,
let's go back to the curdoroy suit.

153
00:08:55,768 --> 00:08:57,713
- Great.
- Yeah.

154
00:08:59,385 --> 00:09:01,222
I said no. Put it down.

155
00:09:07,135 --> 00:09:09,160
Hey Sheldon.

156
00:09:09,427 --> 00:09:10,681
Hello Penny.

157
00:09:12,197 --> 00:09:14,113
Get anything good?

158
00:09:15,758 --> 00:09:19,211
Just the latest copy of Applied
Particle Physics quarterly.

159
00:09:19,276 --> 00:09:22,523
Oh, you know, that is so weird
that yours came and mine didn't.

160
00:09:28,307 --> 00:09:29,880
It was a joke.

161
00:09:34,146 --> 00:09:36,033
Yep.

162
00:09:36,245 --> 00:09:38,691
Tip you waitresses.
I'm here all week.

163
00:09:39,715 --> 00:09:41,841
Penny, just to save you
from further awkwardness,

164
00:09:41,915 --> 00:09:45,360
know that I'm perfectly comfortable with
the two of us climbing the stairs
in silence.

165
00:09:45,615 --> 00:09:48,152
Oh, yeah ok, me too.
Zip it, lock it.

166
00:09:50,895 --> 00:09:52,083
Put it in your pocket.

167
00:09:56,486 --> 00:09:58,991
- So, you and Leonard...
- Oh dear God...

168
00:09:59,695 --> 00:10:01,292
Little misunderstanding, huh?

169
00:10:01,818 --> 00:10:03,141
A little misunderstanding... ?

170
00:10:03,385 --> 00:10:06,500
Galileo and the Pope had
a little misunderstanding.

171
00:10:10,166 --> 00:10:13,993
Anyway, I was talking to Leonard this
morning and I think he feels really bad
about it.

172
00:10:16,368 --> 00:10:17,711
How do you feel?

173
00:10:19,075 --> 00:10:20,521
I don't understand
the question.

174
00:10:21,978 --> 00:10:25,280
I'm just asking if it's difficult to
be fighting with your best friend.

175
00:10:26,726 --> 00:10:28,590
I haven't thought
about it like that.

176
00:10:28,878 --> 00:10:31,762
I wonder if I've been experiencing
physiological manifestations

177
00:10:31,886 --> 00:10:34,433
of some sort of unconscious
emotional turmoil

178
00:10:35,826 --> 00:10:36,851
Wait, what?

179
00:10:37,078 --> 00:10:38,812
I couldn't poop this morning.

180
00:10:43,348 --> 00:10:46,270
You should just talk to him,
I'm sure you guys can work this out.

181
00:10:46,655 --> 00:10:49,433
- Certainly preferable to my plan.
- Which was?

182
00:10:49,827 --> 00:10:51,243
A powerful laxative.

183
00:10:53,468 --> 00:10:55,623
You absolutely
should talk to him.

184
00:10:55,807 --> 00:10:58,401
Look, I know Leonard values you as
a friend and he told me himself

185
00:10:58,427 --> 00:11:01,981
without your little idea there's no way
he could come up with this whole
experiment thing.

186
00:11:06,236 --> 00:11:07,943
Excuse me. "Little idea"?

187
00:11:08,197 --> 00:11:09,963
Yeah, I mean he tried
to explain it to me,

188
00:11:10,088 --> 00:11:11,833
- I didn't really understand it but...
- Of course you didn't.

189
00:11:11,868 --> 00:11:13,411
He said: "Little idea"?

190
00:11:14,957 --> 00:11:16,660
Oh, well, no... Not...

191
00:11:16,847 --> 00:11:19,170
- Not in those words.
- In what words then, exactly?

192
00:11:19,636 --> 00:11:22,683
Oh, you know... Gee. The exact words
are... It's more the spirit in which
he said...

193
00:11:22,718 --> 00:11:25,133
- What did he say?
- You had a lucky hunch

194
00:11:26,987 --> 00:11:29,220
Hey, Sheldon, I've been thinking,
instead of arguing about this...

195
00:11:29,255 --> 00:11:31,220
Don't you ever
speak to me again.

196
00:11:33,806 --> 00:11:34,812
What... ?

197
00:11:47,998 --> 00:11:50,023
Ok, I'm leaving
for the conference.

198
00:11:50,385 --> 00:11:52,720
Have fun presenting
my "lucky hunch"

199
00:11:52,845 --> 00:11:54,553
Shel, I didn't
mean it like that.

200
00:11:54,676 --> 00:11:56,430
- Then why did you say it?
- I don't know, I wasn't...

201
00:11:56,466 --> 00:12:00,203
- Were you trying to impress Penny?
- No, no, not at all. A little bit.

202
00:12:01,486 --> 00:12:03,253
How did that work out for you?

203
00:12:03,545 --> 00:12:07,060
- Leonard, ready to go?
- Libido: 1 Truth: 0.

204
00:12:09,926 --> 00:12:11,283
Ok, I'm gonna ask
you one more time.

205
00:12:11,317 --> 00:12:13,842
We did the work together,
let's present the paper together.

206
00:12:13,968 --> 00:12:15,772
And I'm telling you
for the last time,

207
00:12:15,895 --> 00:12:18,720
it's pandering,
it's undignifying, and bite me.

208
00:12:21,115 --> 00:12:22,120
Let's go.

209
00:12:22,368 --> 00:12:24,240
- Bye Sheldon.
- Goodbye Penny.

210
00:12:35,466 --> 00:12:37,022
One of these days... bshh!

211
00:12:40,585 --> 00:12:41,580
There you go.

212
00:12:41,885 --> 00:12:43,481
You're right this side
does look better.

213
00:12:43,625 --> 00:12:46,391
No, no, I didn't say better,
I said "less stained".

214
00:12:48,507 --> 00:12:51,383
I just checked the house.
There's probably 20-25 people in there.

215
00:12:51,505 --> 00:12:53,000
- You're kidding!
- Is that all?

216
00:12:53,195 --> 00:12:56,520
"All? " In particle
physics 25 is Woodstock!

217
00:12:59,866 --> 00:13:01,900
- Then good!
- I wasn't expecting such a crowd,

218
00:13:02,025 --> 00:13:03,030
I'm all nervous.

219
00:13:03,155 --> 00:13:05,322
It's ok, just open with a joke,
you'll be fine.

220
00:13:05,648 --> 00:13:07,541
A joke... Ok.

221
00:13:08,968 --> 00:13:10,453
How about this? Um, ok...

222
00:13:10,578 --> 00:13:12,021
There's this farmer

223
00:13:12,296 --> 00:13:16,071
and he has these chickens but
they won't lay any eggs, so...

224
00:13:16,385 --> 00:13:18,192
He calls a physicist to help.

225
00:13:18,415 --> 00:13:21,141
The physicist then does
some calculations,

226
00:13:21,397 --> 00:13:24,733
and he says:
"I have a solution,

227
00:13:25,095 --> 00:13:29,302
but it only works for spherical
chickens in a vacuum".

228
00:13:31,866 --> 00:13:32,871
Right?

229
00:13:37,726 --> 00:13:39,803
Oh, sorry,
I just had heard it before.

230
00:13:40,517 --> 00:13:43,562
- Let's roll. Hey, nice suit.
- It's a classic, right?

231
00:13:45,046 --> 00:13:47,440
I really should have
brought my own car.

232
00:13:50,067 --> 00:13:51,651
So, in conclusion,

233
00:13:51,776 --> 00:13:54,553
the data show that at temperatures
approaching absolute zero,

234
00:13:54,677 --> 00:13:58,171
the moment of inertia changes,
and a solid becomes a supersolid,

235
00:13:58,295 --> 00:14:02,313
which clearly appears to be a
previously unknown state of matter.

236
00:14:05,217 --> 00:14:06,302
Thank you!

237
00:14:11,468 --> 00:14:13,513
- Are there any questions?
- Yeah.

238
00:14:13,746 --> 00:14:15,511
What the hell was that?

239
00:14:18,158 --> 00:14:19,242
Any other questions?

240
00:14:19,846 --> 00:14:24,390
Doctor Sheldon Cooper here, I am the
lead author of this particular paper.

241
00:14:26,088 --> 00:14:27,121
Thank you.

242
00:14:29,137 --> 00:14:33,522
And you sir, you have completely skipped
over the part where I was walking
through the park

243
00:14:33,788 --> 00:14:36,910
and I saw these children on a
merry-go-round which started me thinking

244
00:14:37,036 --> 00:14:39,832
about the moment of inertia
in gases like helium

245
00:14:39,957 --> 00:14:41,890
at temperatures
approaching absolute zero.

246
00:14:42,018 --> 00:14:44,810
I didn't skip it.
It's just an anecdote, it's not science.

247
00:14:44,937 --> 00:14:47,921
Oh, I see. It was the apple
falling on Newton's head,

248
00:14:48,048 --> 00:14:50,613
- was that just an anecdote?
- You are not Isaac Newton.

249
00:14:50,736 --> 00:14:54,050
No, no, that's true. Gravity would have
been apparent to me without the apple.

250
00:14:54,846 --> 00:14:56,541
You cannot possibly
be that arrogant.

251
00:14:56,665 --> 00:14:59,023
You continue to underestimate me,
my good man.

252
00:14:59,616 --> 00:15:03,482
Look, if you weren't happy with my
presentation then maybe you should have
given it with me!

253
00:15:03,508 --> 00:15:05,533
As I have explained repeatedly,
unlike you,

254
00:15:05,557 --> 00:15:07,991
I don't need validation
from lesser minds.

255
00:15:08,026 --> 00:15:08,843
No offense.

256
00:15:09,408 --> 00:15:10,612
Really, so why did you come?

257
00:15:10,637 --> 00:15:12,071
Because I knew
you'd screw this up.

258
00:15:12,095 --> 00:15:13,420
I didn't screw it up!

259
00:15:13,445 --> 00:15:15,853
Oh, please.
I admit that spherical chicken joke,

260
00:15:15,878 --> 00:15:16,871
- that was hilarious.
- Thank you.

261
00:15:16,907 --> 00:15:18,801
But it was straight
downhill from there.

262
00:15:19,015 --> 00:15:21,101
I've had enough of
your condescendship.

263
00:15:21,205 --> 00:15:24,172
Maybe I didn't go to college
when I was 11 like you.

264
00:15:24,187 --> 00:15:27,672
Maybe I got my doctorate
at 24 instead of 16

265
00:15:27,698 --> 00:15:29,380
but you are not the only
person who is smarter than

266
00:15:29,415 --> 00:15:30,800
everyone else in this room!

267
00:15:32,185 --> 00:15:32,791
No offense.

268
00:15:34,867 --> 00:15:36,621
And I am clearly
not the only person

269
00:15:36,656 --> 00:15:38,413
who is tormented by insecurity

270
00:15:38,437 --> 00:15:40,542
and has an ego in need
of constant validation!

271
00:15:40,688 --> 00:15:42,283
So you admit you're an egotist?

272
00:15:42,518 --> 00:15:43,293
Yes!

273
00:15:44,706 --> 00:15:47,681
My name is doctor Leonard Hofstadter
and I can never please my parents so

274
00:15:47,696 --> 00:15:50,060
I need all my self esteem
from strangers like you!

275
00:15:50,186 --> 00:15:51,161
But he's worse!

276
00:15:51,726 --> 00:15:53,071
Ok, that's it!

277
00:15:53,465 --> 00:15:54,021
Stop it!

278
00:15:56,726 --> 00:15:59,322
You cannot blow up my
head with your mind!

279
00:15:59,535 --> 00:16:01,252
Then I'll settle
for your aneurism!

280
00:16:01,817 --> 00:16:03,442
- Stop it!
- You hit me!

281
00:16:03,545 --> 00:16:04,691
You saw that he hit me!

282
00:16:04,868 --> 00:16:06,320
You tried to blow up my head!

283
00:16:06,356 --> 00:16:07,263
So it was working!

284
00:16:07,295 --> 00:16:09,641
It wa... It was not!
You're a nutcase!

285
00:16:09,665 --> 00:16:10,623
We'll see about that!

286
00:16:10,766 --> 00:16:12,092
Heads up,
you people in the front row!

287
00:16:12,125 --> 00:16:13,352
This is a splash zone!

288
00:16:14,508 --> 00:16:15,161
Stop it!

289
00:16:15,658 --> 00:16:16,172
Just quit it!

290
00:16:22,306 --> 00:16:24,542
Is this usually how
these physics things go?

291
00:16:24,987 --> 00:16:26,221
More often than you think.

292
00:16:42,847 --> 00:16:44,623
You could have offered
me a ride home.

293
00:16:46,557 --> 00:16:48,290
You're lucky I
didn't run you over.

294
00:16:49,408 --> 00:16:51,271
I really don't understand
what you're so unhappy about.

295
00:16:51,305 --> 00:16:52,950
You begged me to come, I came.

296
00:16:52,976 --> 00:16:54,153
There's just no pleasing you.

297
00:16:55,156 --> 00:16:57,711
You're right, I'm the problem,
I'm the one that needs help.

298
00:16:58,206 --> 00:17:00,380
Well, that's not much of an
apology but I'll take it.

299
00:17:01,578 --> 00:17:02,780
Excuse me,

300
00:17:03,325 --> 00:17:05,592
is there anything you
would apologise for?

301
00:17:07,198 --> 00:17:07,812
Yes.

302
00:17:08,958 --> 00:17:11,111
I'm sorry I tried to
blow up your head.

303
00:17:13,086 --> 00:17:14,182
It was uncalled for.

304
00:17:15,625 --> 00:17:16,601
You won't believe this.

305
00:17:16,637 --> 00:17:19,511
Somebody got the whole thing with
their cell phone and put it on YouTube!

306
00:17:20,696 --> 00:17:21,270
What?

307
00:17:22,966 --> 00:17:25,213
- Who would do that?
- That would be me.

308
00:17:27,008 --> 00:17:28,713
Hey, check it out,
it's a featured video!

309
00:17:28,817 --> 00:17:30,733
He hit me!
You saw that he hit me!

310
00:17:31,836 --> 00:17:34,801
- Then it was working!
- It was not working! You're a nutcase!

311
00:17:34,826 --> 00:17:35,842
We will see about that!

312
00:17:35,898 --> 00:17:37,150
You people in the
front row heads up!

313
00:17:37,186 --> 00:17:38,452
This is splash zone

314
00:17:40,057 --> 00:17:42,640
Stop it! Leave it, leave it!

315
00:17:46,146 --> 00:17:47,680
You want a volcano nerve pinch!

316
00:17:49,325 --> 00:17:51,761
You should clip your fingernails!
Those hurt!

317
00:17:53,327 --> 00:17:56,452
Oh, Jeez.
does this suit really look that bad?

318
00:17:57,585 --> 00:17:59,440
Forget your suit,
look at my arms waving.

319
00:17:59,446 --> 00:18:01,071
I'm like a flamingo on Ritalin.

320
00:18:04,196 --> 00:18:04,882
Howard,

321
00:18:05,115 --> 00:18:07,542
would you like to explain to
me why your Facebook page

322
00:18:07,556 --> 00:18:09,830
has a picture of me sleeping
on your shoulder captioned

323
00:18:09,845 --> 00:18:11,291
"me and my girlfriend"?

324
00:18:13,638 --> 00:18:15,661
Uh oh, here comes the talk.

325
00:18:24,697 --> 00:18:26,453
- You hit me!
- You tried to blow up my head!

326
00:18:26,497 --> 00:18:29,491
- So it was working!
- It was not working! You're a nutcase!

327
00:18:30,558 --> 00:18:32,482
What losers

328
00:18:32,856 --> 00:18:35,480
Yeah Gigantic American geeks

329
00:18:38,457 --> 00:18:41,183
who's doing that?

330
00:18:41,457 --> 00:18:46,283
Someone from Pasadena,
California named...

331
00:18:46,955 --> 00:18:50,881
"Wolowizard"

332
00:18:51,156 --> 00:18:53,882
Awesome!

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
