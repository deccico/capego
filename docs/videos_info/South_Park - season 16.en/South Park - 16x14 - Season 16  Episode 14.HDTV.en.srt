1
00:00:04,644 --> 00:00:07,549
<font color="#3399FF">Sync and correction by Nitin</font>

2
00:00:38,709 --> 00:00:43,103
Okay Mister Thompson,
voting line is right over there.

3
00:00:43,642 --> 00:00:46,421
- Next, please.
- Mom?

4
00:00:46,587 --> 00:00:49,487
Mommy?
Where's my mommy?

5
00:00:49,612 --> 00:00:52,651
Oh dear,
did you lose your mother, little boy?

6
00:00:52,972 --> 00:00:55,520
- Mom!
- Security!

7
00:00:57,014 --> 00:01:00,559
- Huh?
- This little boy lost his mom!

8
00:01:00,684 --> 00:01:04,325
She said to wait for her
by the plastic boxes if I got lost!

9
00:01:04,708 --> 00:01:06,854
It's alright come on.

10
00:01:21,052 --> 00:01:23,804
Mom, where's my mom?

11
00:01:23,929 --> 00:01:26,920
Oh dear, did you lose your mommy,
little boy?

12
00:01:31,647 --> 00:01:33,262
Would you like some warm nuts, sir?

13
00:01:33,387 --> 00:01:36,202
Ha ha haa! Warm nuts she says!

14
00:01:43,595 --> 00:01:46,940
Mom!
I lost my mom!

15
00:01:51,208 --> 00:01:53,442
Warm nuts, sir?
Warm nuts?

16
00:01:54,309 --> 00:01:57,034
No way, dude.
No way.

17
00:02:01,459 --> 00:02:04,791
Mom!
I can't find my mom!

18
00:02:07,794 --> 00:02:11,298
- You like warm nuts, sir?
- Ahah! Okay, okay, it's getting old.

19
00:02:15,736 --> 00:02:17,372
Mom!

20
00:02:17,497 --> 00:02:19,014
Mom!

21
00:02:25,868 --> 00:02:27,564
<i>The people have spoken</i>

22
00:02:27,730 --> 00:02:32,152
and the President of the United States
is once again, Barack Obama.

23
00:02:32,318 --> 00:02:35,352
I assure you all that I am
heading back to the White House

24
00:02:35,477 --> 00:02:39,076
more motivated,
more titillated than ever.

25
00:02:41,253 --> 00:02:43,401
Don't be sad, ike.
Just be happy that you live in a

26
00:02:43,526 --> 00:02:45,668
country where people get to
elect a president at all.

27
00:02:48,355 --> 00:02:50,504
- Hello?
- Hey, Kyle, what's up?

28
00:02:50,804 --> 00:02:52,422
Nothing,
watching the election results.

29
00:02:52,588 --> 00:02:54,288
Yeah, I figured.
It's the day after the election

30
00:02:54,413 --> 00:02:55,792
so you're probably sitting on
the couch telling your little

31
00:02:55,917 --> 00:02:58,011
brother how great it is to
live in a Democratic society.

32
00:02:59,109 --> 00:03:00,778
Listen, Kyle, could you come over
for a second.

33
00:03:00,903 --> 00:03:02,099
I've got something
I need to show you.

34
00:03:02,265 --> 00:03:03,487
- What?
- Please, Kyle.

35
00:03:03,612 --> 00:03:05,268
It's kind of important.

36
00:03:07,019 --> 00:03:08,771
Alright,
what is this all about, Fat Ass?

37
00:03:08,896 --> 00:03:10,958
You happy with the election
results last night, Kyle?

38
00:03:11,083 --> 00:03:12,359
It doesn't matter if I am or not.

39
00:03:12,525 --> 00:03:14,611
People voted and I stand
behind the President.

40
00:03:15,024 --> 00:03:17,237
Oh, it's such a Democratic
thing to say, Kyle.

41
00:03:17,362 --> 00:03:18,772
What if I were to tell you...

42
00:03:18,897 --> 00:03:22,160
that I have something in my room
that could change the entire election?

43
00:03:22,326 --> 00:03:24,454
- Sure you do.
- What if I did, Kyle?

44
00:03:24,620 --> 00:03:26,243
What could you possibly
have in your room

45
00:03:26,368 --> 00:03:27,791
that could change
the outcome of the election?

46
00:03:34,453 --> 00:03:35,841
Pretty sweet, huh?

47
00:03:36,235 --> 00:03:37,481
What the hell is this?

48
00:03:37,606 --> 00:03:38,468
What's it look like?

49
00:03:38,634 --> 00:03:41,388
Hundreds of thousands of votes
from all the swing states.

50
00:03:41,803 --> 00:03:42,889
I don't believe it.

51
00:03:43,055 --> 00:03:44,761
No, really,
there are states full of swingers.

52
00:03:44,886 --> 00:03:46,143
Bunch of perverts if you ask me.

53
00:03:46,309 --> 00:03:47,477
Why do you have these?

54
00:03:47,643 --> 00:03:49,547
Funny how voting works in this country,
isn't it, Kyle.

55
00:03:49,672 --> 00:03:53,306
Each one of these a person...
someone who took the time to get

56
00:03:53,431 --> 00:03:56,206
themselves informed,
actually got up and drove to a voting

57
00:03:56,331 --> 00:03:57,904
area to make sure
their voice was heard.

58
00:04:01,365 --> 00:04:02,211
Dude!

59
00:04:02,437 --> 00:04:04,253
Here's another patriotic American.

60
00:04:04,378 --> 00:04:06,855
He probably spent hours listening
to all those presidential ads

61
00:04:06,980 --> 00:04:08,417
and tuned in to every debate.

62
00:04:09,373 --> 00:04:10,267
Knock it off, Cartman!

63
00:04:10,392 --> 00:04:12,836
Now believe it or not, Kyle.
I actually need your help.

64
00:04:12,961 --> 00:04:15,160
But first,
you have to promise not to tell anyone.

65
00:04:15,285 --> 00:04:17,103
You're not getting away with this,
you fat turd!!

66
00:04:27,568 --> 00:04:30,995
Run now, little firefly.
It's all part of the plan.

67
00:04:31,723 --> 00:04:34,942
Sweetie, there's a Mr. Pun Lee Tsao
on the phone for you.

68
00:04:35,067 --> 00:04:37,881
Thanks Mom.
I'll take that... in the study.

69
00:04:38,006 --> 00:04:40,218
Do I have a study?
I don't think I have a study.

70
00:04:40,343 --> 00:04:41,531
That's fine.

71
00:04:45,159 --> 00:04:46,661
Mr. President.

72
00:04:46,827 --> 00:04:47,464
Yes.

73
00:04:47,589 --> 00:04:49,548
Line two from China for you, sir.

74
00:04:49,673 --> 00:04:51,416
It's General Tsao.

75
00:04:53,163 --> 00:04:54,544
Yes, hello, General.

76
00:04:54,710 --> 00:04:58,048
We presume you are pleased
with the election results?

77
00:04:58,214 --> 00:05:00,383
Very pleased.
Thank you for your help.

78
00:05:00,549 --> 00:05:04,554
And you, no doubt, will now begin
filling your side of the bargain.

79
00:05:04,720 --> 00:05:07,300
You'll have what you want.
I'll meet you in three hours.

80
00:05:09,262 --> 00:05:11,567
Sir, the election may have gone
the way they said,

81
00:05:11,692 --> 00:05:14,731
but we can't possibly give
the Chinese what you promised.

82
00:05:15,207 --> 00:05:18,169
I don't know how they did it
but the Chinese secured my victory.

83
00:05:18,294 --> 00:05:20,153
I have to fulfill my obligation.

84
00:05:20,392 --> 00:05:22,632
And what if General Tsao
suddenly gets the courage

85
00:05:22,757 --> 00:05:24,402
to go to the press with all this?

86
00:05:24,527 --> 00:05:25,627
Don't worry.

87
00:05:25,958 --> 00:05:28,331
Everyone knows General Tsao's chicken.

88
00:05:34,596 --> 00:05:35,585
Eric Cartman?

89
00:05:35,751 --> 00:05:38,880
We have a report you might
be involved in voter fraud.

90
00:05:39,250 --> 00:05:40,505
Mmm, no.

91
00:05:40,881 --> 00:05:42,592
You mind if we search your room?

92
00:05:43,050 --> 00:05:44,299
I'm sorry, but I know my rights.

93
00:05:44,424 --> 00:05:45,922
You can't search my room
without a warrant,

94
00:05:46,047 --> 00:05:47,722
and I'm afraid I can't
give you permission.

95
00:05:47,888 --> 00:05:49,402
This isn't a joke, Cartman!

96
00:05:49,527 --> 00:05:50,896
You have to believe me, Officer,

97
00:05:51,021 --> 00:05:52,565
the outcome of the election
depends on it.

98
00:05:52,816 --> 00:05:54,104
Let's just see.

99
00:05:54,270 --> 00:05:55,689
Hey!
You can't do that!

100
00:05:56,504 --> 00:05:58,108
Upstairs!
First door on the left!

101
00:05:58,969 --> 00:06:00,801
I know my rights!
Stop right now!

102
00:06:05,808 --> 00:06:08,285
But... they were here.

103
00:06:08,451 --> 00:06:09,341
I swear it!

104
00:06:09,466 --> 00:06:12,038
Stacks and stacks of ballots
from states all over the country!

105
00:06:12,204 --> 00:06:16,167
I was always told that we lived
in a country based on freedom.

106
00:06:16,333 --> 00:06:18,076
That the one thing that made
America different was

107
00:06:18,201 --> 00:06:20,498
that the government could never barge
into our lives unwarranted.

108
00:06:20,623 --> 00:06:22,088
But it's all changing, isn't it?

109
00:06:22,556 --> 00:06:24,571
Ever since Obama was first elected,

110
00:06:24,696 --> 00:06:25,955
it's all changing.

111
00:06:26,734 --> 00:06:28,439
Oh my God, what have we done?

112
00:06:28,564 --> 00:06:30,223
I guess this country is changing.

113
00:06:30,389 --> 00:06:32,796
We're sorry, little boy.
We're so sorry.

114
00:06:33,476 --> 00:06:34,813
Please, you've got to believe me!

115
00:06:34,938 --> 00:06:36,039
They've still got to
be here somewhere!

116
00:06:36,164 --> 00:06:37,383
We have to find them!

117
00:06:38,426 --> 00:06:39,682
But where could they be?

118
00:06:39,807 --> 00:06:40,648
Where?

119
00:06:40,773 --> 00:06:43,132
And why would I care so much
about the election results?

120
00:06:43,257 --> 00:06:44,964
So many questions, Kyle.

121
00:06:45,089 --> 00:06:46,910
So little time.

122
00:06:53,425 --> 00:06:55,402
Guys! You guys!

123
00:06:55,751 --> 00:06:56,491
Listen,

124
00:06:56,616 --> 00:06:59,113
Cartman has tens of thousands
of voter ballots hidden somewhere!

125
00:06:59,238 --> 00:07:01,223
He's changed the outcome of
the election!

126
00:07:01,763 --> 00:07:03,506
Wow, really.
That's pretty impressive.

127
00:07:03,672 --> 00:07:06,267
Come on you guys, he's hid them
somewhere we have to find them.

128
00:07:06,392 --> 00:07:08,498
But I thought Obama won
pretty easily last night.

129
00:07:08,623 --> 00:07:10,034
Dude, because of the electoral college

130
00:07:10,159 --> 00:07:11,980
these votes in swing states
can really matter!

131
00:07:12,105 --> 00:07:13,498
I don't understand that stuff at all.

132
00:07:13,623 --> 00:07:15,044
I need Morgan Freeman
to explain it to me.

133
00:07:15,169 --> 00:07:17,437
Yeah, I love when Morgan Freeman
explains stuff.

134
00:07:17,603 --> 00:07:19,712
Whenever I'm confused about
what's going on in a movie

135
00:07:19,837 --> 00:07:23,271
I'm always so relieved when Morgan Freeman
shows up and explains the plot to me.

136
00:07:23,396 --> 00:07:25,024
God dammit,
this is serious, you guys!

137
00:07:25,149 --> 00:07:26,839
Cartman has stolen the election!

138
00:07:26,964 --> 00:07:28,784
Well, maybe Cartman had a good reason.

139
00:07:30,598 --> 00:07:33,261
Butters, you know something,
don't you? Huh?

140
00:07:33,386 --> 00:07:34,704
Uh, no!
Why?

141
00:07:34,870 --> 00:07:37,073
- What did Cartman tell you?!
- He didn't tell me nothing!

142
00:07:37,198 --> 00:07:39,545
And I swore to secrecy!
It's really important, Kyle,

143
00:07:39,670 --> 00:07:41,064
and it isn't what you think.

144
00:07:41,189 --> 00:07:44,255
- Don't make me say any more.
- Tell us what you know, right now.

145
00:07:44,421 --> 00:07:46,067
I can't, don't you see?

146
00:07:48,053 --> 00:07:50,553
Don't make me swallow this!
I'll do it!

147
00:07:51,068 --> 00:07:53,094
- Is that an M&M?
- It's an almond M&M!

148
00:07:53,219 --> 00:07:55,544
I'm very allergic to almonds!
Please!

149
00:07:55,669 --> 00:07:56,893
Just leave me alone!

150
00:07:57,059 --> 00:07:58,478
Then you have to tell us.

151
00:07:59,796 --> 00:08:01,909
Well... huh...

152
00:08:03,941 --> 00:08:05,598
Oh, shit!
Don't let him swallow it!

153
00:08:05,723 --> 00:08:07,821
- Pry his mouth open!
- He ate it!

154
00:08:08,307 --> 00:08:09,675
Butters,
where are the ballots going?

155
00:08:09,800 --> 00:08:10,824
Where are they going?

156
00:08:10,990 --> 00:08:14,449
I... I... I hate almonds.

157
00:08:28,544 --> 00:08:31,309
- More melted butter, sir?
- Mmm, mmm.

158
00:08:33,611 --> 00:08:34,703
Little boy,

159
00:08:34,828 --> 00:08:37,420
the President is on his way
to pick up the ballots.

160
00:08:37,545 --> 00:08:38,476
Where are they?

161
00:08:39,561 --> 00:08:40,452
They're nearby.

162
00:08:40,577 --> 00:08:41,783
Very safe and sound.

163
00:08:42,205 --> 00:08:45,150
I just might want to
alter our deal a lite bit.

164
00:08:46,365 --> 00:08:47,763
Alter our...

165
00:08:47,888 --> 00:08:50,488
the President will be here in moments
with what we want!

166
00:08:50,654 --> 00:08:53,491
Yeah, tell me again exactly
what you plan on doing with it.

167
00:08:53,657 --> 00:08:55,491
Oh my God, it's the President!

168
00:08:57,430 --> 00:08:59,527
- The President!
- Congratulations, sir!

169
00:08:59,891 --> 00:09:02,614
Thank you, thank you all
for your votes of confidence.

170
00:09:02,969 --> 00:09:04,629
If I could just be a little rude

171
00:09:04,754 --> 00:09:07,171
and ask to have a nice
quiet meal in private?

172
00:09:07,296 --> 00:09:08,565
Oh, of course!

173
00:09:08,690 --> 00:09:10,754
Come on, let's let the President
have some peace and quiet!

174
00:09:10,879 --> 00:09:13,274
Wow,
the President eats at Red Lobster!

175
00:09:13,399 --> 00:09:15,133
He's just a normal guy like me!

176
00:09:15,258 --> 00:09:16,973
Come on, guys!
President says get out!

177
00:09:17,139 --> 00:09:19,360
I didn't actually vote for him. Huh.

178
00:09:23,570 --> 00:09:24,571
Alright.

179
00:09:24,926 --> 00:09:26,081
Where are the ballots?

180
00:09:31,653 --> 00:09:34,603
{pub}Your friend has had
a severe allergic reaction.

181
00:09:34,835 --> 00:09:36,442
Can we please just
try and talk to him?

182
00:09:36,567 --> 00:09:38,703
Alright, but don't take too long.

183
00:09:41,789 --> 00:09:43,904
Butters, you have to tell
us what Cartman is up to.

184
00:09:46,195 --> 00:09:48,350
I didn't want to have to do this,
but if you don't tell us,

185
00:09:48,475 --> 00:09:51,424
I'm gonna tell your dad you helped get
the wrong person elected president.

186
00:09:51,982 --> 00:09:53,718
No!
Please, you can't!

187
00:09:53,884 --> 00:09:55,428
Then just tell me
what Cartman is up to!

188
00:09:55,594 --> 00:09:56,980
Okay! Okay!

189
00:09:57,217 --> 00:09:59,326
(he burbles something incomprehensible)

190
00:10:00,130 --> 00:10:01,046
What'd he say?

191
00:10:01,171 --> 00:10:03,393
(incomprehensible)

192
00:10:03,518 --> 00:10:04,286
Oh.

193
00:10:04,411 --> 00:10:07,106
What do you mean the election isn't the
biggest thing that happened this week?

194
00:10:07,272 --> 00:10:10,026
(incomprehensible)

195
00:10:10,192 --> 00:10:10,883
What?

196
00:10:11,008 --> 00:10:13,211
(incomprehensible)

197
00:10:13,336 --> 00:10:14,935
You mean the missile defense program?

198
00:10:15,060 --> 00:10:18,117
Butters, where is Cartman
supposed to hand over the ballots?

199
00:10:18,283 --> 00:10:19,744
At Red Lobster.

200
00:10:20,872 --> 00:10:21,914
At Red Lobster.

201
00:10:22,661 --> 00:10:23,373
Where?

202
00:10:23,539 --> 00:10:24,874
Red Lobster.

203
00:10:25,040 --> 00:10:27,961
- Huh?
- He said, at r-r-red...

204
00:10:28,127 --> 00:10:29,379
Red Lobster!

205
00:10:29,545 --> 00:10:31,172
- Red Lobster!
- What?

206
00:10:31,484 --> 00:10:33,049
R-r-r-red, r-r-r-red...

207
00:10:33,215 --> 00:10:34,384
Red Lobster!

208
00:10:34,550 --> 00:10:35,624
Red Lobster!

209
00:10:35,749 --> 00:10:37,142
Oh, Red Lobster.

210
00:10:37,678 --> 00:10:41,307
General Tsao,
do you have the missing ballots or not?

211
00:10:41,939 --> 00:10:43,232
Sure, we have the ballots.

212
00:10:43,357 --> 00:10:46,771
But we don't have them right
here with us now.

213
00:10:48,661 --> 00:10:52,026
Okay, you want me to come in again,
we can start over. What the fuck is this?

214
00:10:52,192 --> 00:10:54,340
Alright!
Where are the missing ballots?!

215
00:10:54,740 --> 00:10:55,744
President Obama?

216
00:10:56,114 --> 00:10:58,491
Oh, God dammit, Kyle,
I was just about to get what I wanted!

217
00:10:58,657 --> 00:11:00,655
Mr. President, we got you re-elected.

218
00:11:00,780 --> 00:11:03,237
Now you will give us
the Star Wars technology

219
00:11:03,362 --> 00:11:05,039
so that we can make the sequels!

220
00:11:05,205 --> 00:11:08,465
I told you China would get
the rights to Star Wars from Disney

221
00:11:08,590 --> 00:11:10,814
as long as my presidency was secure.

222
00:11:10,939 --> 00:11:13,380
Until I have those
ballots it is not secure.

223
00:11:13,505 --> 00:11:16,738
Wait a minute.
The rights to "Star Wars," the movies?

224
00:11:16,863 --> 00:11:18,249
What the hell is going on here?

225
00:11:18,374 --> 00:11:22,010
What's going on is the sale
of America's greatest asset.

226
00:11:23,544 --> 00:11:26,833
You see, when the United States
created Star Wars

227
00:11:26,958 --> 00:11:29,619
it made this country
incredibly powerful.

228
00:11:30,134 --> 00:11:31,524
Morgan Freeman?

229
00:11:31,931 --> 00:11:36,215
Earlier this week, Lucas signed
the rights to Star Wars over to Disney,

230
00:11:36,340 --> 00:11:39,595
and the Chinese saw a way
to obtain it for themselves.

231
00:11:39,720 --> 00:11:42,917
The Chinese government knew
that President Obama would help them

232
00:11:43,042 --> 00:11:46,414
take the rights from Disney
if they helped him get re-elected.

233
00:11:47,064 --> 00:11:48,065
Oh.

234
00:11:48,540 --> 00:11:51,638
But the child who actually stole
the ballots has hidden them

235
00:11:51,763 --> 00:11:54,975
and won't give them to anyone until
his demands are met.

236
00:11:55,766 --> 00:11:58,113
You've all got quite
a mess on your hands.

237
00:11:58,427 --> 00:12:00,134
I wish you well with it.

238
00:12:01,888 --> 00:12:03,306
Just one thing Morgan Freeman.

239
00:12:04,028 --> 00:12:06,893
How come every time something convoluted
needs explaining, you show up?

240
00:12:07,059 --> 00:12:11,064
Because every time I show up
and explain something, I earn a freckle.

241
00:12:16,401 --> 00:12:18,696
So here's the deal, General Tsao.
Mr. President.

242
00:12:18,862 --> 00:12:20,507
When the Chinese make the sequels,

243
00:12:20,896 --> 00:12:23,188
I get to play the part
of Luke Skywalker's son.

244
00:12:23,439 --> 00:12:25,078
Cartman Skywalker.

245
00:12:25,244 --> 00:12:28,665
That was not the deal!
We will not be bullied by you!

246
00:12:28,831 --> 00:12:32,921
Well, then I guess we're about
to play a game of chicken, General Tsao.

247
00:12:37,316 --> 00:12:40,090
I get it.
General Tsao's ch-chi-chicken.

248
00:12:40,843 --> 00:12:42,876
You are absolutely sure about this?

249
00:12:43,001 --> 00:12:46,441
It's been confirmed, Mike.
My God, we may have won this thing.

250
00:12:47,477 --> 00:12:48,377
Sir?

251
00:12:48,502 --> 00:12:51,062
Sir, we have some incredible news.

252
00:12:51,228 --> 00:12:54,280
There are rumors of hundreds
of thousands of stolen ballots.

253
00:12:54,405 --> 00:12:56,752
You might have won
the election after all

254
00:13:03,343 --> 00:13:04,649
There's nothing here, Kyle.

255
00:13:04,774 --> 00:13:06,160
Anything in the basement, Jimmy?

256
00:13:06,326 --> 00:13:08,588
Just old junk and boxes
of Eric's mom's dildos.

257
00:13:08,713 --> 00:13:09,664
Well, keep looking!

258
00:13:10,802 --> 00:13:12,710
Oh, my goodness, what's going on?

259
00:13:12,835 --> 00:13:13,585
Sorry, Ms. Cartman,

260
00:13:13,710 --> 00:13:15,581
but your son took some things
that didn't belong to him.

261
00:13:15,706 --> 00:13:19,164
I've told him to respect people's property.
What did he take?

262
00:13:19,289 --> 00:13:20,567
He stole ballots
in all the swing states

263
00:13:20,692 --> 00:13:22,073
so the wrong person
was elected president.

264
00:13:22,198 --> 00:13:24,948
Oh, well,
no TV for him for a few days.

265
00:13:25,579 --> 00:13:28,808
There's nothing here, Kyle.
Not even one Scooby clue.

266
00:13:28,974 --> 00:13:30,949
Oh, my God, you guys!
Look!

267
00:13:34,299 --> 00:13:36,232
Dude, it's Boba Fett's ship!

268
00:13:49,331 --> 00:13:51,664
Just what the Dickens
is going on here, ha ha?

269
00:13:51,830 --> 00:13:53,374
Oh shit, it's him again.

270
00:13:53,540 --> 00:13:55,815
What's this about
a deal with the Chinese?!

271
00:13:55,940 --> 00:13:58,003
I own all this shit now!

272
00:13:58,128 --> 00:14:02,566
I own the death star,
I own Tatooine, it's all mine.

273
00:14:03,795 --> 00:14:04,899
Alright, fuckers.

274
00:14:05,024 --> 00:14:06,870
Where are the missing ballots?

275
00:14:10,849 --> 00:14:11,850
Butters.

276
00:14:13,101 --> 00:14:15,342
You just couldn't keep your mouth shut,
could you, Butters?

277
00:14:15,467 --> 00:14:17,732
I didn't say anything!
I promise.

278
00:14:18,132 --> 00:14:20,985
I guess we learned that when it comes
to Star Wars we can't trust anyone.

279
00:14:21,151 --> 00:14:24,238
Not the president, not the Chinese.
And not you.

280
00:14:24,404 --> 00:14:27,074
Please, Eric!
I tried to be quiet, I swear!

281
00:14:27,323 --> 00:14:28,785
Shhhh, it's okay, Butters.

282
00:14:29,162 --> 00:14:31,140
Nobody's going to find
the election ballots.

283
00:14:31,265 --> 00:14:34,683
I have them hidden away,
somewhere nobody would ever look.

284
00:14:36,243 --> 00:14:39,143
A place in town people
barely even know exists.

285
00:14:43,102 --> 00:14:46,093
Hey, guys!
Lookin' to buy a Hummer today?

286
00:14:46,663 --> 00:14:50,389
We're having a Rocktober sales
event that's goin' into Rockvember.

287
00:14:55,746 --> 00:14:56,776
Hey, there!

288
00:14:56,901 --> 00:14:59,148
Interested in test driving
a Hummer today?

289
00:15:00,089 --> 00:15:03,686
I can see you... the Ray Bans,
got that nice bicep hanging out

290
00:15:03,811 --> 00:15:05,863
the window rolled down.
What do you think?

291
00:15:09,839 --> 00:15:15,164
Rockvember sales event, guys!
Every Hummer comes with a free Segway.

292
00:15:16,007 --> 00:15:17,701
Hey, why don't you shut up?

293
00:15:18,291 --> 00:15:20,784
Just excited about these deals,
that's all.

294
00:15:35,930 --> 00:15:39,438
We found him, sir.
But still no sign of the missing ballots.

295
00:15:39,604 --> 00:15:41,774
Go on, leave us alone.

296
00:15:43,547 --> 00:15:44,929
Now listen, little boy,

297
00:15:45,054 --> 00:15:49,046
the reason this country works is
because people go out and vote.

298
00:15:49,351 --> 00:15:52,038
- Every vote counts, and...
- Cut the crap, Mouse.

299
00:15:52,163 --> 00:15:53,900
You only care about
the election results because

300
00:15:54,025 --> 00:15:55,788
Romney would have been tougher
on the Chinese.

301
00:15:55,954 --> 00:15:58,451
Why don't you just tell me
where the missing ballots are?

302
00:15:58,576 --> 00:16:01,225
It'll make your death
a lot less painful, ha, ha.

303
00:16:01,466 --> 00:16:04,566
Trust me, they're somewhere
nobody will ever find them.

304
00:16:05,386 --> 00:16:07,746
If you kill me then Obama will
stay president and you'll lose

305
00:16:07,871 --> 00:16:09,634
Star Wars to the Chinese forever.

306
00:16:14,101 --> 00:16:16,963
Pretty neat, huh?
How would you like to have one?

307
00:16:17,880 --> 00:16:20,655
I can get those at Kmart.
If you want me to switch sides

308
00:16:20,780 --> 00:16:22,648
you'll have to do better than that.

309
00:16:23,479 --> 00:16:26,391
- Like what?
- I want a part in the new movies.

310
00:16:26,861 --> 00:16:29,911
Well, then, why didn't you just
come to me in the first place?

311
00:16:30,036 --> 00:16:33,075
If you wanna be in the next
Star Wars, I'm your guy.

312
00:16:33,577 --> 00:16:35,196
I get to be Luke Skywalker's son.

313
00:16:35,321 --> 00:16:37,496
And there has to be
a character called Jewbacca!

314
00:16:37,662 --> 00:16:39,910
You can be Luke's son,
you can be Han Solo's son,

315
00:16:40,035 --> 00:16:41,959
I don't give two shits in a ****.

316
00:16:42,125 --> 00:16:42,673
Really?

317
00:16:42,798 --> 00:16:45,227
Well, then,
I suppose that making the votes public

318
00:16:45,352 --> 00:16:46,720
is the right thing to do.

319
00:16:46,845 --> 00:16:48,252
I can get the ballots for you.

320
00:16:48,490 --> 00:16:51,465
But if I'm gonna smuggle them here,
I'm gonna need a blaster and a Tonton.

321
00:16:51,709 --> 00:16:55,161
Sure thing!
I got a Tonton coming up my asshole. Haha.

322
00:16:55,286 --> 00:16:56,592
- Haha!
- Haha!

323
00:16:56,717 --> 00:16:57,370
Hehe.

324
00:16:58,651 --> 00:17:01,024
This is Breaking Election News!

325
00:17:01,353 --> 00:17:02,618
Hold that phone!

326
00:17:02,743 --> 00:17:05,580
The election may be over,
but rumors are running rampant

327
00:17:05,705 --> 00:17:09,153
that hundreds of thousands of ballots
for Mitt Romney were stolen.

328
00:17:09,319 --> 00:17:12,153
We need everyone's help here.
What you're looking for are big

329
00:17:12,278 --> 00:17:13,689
boxes of ballots, hundreds of them.

330
00:17:13,814 --> 00:17:15,548
Please check your basements,
your attics.

331
00:17:15,673 --> 00:17:22,003
And you claim that a General Tsao hired
your friend Fat Ass to steal the ballots.

332
00:17:22,128 --> 00:17:24,848
But so far the Chinese are
refusing to speak with us.

333
00:17:24,973 --> 00:17:27,245
Yeah, well,
probably because General Tsao's chicken.

334
00:17:29,454 --> 00:17:31,092
Is that it?
Did he just... did he just...

335
00:17:31,258 --> 00:17:34,261
Yes, Chris we are getting confirmation
that was the millionth time!

336
00:17:34,427 --> 00:17:37,853
Excitement and revelry in South Park
as a little boy has just

337
00:17:37,978 --> 00:17:41,514
made the General Tsao's chicken
joke for the one millionth time!

338
00:17:41,639 --> 00:17:43,798
- What?
- Little boy, the General Tsao's chicken joke,

339
00:17:43,923 --> 00:17:47,269
a favorite for years, you've just
hit the magic number how's it feel?

340
00:17:48,157 --> 00:17:49,864
I... what?

341
00:17:50,630 --> 00:17:52,308
He's being presented the check now.

342
00:17:52,433 --> 00:17:55,116
A five thousand dollar gift
certificate to PF Chang's.

343
00:17:55,657 --> 00:17:58,922
This has been a long campaign
but someone's finally done it, wolf.

344
00:18:01,690 --> 00:18:03,322
Well, we tried dude.

345
00:18:04,084 --> 00:18:06,002
It just seems so unfair.

346
00:18:06,168 --> 00:18:09,069
People won't ever even know that
the wrong man is in the White House.

347
00:18:09,751 --> 00:18:11,359
Well, I mean, look at it this way.

348
00:18:11,484 --> 00:18:13,878
Almost half the country did
actually vote for Obama.

349
00:18:14,003 --> 00:18:16,630
If the election really just came
down to a bunch of boxes Cartman stole,

350
00:18:16,755 --> 00:18:18,274
then, does it matter that much?

351
00:18:18,399 --> 00:18:20,746
It matters, Stan.
It matters.

352
00:18:24,352 --> 00:18:26,390
<i>Come on down to</i> Stephenson Hammer

353
00:18:26,515 --> 00:18:29,358
<i>for our big Christmas
in Humvember sale!</i>

354
00:18:29,524 --> 00:18:32,028
<i>Not sure what to get
your loved one this holiday season?</i>

355
00:18:32,194 --> 00:18:33,962
<i>Why not give them a nice Hummer?</i>

356
00:18:34,087 --> 00:18:36,532
<i>Nothing beats a Hummer
on Christmas morning!</i>

357
00:18:37,853 --> 00:18:41,098
Stan... when's the last time you saw
somebody drive a Hummer?

358
00:18:41,692 --> 00:18:44,541
Uh, forever ago.
Like 2010.

359
00:18:45,332 --> 00:18:46,542
Oh my God.

360
00:18:48,721 --> 00:18:51,297
Hey! Looking for a Hummer today?

361
00:18:51,463 --> 00:18:53,470
Got some 2009s here.

362
00:18:53,843 --> 00:18:57,421
Never been driven... ever.

363
00:19:00,222 --> 00:19:01,562
Go on, get outta here.
Shoo!

364
00:19:02,138 --> 00:19:05,102
- Look! There they are!
- Hey! Hi there!

365
00:19:05,643 --> 00:19:08,625
You kids like hummers, huh?
Lemme show you these babies!

366
00:19:08,750 --> 00:19:10,333
We found it, you guys!

367
00:19:11,050 --> 00:19:12,485
Oh, no!
They followed us!

368
00:19:16,696 --> 00:19:19,867
Oh hell yes! Asians!
Hey guys!

369
00:19:20,274 --> 00:19:22,203
- Get out of our way.
- No!

370
00:19:22,369 --> 00:19:24,997
These don't belong to you.
They belong to the people!

371
00:19:25,932 --> 00:19:29,651
I am tired of playing games!
This little farce is over!

372
00:19:31,811 --> 00:19:35,351
I don't think so, General Tsao.
This way, Officers!!

373
00:19:40,011 --> 00:19:41,764
Oh my God, it's a sales rush!

374
00:19:42,598 --> 00:19:43,432
Oh my God.

375
00:19:44,766 --> 00:19:46,769
Are those what I think they are?

376
00:19:46,935 --> 00:19:50,481
Yes, we must show these to the public
for democracy's sake! Right, Kyle?

377
00:19:50,647 --> 00:19:54,944
Sir, we found something.
At the Hummer sales lot outside of town.

378
00:19:55,235 --> 00:19:56,706
You don't understand!

379
00:19:57,011 --> 00:20:00,616
We are trying to protect
the greatest film series ever made!

380
00:20:00,925 --> 00:20:01,951
Protect it?

381
00:20:02,117 --> 00:20:03,900
What the hell is going on here?

382
00:20:04,025 --> 00:20:06,455
Perhaps I can explain it to you.

383
00:20:10,213 --> 00:20:12,088
You see, it turns out

384
00:20:12,213 --> 00:20:16,193
the only reason the Chinese
so desperately wanted Star Wars

385
00:20:16,618 --> 00:20:21,254
is because they're afraid that Disney
might not be the right place for it.

386
00:20:21,794 --> 00:20:26,851
The Chinese simply want to guard
Star Wars' impeccable legacy.

387
00:20:27,551 --> 00:20:28,811
Is that Morgan Freeman?

388
00:20:28,977 --> 00:20:30,950
And now we are at a crossroads.

389
00:20:31,075 --> 00:20:32,853
If these ballots are made public

390
00:20:32,978 --> 00:20:36,435
then the man that people
voted for will be president,

391
00:20:36,560 --> 00:20:40,186
but he will no doubt
keep Star Wars from the Chinese

392
00:20:40,508 --> 00:20:42,780
and allow Disney to keep it instead.

393
00:20:43,533 --> 00:20:45,893
And so we have to ask ourselves:

394
00:20:46,018 --> 00:20:47,386
What's more important?

395
00:20:47,863 --> 00:20:50,273
That the right man is elected president,

396
00:20:50,640 --> 00:20:54,318
or that Star Wars is
with people who will protect it most?

397
00:21:02,466 --> 00:21:04,983
<i>Unit four, what is it? Over?</i>

398
00:21:05,108 --> 00:21:06,393
Well, kid?

399
00:21:06,518 --> 00:21:09,393
Well, if you put it that way.

400
00:21:09,559 --> 00:21:12,988
<i>Come in! Unit four, come in!
What have you found?</i>

401
00:21:14,018 --> 00:21:15,198
Nothing, sir.

402
00:21:15,323 --> 00:21:18,594
We just found some tremendous deals
on cars nobody wants, that's all.

403
00:21:18,719 --> 00:21:19,653
Barkley, out.

404
00:21:23,573 --> 00:21:26,785
No! But Obama wasn't really elected.
Don't you people care?!

405
00:21:38,378 --> 00:21:41,634
<font color="#3399FF">Sync and correction by Nitin</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
