﻿1
00:00:16,282 --> 00:00:27,224
Sync by YYeTs.net
<font color="#ec14bd">Corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

2
00:00:31,304 --> 00:00:33,489
I know the people are going to
say you should only practice

3
00:00:33,490 --> 00:00:34,997
this way or that way.

4
00:00:35,131 --> 00:00:36,800
I believe in capitalism too.

5
00:00:36,801 --> 00:00:37,853
I believe in capitalism for everybody.

6
00:00:37,854 --> 00:00:40,537
What he said, which I find mildly amazing,

7
00:00:40,567 --> 00:00:44,221
was he thought I would have a
hard time debating Barack Obama.

8
00:00:44,222 --> 00:00:47,602
We faced something greater
after world war ii.

9
00:00:47,603 --> 00:00:50,421
We had ten million coming home at once.

10
00:00:50,422 --> 00:00:52,189
What did we do then?

11
00:00:52,190 --> 00:00:55,987
Some liberals said we need
more work programs --

12
00:00:55,988 --> 00:00:58,089
Tango, Tango in position.

13
00:00:58,353 --> 00:00:59,132
Copy, tango.

14
00:00:59,133 --> 00:01:00,379
Clear vantage point.

15
00:01:00,380 --> 00:01:02,432
Will have to do. We're not getting closer.

16
00:01:02,490 --> 00:01:04,068
Alright, butters. Bring it in.

17
00:01:04,689 --> 00:01:06,989
***, ten seconds.

18
00:01:07,123 --> 00:01:09,195
Copy that.
10 seconds, Carlton.

19
00:01:09,316 --> 00:01:11,582
I can hear that, tell me when to go.

20
00:01:15,791 --> 00:01:17,011
We might have a problem.

21
00:01:17,012 --> 00:01:18,236
What's that?

22
00:01:18,424 --> 00:01:22,254
We just got word somebody might
try to faith hill this event.

23
00:01:23,077 --> 00:01:24,386
Lock down the perimeter.

24
00:01:24,387 --> 00:01:27,060
Nobody is faith hilling not on my watch.

25
00:01:28,767 --> 00:01:30,627
They're onto us guys. Let's do this.

26
00:01:37,063 --> 00:01:38,289
I got it.

27
00:01:38,361 --> 00:01:40,160
Hey, somebody is under the stage.

28
00:01:40,161 --> 00:01:41,413
Go Cartman.

29
00:01:53,309 --> 00:01:54,391
We got it.

30
00:01:54,392 --> 00:01:55,316
Go, go, go.

31
00:01:55,317 --> 00:01:57,294
That was sweet.

32
00:02:00,521 --> 00:02:02,212
First, there was planking.

33
00:02:02,464 --> 00:02:05,269
People taking pictures of
themselves in a planked position

34
00:02:05,270 --> 00:02:07,283
and putting the photos on the Internet.

35
00:02:07,284 --> 00:02:09,760
Planking was soon replaced by owling.

36
00:02:09,761 --> 00:02:12,376
After the super bowl by bradying.

37
00:02:12,484 --> 00:02:16,572
The newest meme is pulling the
shirt out to look like boobs.

38
00:02:16,573 --> 00:02:18,476
It's called faith hilling.

39
00:02:18,477 --> 00:02:20,842
All around the world people are doing it.

40
00:02:20,843 --> 00:02:24,931
Kids, adults, and even notable
celebrities are getting into the act.

41
00:02:24,932 --> 00:02:27,492
But as faith hilling is
more and more popular,

42
00:02:27,493 --> 00:02:32,639
the question is, who will be
the first to die doing it?

43
00:02:35,828 --> 00:02:40,735
I have been sent here because you
children are playing with fire!

44
00:02:41,019 --> 00:02:44,648
Faith hilling is nothing more
than a evolution of bradying.

45
00:02:44,649 --> 00:02:47,215
From football quarterback
to football singer.

46
00:02:47,306 --> 00:02:48,150
Please.

47
00:02:48,151 --> 00:02:50,655
Bradying is to 2000 and lame.

48
00:02:50,656 --> 00:02:54,516
I know what you think is
doing is new, hip and cool.

49
00:02:54,517 --> 00:02:57,552
The truth is meming has
been around a long time.

50
00:02:57,593 --> 00:02:58,999
We are going to watch a film strip now,

51
00:02:59,000 --> 00:03:02,460
although it's a little dated
but it gets the point across.

52
00:03:08,425 --> 00:03:11,810
For many young people today
taking pictures in silly poses

53
00:03:11,811 --> 00:03:13,928
becomes a dangerous past time.

54
00:03:13,929 --> 00:03:17,129
The latest meme has also
become the most deadly.

55
00:03:17,130 --> 00:03:19,290
It's called tebowing.

56
00:03:19,977 --> 00:03:21,984
This is Ryan and Barkley.

57
00:03:21,985 --> 00:03:25,217
They're about to learn how
dangerous tebowing can be.

58
00:03:25,218 --> 00:03:26,522
Here is a good place.

59
00:03:26,523 --> 00:03:27,749
I'll do it right here.

60
00:03:27,750 --> 00:03:31,425
I don't know, Ryan, you
sure this is a good idea.

61
00:03:31,426 --> 00:03:35,751
Stop being a scaredy-cat it will just
take a second how could I get hurt.

62
00:03:35,752 --> 00:03:38,062
Okay hold still.

63
00:03:40,584 --> 00:03:41,850
What's that?

64
00:03:41,893 --> 00:03:44,291
A train!

65
00:03:47,840 --> 00:03:50,197
Ryan!

66
00:03:50,198 --> 00:03:53,297
No, no, no, no!

67
00:03:54,774 --> 00:03:58,089
No, no, no, no, no!

68
00:04:03,719 --> 00:04:05,389
Ryan!

69
00:04:05,422 --> 00:04:08,042
Learn from me.

70
00:04:09,456 --> 00:04:11,222
This sure is a nice car, Tommy.

71
00:04:11,223 --> 00:04:14,316
Would you like to get a
picture of me tebowing in it.

72
00:04:14,317 --> 00:04:15,890
Sure.

73
00:04:19,153 --> 00:04:20,460
What is that.

74
00:04:20,661 --> 00:04:22,852
My God it's coming.

75
00:04:22,930 --> 00:04:24,445
God no!

76
00:04:24,916 --> 00:04:25,848
No, no!

77
00:04:25,849 --> 00:04:27,613
Tommy, did you get the picture.

78
00:04:27,614 --> 00:04:28,789
Hold on.

79
00:04:28,790 --> 00:04:31,302
Hurry, hurry!

80
00:04:38,987 --> 00:04:40,747
Be careful up, there Pete.

81
00:04:40,748 --> 00:04:42,826
Go on and take the picture.

82
00:04:46,572 --> 00:04:49,175
These youths paid with
their lives for tebowing

83
00:04:49,176 --> 00:04:51,563
when they posed for pictures
they should have remembered

84
00:04:51,564 --> 00:04:56,809
there are only three approved memes, peace
signs, bunny ears and fake winners.

85
00:04:56,810 --> 00:05:01,726
Maybe you think this doesn't apply to you,
maybe you think your memes are safe.

86
00:05:01,951 --> 00:05:04,734
Maybe you're watching this in the future

87
00:05:04,735 --> 00:05:07,862
and teabowing has been
replace by some other meme.

88
00:05:07,881 --> 00:05:10,109
Well, if you are watching
this in the future,

89
00:05:10,110 --> 00:05:13,378
if you are get off your flying
cellphone scooters and think.

90
00:05:13,379 --> 00:05:14,626
Just remember.

91
00:05:14,643 --> 00:05:18,147
Use the approved poses if
you want to be a memer.

92
00:05:18,148 --> 00:05:21,169
Peace signs, bunny ears, and fake wieners.

93
00:05:25,151 --> 00:05:27,180
Oh, God. That was boring.

94
00:05:27,475 --> 00:05:29,301
So, what do you want to do now?

95
00:05:29,631 --> 00:05:31,592
There is the nice French cafe downtown.

96
00:05:31,593 --> 00:05:33,427
Maybe we should get some faith
hilling pictures there.

97
00:05:33,428 --> 00:05:34,572
That's a good idea.

98
00:05:34,573 --> 00:05:39,078
What, you mean you guys still plan on
faith hilling after what we just saw?

99
00:05:39,079 --> 00:05:41,641
Butters, faith hilling
defines our generation.

100
00:05:41,761 --> 00:05:43,397
Count me out.

101
00:05:43,398 --> 00:05:46,039
You're gonna give up on faith
hilling just like that, butters?

102
00:05:46,040 --> 00:05:47,351
How could you?

103
00:05:47,593 --> 00:05:49,089
I'm scared.

104
00:05:49,164 --> 00:05:50,999
Did you see today's newspaper?

105
00:05:51,000 --> 00:05:53,039
Why would we look at a newspaper, retard?

106
00:05:53,040 --> 00:05:54,451
You made the front page.

107
00:05:54,452 --> 00:05:55,767
Really?

108
00:05:58,545 --> 00:06:00,196
Dude, we are on the front page.

109
00:06:00,197 --> 00:06:01,559
Yes.

110
00:06:01,729 --> 00:06:03,398
Can we get a different pose, please.

111
00:06:04,144 --> 00:06:05,648
What's it say, what's it say?

112
00:06:06,015 --> 00:06:08,475
It says...
Oh, no.

113
00:06:08,905 --> 00:06:12,694
It says faith hilling is now
all like 2000 and late.

114
00:06:13,055 --> 00:06:15,187
What?
Let me see that.

115
00:06:17,268 --> 00:06:18,707
How can that be?

116
00:06:18,718 --> 00:06:19,662
Already?

117
00:06:19,663 --> 00:06:21,632
Public reaction was the stunt pulled off

118
00:06:21,633 --> 00:06:25,171
by five elementary school students
wasn't not only dangerous and disruptive

119
00:06:25,172 --> 00:06:27,902
but also completely passe.

120
00:06:27,990 --> 00:06:30,566
How can we be passe?
We're only in 4th grade.

121
00:06:30,567 --> 00:06:34,564
"Faith hilling is pretty stale" said
Republican candidate Newt Gingrich.

122
00:06:34,565 --> 00:06:38,534
If they had crashed the debate by taylor
swifting that would of been impressive.

123
00:06:39,039 --> 00:06:41,877
Taylor swifting?
What the fuck is that?

124
00:06:47,340 --> 00:06:48,478
That's all it is?

125
00:06:48,479 --> 00:06:52,004
You pull down your pants and wipe your
butt on the ground like an old dog.

126
00:06:55,553 --> 00:06:57,188
But that's stupid.

127
00:06:57,189 --> 00:06:59,019
How can that replace faith hilling.

128
00:06:59,020 --> 00:07:00,637
It doesn't even make any sense.

129
00:07:00,774 --> 00:07:03,327
I can't believe people take
the time to do this garbage.

130
00:07:03,328 --> 00:07:05,005
This has to be stopped, you guys.

131
00:07:09,850 --> 00:07:11,858
Okay. Hold it there. Okay.

132
00:07:11,859 --> 00:07:13,119
That's good, don't move.

133
00:07:14,487 --> 00:07:16,245
How is this? This good?

134
00:07:17,013 --> 00:07:18,338
Yep, that's great.

135
00:07:18,339 --> 00:07:20,202
These will be good.

136
00:07:20,727 --> 00:07:21,869
What are you doing?

137
00:07:21,870 --> 00:07:23,959
Faith hilling.
Why don't you get the fuck out of here?

138
00:07:24,236 --> 00:07:27,457
Oh God. Faith hilling is so February 2012.

139
00:07:27,458 --> 00:07:28,924
Saying something is so 2000

140
00:07:28,925 --> 00:07:31,895
and anything is so 2009,
you stupid ass wipe.

141
00:07:31,896 --> 00:07:33,350
Come on guys. It's not worth it.

142
00:07:33,351 --> 00:07:35,486
We can do our taylor
swifting somewhere else.

143
00:07:39,545 --> 00:07:40,548
Knock it off!

144
00:07:40,549 --> 00:07:42,051
Why don't you make me.

145
00:07:58,385 --> 00:08:03,294
Yesterday afternoon four kids went to
the hospital for injuries resulting

146
00:08:03,295 --> 00:08:05,655
from meming in front of a local cafe.

147
00:08:07,562 --> 00:08:09,146
Faith hilling, taylor swifting.

148
00:08:09,147 --> 00:08:11,498
These are things that will get you killed!

149
00:08:12,801 --> 00:08:14,545
That's a loaded .38.

150
00:08:14,882 --> 00:08:17,261
How many pf you think it's a smart idea

151
00:08:17,262 --> 00:08:20,395
to put a loaded .38 on a 9-year-old's desk.

152
00:08:21,280 --> 00:08:24,744
If safety doesn't matter
go ahead, pick the gun up.

153
00:08:24,864 --> 00:08:27,553
That's okay. I think...
Pick the gun up!

154
00:08:27,554 --> 00:08:31,967
You might as well. Swifting and hilling
is like playing with a loaded gun.

155
00:08:31,995 --> 00:08:33,793
Do you all understand my point?

156
00:08:33,794 --> 00:08:35,146
Yes, sir.

157
00:08:35,147 --> 00:08:35,938
Good.

158
00:08:35,939 --> 00:08:37,123
Now put the gun in your mouth.

159
00:08:38,105 --> 00:08:42,808
Hey. You're following plankers and teabowers,
so put a loaded gun in your mouth!

160
00:08:43,029 --> 00:08:44,350
Do it!

161
00:08:47,140 --> 00:08:48,831
Everybody take a good look.

162
00:08:48,832 --> 00:08:52,747
This is what you're doing every
time you play with Internet memes.

163
00:08:52,748 --> 00:08:56,461
You're playing roulette with your lives!

164
00:08:58,173 --> 00:09:00,643
Professor Lamont, we need to talk.

165
00:09:01,434 --> 00:09:02,747
I will be right back.

166
00:09:07,327 --> 00:09:09,601
You're an expert on memes,
Professor Lamont.

167
00:09:09,602 --> 00:09:11,188
Yes, what is this about?

168
00:09:11,367 --> 00:09:13,548
We need your expertise.

169
00:09:13,549 --> 00:09:17,838
Have you heard of another species,
meming on the Internet?

170
00:09:17,903 --> 00:09:18,764
Another species?

171
00:09:18,765 --> 00:09:20,153
What are you talking about?

172
00:09:20,154 --> 00:09:24,032
We were hoping you could
help us understand this.

173
00:09:28,990 --> 00:09:31,846
Cats have started to put pictures
of themselves on the Internet

174
00:09:31,847 --> 00:09:33,586
with bread around their heads.

175
00:09:33,587 --> 00:09:35,696
Kat breading is what it's called.

176
00:09:35,697 --> 00:09:39,701
It's an odd thing to do, we thought
would you explain it to us.

177
00:09:40,638 --> 00:09:41,964
They're evolving.

178
00:09:42,145 --> 00:09:43,693
Cats are evolving.

179
00:09:44,978 --> 00:09:46,227
Sorry?

180
00:09:46,697 --> 00:09:49,155
There are two ways a species evolves.

181
00:09:49,269 --> 00:09:53,077
Physically from genes and
culturally from memes.

182
00:09:53,078 --> 00:09:56,897
Just like genes, memes
replicate, mutate and adapt.

183
00:09:57,451 --> 00:09:59,523
We're having a hard time
following you here.

184
00:09:59,524 --> 00:10:00,753
Look.

185
00:10:00,775 --> 00:10:02,303
In the 70s there was fonzing.

186
00:10:02,564 --> 00:10:04,663
Which replaced the outdated mustaching.

187
00:10:05,053 --> 00:10:08,751
The 60s cultural ideas were passed
on by everybody poodle-fisting.

188
00:10:09,578 --> 00:10:12,651
Even that evolved from people
ass wedging in the 40s.

189
00:10:13,529 --> 00:10:16,681
Even before photographs
humans memed for portraits.

190
00:10:17,200 --> 00:10:21,628
All the way back to the Egyptians who had
pictures painted of themselves donkey ticking.

191
00:10:22,091 --> 00:10:26,056
You are saying cats are showing signs of
evolution with their Kat breading meme?

192
00:10:27,267 --> 00:10:30,804
If they're putting slices of bread
on their heads and taking pictures

193
00:10:31,168 --> 00:10:34,008
they're proving to be almost
as intelligent as we are.

194
00:10:47,620 --> 00:10:50,055
Mister kitty, you want to explain this?

195
00:10:51,849 --> 00:10:55,064
Why are you putting pictures on the
Internet with bread around your face?

196
00:10:57,335 --> 00:10:59,644
This is a bad kitty, bad.

197
00:11:00,292 --> 00:11:03,213
You're taking the idea of faith
hilling and making it stupid.

198
00:11:03,537 --> 00:11:04,332
Bad kitty.

199
00:11:05,441 --> 00:11:06,300
Bad Mr. kitty.

200
00:11:09,099 --> 00:11:09,850
Bad kitty.

201
00:11:10,035 --> 00:11:10,878
No more meming.

202
00:11:11,738 --> 00:11:12,427
Come on, guys.

203
00:11:13,283 --> 00:11:14,151
Bad mister kitty.

204
00:11:16,549 --> 00:11:18,183
I'm glad we took care of that.

205
00:11:18,382 --> 00:11:19,902
Ya, what do you want to do now?

206
00:11:20,075 --> 00:11:23,059
I thought of faith hilling at
the place they do AA meeting.

207
00:11:23,246 --> 00:11:24,171
That's a cool idea

208
00:11:28,253 --> 00:11:29,276
what's the matter, Kenny?

209
00:11:30,152 --> 00:11:30,849
I don't know.

210
00:11:33,666 --> 00:11:34,538
What seems point less?

211
00:11:37,078 --> 00:11:40,224
Hey, faith hilling is not
out of style alright?

212
00:11:42,816 --> 00:11:43,511
No, no, no!

213
00:11:44,005 --> 00:11:45,700
These stupid fads are only that, okay?

214
00:11:46,079 --> 00:11:47,322
We can't give in to this crap.

215
00:11:47,874 --> 00:11:49,494
Don't give up on faith
hilling, Kenny.

216
00:11:50,004 --> 00:11:51,021
Don't you give up on her.

217
00:11:56,812 --> 00:12:01,099
Two Boulder children died today while
oh long johnsoning in a battling cage.

218
00:12:01,551 --> 00:12:04,519
Oh long johnsonning is of
course the latest Internet meme

219
00:12:04,520 --> 00:12:07,130
which involves putting
oneself in a risky situation

220
00:12:07,131 --> 00:12:09,154
and see how many times you can say

221
00:12:09,251 --> 00:12:12,487
old long Johnson on video
before getting out of the way.

222
00:12:17,274 --> 00:12:18,301
You ready, you ready?

223
00:12:18,688 --> 00:12:19,802
I'm recording, go.

224
00:12:21,540 --> 00:12:23,133
Oh long Johnson.

225
00:12:23,210 --> 00:12:24,422
Oh long Johnson.

226
00:12:24,633 --> 00:12:25,778
Oh long --

227
00:12:26,503 --> 00:12:27,283
Larry.

228
00:12:28,496 --> 00:12:30,186
This latest Internet meme is shocking.

229
00:12:30,187 --> 00:12:34,492
But most shocking is the person who started
the meme isn't a person at all

230
00:12:34,745 --> 00:12:38,531
but a cat who seems to have
no record for peoples safety.

231
00:12:39,534 --> 00:12:41,810
Oh long Johnson.

232
00:12:42,842 --> 00:12:44,518
Oh long Johnson.

233
00:12:47,221 --> 00:12:48,934
Oh long Johnson.

234
00:12:49,776 --> 00:12:53,977
The cat is awaiting trial for its
part in the teenager's death.

235
00:13:00,170 --> 00:13:01,468
Oh long Johnson.

236
00:13:02,103 --> 00:13:03,551
Oh long Johnson.

237
00:13:05,125 --> 00:13:05,945
Yeah, that's good.

238
00:13:06,174 --> 00:13:07,283
Now go back the other way.

239
00:13:09,444 --> 00:13:11,447
Oh long Johnson, oh long --

240
00:13:12,025 --> 00:13:13,528
Dude, what's going on?

241
00:13:13,678 --> 00:13:15,840
Oh, hey, guys.
How's it going?

242
00:13:16,000 --> 00:13:17,844
We thought you were
meeting us at Cartman's.

243
00:13:17,871 --> 00:13:19,820
What are you doing here taylor swifting?

244
00:13:19,990 --> 00:13:22,075
He's not taylor swifting,
that's old stuff.

245
00:13:22,164 --> 00:13:24,854
Yeah, now you're doing to see how many
times you can say oh long Johnson.

246
00:13:24,953 --> 00:13:26,475
I thought just, you know, try it out.

247
00:13:28,566 --> 00:13:32,611
Remember when we heard about that pollack
with one testicle in the revolutionary war?

248
00:13:32,684 --> 00:13:33,432
What's his name?

249
00:13:33,783 --> 00:13:35,202
Benedict Arnold.

250
00:13:35,203 --> 00:13:38,763
Oh guys. You need to realize that
faith hilling is over, okay?

251
00:13:38,804 --> 00:13:41,103
You can pretend all you want,
but it's not coming back.

252
00:13:42,232 --> 00:13:42,927
Kenny.

253
00:13:48,650 --> 00:13:50,145
If you guys want to keep faith hilling

254
00:13:50,146 --> 00:13:52,278
I'm sure people are still doing
it at the old folks home.

255
00:13:52,511 --> 00:13:54,182
You will like this, Kenny.
Pull down your pants.

256
00:13:57,929 --> 00:13:59,122
You guys are sellouts.

257
00:13:59,540 --> 00:14:00,212
Come on.

258
00:14:00,951 --> 00:14:02,900
You're freaking sellouts!

259
00:14:05,369 --> 00:14:07,128
What are you doing?

260
00:14:07,324 --> 00:14:10,836
When you play with memes
you're playing with fire.

261
00:14:13,380 --> 00:14:15,229
Oh long Johnson.

262
00:14:16,515 --> 00:14:18,162
Oh, very funny.

263
00:14:18,690 --> 00:14:21,089
People are dying out
there. Is that what you want?

264
00:14:21,741 --> 00:14:23,735
Oh long --

265
00:14:24,540 --> 00:14:25,802
You cats want a war?

266
00:14:26,170 --> 00:14:27,721
That's what you're going to get!

267
00:14:28,047 --> 00:14:30,375
Oh oh oh.

268
00:14:30,576 --> 00:14:32,489
Long ago there was tebowing

269
00:14:32,950 --> 00:14:34,745
which evolved into faith hilling.

270
00:14:35,088 --> 00:14:39,788
The latest meming craze, swift
johnsoning, may have it's rival

271
00:14:40,173 --> 00:14:44,276
a brand new meme, where people videotape
themselves wearing trench coats

272
00:14:44,344 --> 00:14:46,611
and talking about the dangers of meming.

273
00:14:46,925 --> 00:14:48,681
They call it "reporting".

274
00:14:48,992 --> 00:14:52,507
They say it's a dangerous
and potentially fatal.

275
00:14:55,519 --> 00:14:58,082
Oh, oh long Johnson.

276
00:14:58,162 --> 00:15:00,800
Oh long Johnson.

277
00:15:17,496 --> 00:15:18,954
Ya, ya, that's cool.

278
00:15:19,308 --> 00:15:20,660
Bring it out some more.

279
00:15:22,171 --> 00:15:23,759
Ya, freeze. There.

280
00:15:25,130 --> 00:15:26,556
What are those boys doing, daddy?

281
00:15:26,642 --> 00:15:28,708
I think they're faith hilling, Bobby.

282
00:15:28,830 --> 00:15:30,232
It's a little before your time.

283
00:15:30,597 --> 00:15:31,390
How droll.

284
00:15:36,933 --> 00:15:38,260
Get a couple facing the other way.

285
00:15:38,712 --> 00:15:39,772
Ya, good idea.

286
00:15:40,430 --> 00:15:42,652
You kids faith hilling
in front of my clinic?

287
00:15:43,113 --> 00:15:46,138
I have a couple of patients that
could use a good time machine.

288
00:15:46,654 --> 00:15:47,372
Get it?

289
00:15:51,548 --> 00:15:54,789
Go back to the 90s!

290
00:16:04,034 --> 00:16:05,228
We have to face it, Cartman.

291
00:16:06,084 --> 00:16:06,745
I know.

292
00:16:07,859 --> 00:16:08,672
I know Cal.

293
00:16:10,887 --> 00:16:12,225
I thought it was going to last.

294
00:16:13,404 --> 00:16:15,723
I guess the only thing that
doesn't change in life

295
00:16:17,161 --> 00:16:18,229
is that things change.

296
00:16:20,477 --> 00:16:21,669
***.

297
00:16:23,751 --> 00:16:26,071
It wasn't like faith
hilling was that great.

298
00:16:26,072 --> 00:16:27,547
I mean --
No, no.

299
00:16:27,594 --> 00:16:28,781
It was kind of stupid really.

300
00:16:28,863 --> 00:16:30,400
It's good it became something else.

301
00:16:30,401 --> 00:16:32,857
We will have a blast
doing the new stuff.

302
00:16:41,855 --> 00:16:46,687
Household cats have evolved
into a species as intelligent as humans.

303
00:16:46,978 --> 00:16:49,812
Will this mean war between
our two life forms?

304
00:16:49,820 --> 00:16:53,544
In an attempt to try to communicate
with the leader of cats,

305
00:16:53,748 --> 00:16:56,933
experts have sent in the
ambassador of people.

306
00:16:59,390 --> 00:17:00,237
Hello.

307
00:17:02,706 --> 00:17:04,362
Oh long John.

308
00:17:04,905 --> 00:17:08,494
Meow, meow.

309
00:17:08,753 --> 00:17:10,794
Oh long John.

310
00:17:11,069 --> 00:17:12,950
Long Johnson.

311
00:17:13,414 --> 00:17:15,030
Oh long Johnson.

312
00:17:15,031 --> 00:17:20,796
Oh long Johnson--
Oh -- oh don piano.

313
00:17:20,911 --> 00:17:22,226
Oh lala.

314
00:17:23,434 --> 00:17:25,502
Oh long Johnson.

315
00:17:27,667 --> 00:17:30,789
Don piano.

316
00:17:33,360 --> 00:17:37,566
I'm not sure but I think it said war
between our species is inevitable

317
00:17:37,932 --> 00:17:41,116
and evolution cannot be
stopped and cats will rise.

318
00:17:41,511 --> 00:17:47,570
It said we cannot coexist and
then said oh don piano and then

319
00:17:47,571 --> 00:17:52,457
and then something about seeing the streets of human
cities running red with the blood of their children.

320
00:17:55,403 --> 00:17:56,935
Okay, that's good, a little higher.

321
00:17:58,292 --> 00:17:59,906
I will go left to right.

322
00:17:59,907 --> 00:18:00,569
Good.

323
00:18:00,710 --> 00:18:01,729
Awesome.

324
00:18:01,866 --> 00:18:02,836
Whenever you're ready.

325
00:18:02,866 --> 00:18:05,696
No, kitty, you have to be quiet.

326
00:18:06,252 --> 00:18:06,941
No kitty!

327
00:18:06,942 --> 00:18:08,044
That's a bad kitty!

328
00:18:09,023 --> 00:18:09,950
Wow, what's this?

329
00:18:10,256 --> 00:18:11,724
What you been living under a rock?

330
00:18:11,975 --> 00:18:14,711
This is the new meme, cat
taylor swift reporting.

331
00:18:15,556 --> 00:18:16,427
Alright, go Cartman.

332
00:18:17,578 --> 00:18:18,668
Taylor swift is dangerous.

333
00:18:19,467 --> 00:18:20,922
Taylor swift is dangerous!

334
00:18:21,812 --> 00:18:22,560
That's pretty cool.

335
00:18:22,809 --> 00:18:24,151
That's cool.
It's awesome.

336
00:18:24,217 --> 00:18:25,208
Super awesome.

337
00:18:25,433 --> 00:18:26,774
We got it down, guys.

338
00:18:27,040 --> 00:18:29,040
Think we're ready for the big times.

339
00:18:29,132 --> 00:18:30,173
Shut up, kitty!

340
00:18:30,832 --> 00:18:33,251
With the inevitable species war looming,

341
00:18:33,464 --> 00:18:36,080
our country's brightest
leaders have come together

342
00:18:36,081 --> 00:18:39,374
to debate and reason with
the ambassador of cats.

343
00:18:39,776 --> 00:18:41,343
It's called putty-whistling.

344
00:18:41,614 --> 00:18:43,173
And the question on everyone's mind...

345
00:18:43,292 --> 00:18:45,915
Who will be the first
person to die from it?

346
00:18:47,281 --> 00:18:49,254
I ran for president four years ago.

347
00:18:49,255 --> 00:18:51,405
This is the position I
described when I ran...

348
00:18:51,406 --> 00:18:55,068
If you want to be in
the -- I believe --

349
00:18:55,396 --> 00:18:56,738
How big of a scale of change
do we want in Washington.

350
00:18:56,739 --> 00:18:58,055
Oh long Johnson.

351
00:18:58,527 --> 00:18:59,461
I hope --

352
00:18:59,772 --> 00:19:01,374
Oh long Johnson.

353
00:19:12,638 --> 00:19:16,599
I have five sons, five daughters
in law, sixteen grand children.

354
00:19:19,080 --> 00:19:21,865
It's not the most attractive
thing to go out and say

355
00:19:22,066 --> 00:19:24,382
it took me ten or twelve years
to figure out it was wrong.

356
00:19:24,383 --> 00:19:25,678
Oh, long Johnson.

357
00:19:27,135 --> 00:19:29,271
Fox trot standing by. Position outlook.

358
00:19:29,469 --> 00:19:31,668
Fox trot, let's ***.

359
00:19:36,423 --> 00:19:37,788
Standing by in three seconds.

360
00:19:37,984 --> 00:19:39,915
Two, one. Go Cartman.

361
00:19:40,074 --> 00:19:40,851
Less do this.

362
00:19:53,792 --> 00:19:54,747
Cartman?

363
00:19:55,740 --> 00:19:56,474
Go, Cartman.

364
00:19:58,238 --> 00:19:59,233
Come on, dude. Hurry.

365
00:20:01,684 --> 00:20:02,343
No.

366
00:20:03,051 --> 00:20:04,014
No!

367
00:20:07,255 --> 00:20:08,198
I won't do it.

368
00:20:08,854 --> 00:20:10,089
I won't do it, you hear me!

369
00:20:11,506 --> 00:20:12,512
I'm better than this.

370
00:20:13,338 --> 00:20:14,766
To hell with you, Mr. kitty.

371
00:20:15,012 --> 00:20:16,206
You're a bad kitty!

372
00:20:17,115 --> 00:20:18,018
Bad kitty!

373
00:20:18,993 --> 00:20:21,881
It's time somebody stood up
and did the right thing.

374
00:20:30,040 --> 00:20:31,500
Ya, do it, Cartman.

375
00:20:33,710 --> 00:20:35,770
♪ Alright ♪♪♪

376
00:20:35,771 --> 00:20:37,166
♪ what do you do

377
00:20:37,953 --> 00:20:40,851
♪ get out your camera and a booby or two

378
00:20:41,640 --> 00:20:45,110
♪ we got to get serious --
have you ever seen faith hilling so good?

379
00:20:45,415 --> 00:20:48,841
♪ come on everybody,
it's faith hilling time

380
00:20:55,688 --> 00:20:57,215
♪ where are you

381
00:20:57,699 --> 00:21:00,789
♪ this is the only meming I'll ever do

382
00:21:01,264 --> 00:21:02,775
Oh long Johnson.

383
00:21:05,150 --> 00:21:08,536
♪ I'm faith hilling until the day I die

384
00:21:12,176 --> 00:21:15,941
And so in the face of war a
little boy reminds us all what

385
00:21:15,942 --> 00:21:17,394
being human really means.

386
00:21:18,080 --> 00:21:19,711
The message is unclear.

387
00:21:20,142 --> 00:21:24,540
It doesn't matter as long as you give
the audience a song, celebrity bashing,

388
00:21:24,684 --> 00:21:27,482
and republican hopeful
dancing around with boobies.

389
00:21:27,921 --> 00:21:31,416
It's called pandering and all
over the country people are --

390
00:21:33,545 --> 00:21:35,411
Oh long Johnson...

391
00:21:36,132 --> 00:21:41,280
Sync by YYeTs.net
<font color="#ec14bd">Corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
