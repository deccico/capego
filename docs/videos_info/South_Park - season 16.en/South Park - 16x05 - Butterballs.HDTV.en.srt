1
00:00:04,870 --> 00:00:07,630
subtitle created by Touhidul Islam Moulik

2
00:00:34,820 --> 00:00:35,990
no no no. I'm telling you,guys

3
00:00:36,110 --> 00:00:38,950
music videos have evolved
to nothing but pretty girls

4
00:00:39,080 --> 00:00:41,950
wearing skin-tight clothes
and singing songs about their vajayjays.

5
00:00:42,080 --> 00:00:44,250
It used to be chick's song about relationships,
now it's all,

6
00:00:44,370 --> 00:00:46,080
"My vajayjay this, my vajayjay that."

7
00:00:46,250 --> 00:00:47,960
But, clearly, that's what sells. Think about that.

8
00:00:48,130 --> 00:00:50,000
When was the last time you turned on a music video and

9
00:00:50,130 --> 00:00:52,760
you didn't see some chicks tuning guitars
singing about her vajayjay?

10
00:00:54,470 --> 00:00:55,680
See.You can't remember.

11
00:00:55,800 --> 00:00:56,630
Hey. Fellas

12
00:00:58,340 --> 00:00:59,680
Where's your lunch? BUTTERS

13
00:00:59,800 --> 00:01:01,640
Oh.It's OK.
I'm not hungry anyway.

14
00:01:01,810 --> 00:01:03,730
Did a bullytake your lunch money again?

15
00:01:05,310 --> 00:01:07,310
Yeah
That's the third day in a row.
You got to tell a teacher.

16
00:01:07,810 --> 00:01:09,190
Nah. I'm not a tattletale.

17
00:01:09,360 --> 00:01:11,070
Then write the principal an anonymous letter.

18
00:01:11,480 --> 00:01:12,980
No. I'm not an anonymous Andy.

19
00:01:13,150 --> 00:01:15,450
So just get a bigger bully
to beat the bully up.

20
00:01:15,820 --> 00:01:18,910
I don't want kids calling me
a clich� conflict resolution Kevin.

21
00:01:19,030 --> 00:01:21,370
He has a point.
Then you got to ride it out. BUTTERS

22
00:01:21,530 --> 00:01:23,700
Life sucks sometimes,
but it will pass.

23
00:01:23,830 --> 00:01:25,120
I can't believe it what i'm hearing.

24
00:01:25,250 --> 00:01:27,540
This is why bullying
is getting worse and worse at our schools.

25
00:01:27,670 --> 00:01:29,710
We can't sit by
and let it happen anymore.

26
00:01:29,830 --> 00:01:32,750
Good for you. STAN
Nice somebody in this school has some balls.

27
00:01:32,920 --> 00:01:35,970
I have balls.
Yeah.. Little swishy bubble tea balls.

28
00:01:36,300 --> 00:01:37,380
Still balls.

29
00:01:37,970 --> 00:01:40,390
Look BUTTERS. Why don't you talk
to your family about it?

30
00:01:40,510 --> 00:01:43,720
You said Your grandma is visiting this week.
Why don't you try talking to her?

31
00:01:43,850 --> 00:01:44,850
My grandma?

32
00:01:45,350 --> 00:01:47,390
So then your cousin Elbert Linda

33
00:01:47,520 --> 00:01:49,350
actually has two girls now.

34
00:01:49,480 --> 00:01:52,730
One is three,
and the others about little BUTTERS age I think.

35
00:01:53,230 --> 00:01:54,570
Wow! Speak of the devil.

36
00:01:54,690 --> 00:01:56,150
BUTTERS! What happened to your eye?

37
00:01:56,280 --> 00:01:58,150
Grandma, can I talk to you for a second?

38
00:01:58,280 --> 00:02:01,530
Well! Sure, you come
and sit right here next to Granny.

39
00:02:01,870 --> 00:02:03,620
Can I maybe talk to Granny alone?

40
00:02:03,740 --> 00:02:06,040
Well! Sure, I guess.
We'll go make some tea.

41
00:02:06,160 --> 00:02:08,290
Oh! Some tea would be lovely.

42
00:02:13,340 --> 00:02:14,340
What's up?

43
00:02:15,710 --> 00:02:17,170
Huh! You think you're tough? Huh!

44
00:02:17,550 --> 00:02:19,930
I just don't want you
to pick on me no more. Grandma

45
00:02:20,050 --> 00:02:21,800
You think you're fucking tough? Huh!

46
00:02:22,140 --> 00:02:23,890
You don't look fucking tough.

47
00:02:24,310 --> 00:02:27,390
I don't think I'm tough.grandma
No,�You're a little faggot.

48
00:02:27,520 --> 00:02:29,440
You got any more money?

49
00:02:29,560 --> 00:02:30,730
No, you took it all!

50
00:02:30,850 --> 00:02:32,860
Why don't you do something about it?

51
00:02:34,360 --> 00:02:36,110
Do something, you little bitch.

52
00:02:36,230 --> 00:02:37,650
Youre Grandma's bitch.

53
00:02:37,820 --> 00:02:40,410
Here we go.
There is the tea.

54
00:02:40,660 --> 00:02:42,700
Tea for me, how lucky!

55
00:02:43,370 --> 00:02:45,080
And your favorite lemon bars.

56
00:02:45,200 --> 00:02:47,750
Goodness, what a treat this is.

57
00:02:50,370 --> 00:02:52,290
Mr.Mackey. What is happening at your school

58
00:02:52,420 --> 00:02:54,880
is no different
from what's happening all over the country.

59
00:02:55,000 --> 00:02:57,340
Bullying has become an epidemic,
I'm afraid.

60
00:02:57,460 --> 00:03:00,630
And we, at Bully Buckers
are trying to stamp it out.

61
00:03:00,760 --> 00:03:02,890
What is your school's policy
on bullying?

62
00:03:04,890 --> 00:03:07,850
Oh well,
We think that bullying is bad.
Mkey

63
00:03:10,140 --> 00:03:13,810
And when we see bullying in the school,
we tell the students you know that

64
00:03:14,360 --> 00:03:15,650
that's bad. Mkay

65
00:03:18,400 --> 00:03:19,900
Aha, One of your students told us

66
00:03:20,030 --> 00:03:22,070
he thinks bullying in your school
is getting worse.

67
00:03:22,410 --> 00:03:24,780
Who said?
He didn't want that disclosed.

68
00:03:25,200 --> 00:03:27,120
He's a little anonymous Andy.

69
00:03:27,410 --> 00:03:30,960
We'd like to have an anti bullying assembly
with your students this afternoon.

70
00:03:31,790 --> 00:03:33,460
Okey, But today is actually bad.

71
00:03:33,580 --> 00:03:36,500
I already have an assembly today
on positive thinking.

72
00:03:36,960 --> 00:03:40,130
You believe positive thinking
is really what's critical in schools right now?

73
00:03:40,260 --> 00:03:42,590
What's wrong with you?
I mean..

74
00:03:43,380 --> 00:03:45,220
Shut up!
What kind of counselor

75
00:03:45,350 --> 00:03:47,390
says no to an anti-bullying campaign?

76
00:03:47,560 --> 00:03:49,930
Bullying needs to be stopped, now.

77
00:03:50,180 --> 00:03:51,270
This afternoon.

78
00:03:54,600 --> 00:03:57,440
You may only have an Internet degree,
but why don't you start acting

79
00:03:57,570 --> 00:03:58,820
like a school counselor

80
00:03:58,940 --> 00:04:01,530
and not an uninformed
backwards little dork? Mkay!

81
00:04:08,990 --> 00:04:10,450
Attention, students.

82
00:04:11,870 --> 00:04:15,210
Today, we will have an assembly

83
00:04:15,750 --> 00:04:18,670
on the subject of bullying. Mkay

84
00:04:20,630 --> 00:04:22,970
<i>The assembly is mandatory. Mkay </i>

85
00:04:24,720 --> 00:04:26,140
<i>Better show up.</i>

86
00:04:29,720 --> 00:04:31,390
BUTTERS. Your grandma's looking for you.

87
00:04:31,810 --> 00:04:32,890
My grandma?

88
00:04:33,020 --> 00:04:35,230
She said to meet her outside
behind the school.

89
00:04:35,350 --> 00:04:36,230
Oh! Hamburgers!

90
00:04:41,110 --> 00:04:43,700
Hey, twerp!
You went narced on me.

91
00:04:45,820 --> 00:04:46,780
Hi, Grandma.

92
00:04:46,910 --> 00:04:50,290
I heard somebody brought in
an anti-bully counselor.

93
00:04:51,120 --> 00:04:53,410
Thought I wouldn't find out,
you little narc?

94
00:04:53,710 --> 00:04:55,790
I didn't narc.
It wasn't me.

95
00:04:56,210 --> 00:04:58,040
Look, what's this over here?

96
00:04:58,580 --> 00:05:00,170
It's a narc puck.

97
00:05:00,300 --> 00:05:02,840
This is what narcs
have to put in their mouth.

98
00:05:03,340 --> 00:05:05,470
It's got piss all over it.

99
00:05:06,050 --> 00:05:07,220
No, knock it off!

100
00:05:07,390 --> 00:05:08,640
No, stop it!

101
00:05:08,760 --> 00:05:11,720
Put it in your mouth.
Put it in your fucking mouth.

102
00:05:17,940 --> 00:05:20,570
Sorry.
This door isn't supposed to be locked.

103
00:05:20,730 --> 00:05:21,650
I'm sorry.

104
00:05:21,770 --> 00:05:25,070
I needed the rest room.
My grandson brought me to this one.

105
00:05:25,190 --> 00:05:26,900
Isn't that right?

106
00:05:28,280 --> 00:05:29,660
Butters, you goofball.

107
00:05:29,780 --> 00:05:31,660
I'll show you to the girls room. Mam

108
00:05:32,160 --> 00:05:33,700
Thank you so much.

109
00:05:34,370 --> 00:05:36,790
You narc again
and you're fucking dead.

110
00:05:36,910 --> 00:05:38,920
Fucking dead, you got it?

111
00:05:39,540 --> 00:05:40,710
I'm coming.

112
00:05:43,760 --> 00:05:44,880
What makes a bully?

113
00:05:45,170 --> 00:05:48,630
The truth is there are more bullies at your school
than you even think.

114
00:05:48,760 --> 00:05:52,850
The student who lets bullying happen
is just as bad as the bully himself.

115
00:05:52,970 --> 00:05:54,310
Come on out, Lorraine.

116
00:05:54,600 --> 00:05:57,480
I asked your schoolmate Lorraine here
to help me out.

117
00:05:57,600 --> 00:05:59,270
Are you bullied in the school, Lorraine?

118
00:06:02,360 --> 00:06:04,610
Kids pick on you, call you names?

119
00:06:05,440 --> 00:06:06,900
Sometimes.

120
00:06:07,320 --> 00:06:09,320
What kinds of things
do they say to you?

121
00:06:10,370 --> 00:06:11,410
Ugly.

122
00:06:12,030 --> 00:06:13,450
Nerd.

123
00:06:14,370 --> 00:06:17,210
Do they say, Nice pants.
Why do you wear them up to your tits?

124
00:06:21,170 --> 00:06:22,960
Bullying affects everyone.

125
00:06:23,090 --> 00:06:27,420
Only if the entire school is united Against,
can bullying ever be stopped.

126
00:06:27,880 --> 00:06:29,090
Go go, get out of here.

127
00:06:29,430 --> 00:06:32,510
What we, at Bucky Bailey's
Bully Bastards, like to do

128
00:06:32,640 --> 00:06:35,470
is get the schools
to make an anti-bullying video.

129
00:06:35,600 --> 00:06:38,980
Who would like to be
the director in charge of our video?

130
00:06:41,770 --> 00:06:45,320
We just need one student
to lead our anti-bullying campaign.

131
00:06:47,860 --> 00:06:49,070
Are you all chicken?

132
00:06:51,110 --> 00:06:53,700
Nobody wants to be
in charge of the anti-bully video?

133
00:06:57,540 --> 00:06:59,500
I'll do it.
I'll be in charge.

134
00:07:00,370 --> 00:07:03,130
Ooo.You're a big man. You wanna show
what a big man you are?

135
00:07:03,460 --> 00:07:07,420
No I just..I think bullying has gotten out of hand,
and it needs to be stopped.

136
00:07:07,840 --> 00:07:09,260
Good for you. STAN

137
00:07:09,420 --> 00:07:10,300
He's cool.

138
00:07:24,310 --> 00:07:27,360
You wanna know who I hate?
I hate that kid, BUTTERS.

139
00:07:27,480 --> 00:07:28,690
He's a dork.

140
00:07:29,570 --> 00:07:31,820
Let's go pick on him.

141
00:07:32,450 --> 00:07:34,530
Hey Guys, guys, don't pick on BUTTERS.

142
00:07:34,660 --> 00:07:37,200
That's not cool.
You can't do that.

143
00:07:37,330 --> 00:07:39,870
Bullying,
did you know that in America,

144
00:07:40,370 --> 00:07:44,920
over 200,000 students everyday are afraid
to come to school because of bullying?

145
00:07:45,040 --> 00:07:47,550
At South Park Elementary,
we're better than that.

146
00:07:47,670 --> 00:07:51,090
Come on! Let's all put an end to bullying,
right now!

147
00:07:51,220 --> 00:07:53,130
Five, six, seven, eight.

148
00:07:53,260 --> 00:07:56,430
<i>Bullying isn't cool
Bullying is lame</i>

149
00:07:56,550 --> 00:07:59,970
<i>Bullying is ugly
and has a stupid name</i>

150
00:08:00,100 --> 00:08:03,020
<i>For a healthy world,
bullying is unfit</i>

151
00:08:03,140 --> 00:08:06,560
<i>And I think I know
what we should do to it</i>

152
00:08:08,150 --> 00:08:10,740
<i>Let's all get together</i>

153
00:08:10,860 --> 00:08:14,240
<i>And make bullying kill itself</i>

154
00:08:14,360 --> 00:08:16,950
<i>Bullying is an ugly thing</i>

155
00:08:17,070 --> 00:08:19,450
<i>Let's shove its face in the dirt</i>

156
00:08:19,580 --> 00:08:22,370
<i>And make bullying kill itself</i>

157
00:08:27,590 --> 00:08:30,760
<i>Boy, you like my body
Let's play</i>

158
00:08:30,880 --> 00:08:34,510
<i>You can touch me anywhere
except my vajayjay</i>

159
00:08:34,630 --> 00:08:37,850
<i>We can make it stop
We can stop it out</i>

160
00:08:37,970 --> 00:08:41,140
<i>We can beat its ass
until it starts to cry</i>

161
00:08:41,270 --> 00:08:44,480
<i>Let's gang up on it
and tell it it smells</i>

162
00:08:44,600 --> 00:08:47,610
<i>And beat its ass worse
if it ever tells</i>

163
00:08:49,610 --> 00:08:52,110
<i>Let's all join together</i>

164
00:08:52,240 --> 00:08:55,610
<i>To try and make bullying kill itself</i>

165
00:08:55,740 --> 00:08:58,910
<i>It will be fun to see just how bad</i>

166
00:08:59,030 --> 00:09:00,990
<i>We can make it feel</i>

167
00:09:01,120 --> 00:09:03,790
<i>Make bullying kill itself</i>

168
00:09:08,790 --> 00:09:10,750
<i>My heart says yes</i>

169
00:09:10,880 --> 00:09:13,550
<i>But my vajayjay says no</i>

170
00:09:16,430 --> 00:09:19,470
<i>Trapped inside
the darkness of my mind</i>

171
00:09:19,600 --> 00:09:22,560
<i>I try to break free
The words are so unkind</i>

172
00:09:22,680 --> 00:09:24,270
Stupid.
Ugly.

173
00:09:24,390 --> 00:09:26,230
Pantsie.
Dork.

174
00:09:26,350 --> 00:09:28,770
You guys, can I not do this, please?

175
00:09:29,110 --> 00:09:30,310
I don't wanna do this.

176
00:09:31,270 --> 00:09:32,280
Oh BUTTERS, You ruined it!

177
00:09:32,400 --> 00:09:34,610
This is all one big long shot,
and you ruined it.

178
00:09:34,740 --> 00:09:37,200
But this is just gonna make
things worst for me.

179
00:09:37,320 --> 00:09:40,620
BUTTERS, you're the star of the video.
He doesn't want to do it dude.

180
00:09:40,740 --> 00:09:43,240
Come on, You wanna be bullied
your whole life, BUTTERS?

181
00:09:47,080 --> 00:09:48,170
KYLE, Dude,Where are you going?

182
00:09:50,170 --> 00:09:51,710
Why are you doing this? STAN

183
00:09:52,170 --> 00:09:53,500
To stop bullying.

184
00:09:53,630 --> 00:09:55,380
You're gonna stop bullying?
Yes

185
00:09:56,840 --> 00:09:58,930
With CARTMAN
singing about his vagina?

186
00:09:59,970 --> 00:10:01,180
It's about awareness dude.

187
00:10:01,300 --> 00:10:03,260
Don't you understand
how important this is?

188
00:10:03,390 --> 00:10:05,930
Bullying,
do you realize that in America,

189
00:10:06,060 --> 00:10:08,890
over 200,000 students
are afraid to come to school everyday because..

190
00:10:09,060 --> 00:10:11,860
Don't act for me. STAN
Really.

191
00:10:11,980 --> 00:10:14,610
Because every minute I'm watching this video
become less about awareness

192
00:10:14,730 --> 00:10:15,820
and more about you.

193
00:10:16,400 --> 00:10:17,610
KYLE, I'm making a difference.

194
00:10:18,610 --> 00:10:21,660
Just be careful you don't end up
naked and jacking it in San Diego.

195
00:10:23,780 --> 00:10:25,370
What the hell does that mean?

196
00:10:30,960 --> 00:10:34,800
Oh, Heavens to Betsy. Linda
This is such a yummy, yummy ham.

197
00:10:34,920 --> 00:10:37,260
I just love your cooking.
Yaaaaa

198
00:10:38,340 --> 00:10:41,010
Our Linda certainly
does know her way around a pork.

199
00:10:42,760 --> 00:10:44,010
What's the matter? BUTTERS

200
00:10:44,260 --> 00:10:46,310
Nothing dad, just a little gassy.

201
00:10:48,980 --> 00:10:51,230
Don't fart on Grandma.
She's trying to enjoy her ham.

202
00:10:51,560 --> 00:10:52,650
Okey dad I won't.

203
00:10:56,530 --> 00:10:57,650
I'll get it.

204
00:10:58,690 --> 00:11:00,950
Linda, Is that a new clock on the wall?

205
00:11:02,160 --> 00:11:03,820
Oh yes.
Stephen and I got that last month.

206
00:11:03,990 --> 00:11:05,490
It's from Germany, I believe.

207
00:11:05,620 --> 00:11:08,250
I just love the cute little canary
on the dial.

208
00:11:08,410 --> 00:11:10,750
And, every hour, it chimes.

209
00:11:10,920 --> 00:11:12,420
Your friends want to see you. BUTTERS

210
00:11:12,580 --> 00:11:14,290
Dude, BUTTERS. We have awesome news.

211
00:11:14,420 --> 00:11:16,960
A Hollywood movie company
is gonna buy our bullying video.

212
00:11:17,880 --> 00:11:21,680
An anti-bullying video?
How adorable.

213
00:11:23,760 --> 00:11:25,050
And BUTTERS is the star.

214
00:11:25,180 --> 00:11:28,020
They want to do a photo shoot
to make movie posters.

215
00:11:29,730 --> 00:11:30,940
I know, right!

216
00:11:42,410 --> 00:11:43,490
What's up?
Oh, Hey

217
00:11:46,200 --> 00:11:49,540
You went and made a video,
and sold it without letting me know.

218
00:11:50,080 --> 00:11:51,870
Doing the video was my idea.

219
00:11:52,000 --> 00:11:55,540
It's the property
of Bucky Baileys Bully Bastards.
You got that?


220
00:11:55,670 --> 00:11:58,210
But I worked hard on that.
It's been stressful and I..

221
00:11:58,340 --> 00:12:01,130
It's been stressful?
What's wrong with you?

222
00:12:01,260 --> 00:12:03,550
Kids are getting bullied at schools,
and with this money,

223
00:12:03,680 --> 00:12:07,350
Bucky Bailey's Bully Bastards
can become the legit organization

224
00:12:07,470 --> 00:12:08,720
it deserves to be.

225
00:12:08,850 --> 00:12:11,270
You greedy, selfish, little prick!

226
00:12:11,980 --> 00:12:13,600
Oh what,You are gonna cry?
No

227
00:12:14,520 --> 00:12:18,110
Go ahead, let me see you cry.

228
00:12:25,450 --> 00:12:29,030
You don't have a choice, BUTTERS.
You have to defend yourself.

229
00:12:29,370 --> 00:12:31,370
But violence is never the answer.

230
00:12:31,620 --> 00:12:33,910
But she's gonna kill you. BUTTERS
You know she is.

231
00:12:34,250 --> 00:12:36,710
You're right, I don't have a choice.

232
00:12:39,340 --> 00:12:42,210
Sorry, Grandma,
but you brought this on yourself.

233
00:12:42,590 --> 00:12:45,260
It's time you met Chaos.

234
00:12:47,510 --> 00:12:49,560
I've been pushed around
for the last time.

235
00:12:49,850 --> 00:12:52,600
Now I'm coming,
and heck's coming with me.

236
00:12:57,060 --> 00:12:59,730
Look, it's Captain Pussy.

237
00:13:01,440 --> 00:13:03,990
You can't stop me, Captain Pussy.

238
00:13:04,110 --> 00:13:05,700
Don't even try.

239
00:13:06,070 --> 00:13:07,910
Grandma?
But how...

240
00:13:08,070 --> 00:13:12,660
I got inspired when I came across
your gay little costume in your closet.

241
00:13:12,790 --> 00:13:17,250
Now come on, Captain Pussy.
Time for you to get your gummy bears.

242
00:13:18,250 --> 00:13:19,880
Grandma, please.
Not gummy bears.

243
00:13:20,040 --> 00:13:21,090
Come here!

244
00:13:21,880 --> 00:13:24,670
Grandma!
Stand up for yourself.

245
00:13:24,840 --> 00:13:28,680
Fight back, be a fucking man.
I can't.

246
00:13:28,800 --> 00:13:31,970
Then it looks like
you get gummy bears.

247
00:13:32,770 --> 00:13:34,680
Gummy bears!

248
00:13:42,020 --> 00:13:42,900
What's up?

249
00:13:43,030 --> 00:13:47,530
I'm Mick Jabs, president of the movie company
that bought the bully video.

250
00:13:49,240 --> 00:13:51,700
Stan Marsh got you
to come talk to me?

251
00:13:51,830 --> 00:13:54,830
That little
clich� conflict resolution Kevin.

252
00:13:54,950 --> 00:13:59,710
The video was conceived,
written and directed by the South Park's student party.
You got that?

253
00:13:59,830 --> 00:14:01,130
It was my idea.

254
00:14:01,250 --> 00:14:04,300
I told the students to make the video,
and I produced the entire thing.

255
00:14:04,420 --> 00:14:06,420
I deserve to have my name on it.

256
00:14:06,550 --> 00:14:08,880
Only problem is
America doesn't give a shit

257
00:14:09,010 --> 00:14:11,800
about an old fart
with a Captain Kangaroo haircut.

258
00:14:11,930 --> 00:14:14,520
They wanna believe
kids did something on their own.

259
00:14:14,680 --> 00:14:16,980
Here's a cease-and-desist letter
from our lawyers.

260
00:14:17,100 --> 00:14:21,900
If you ever claim any authorship,
we'll sue you for everything you have.

261
00:14:22,520 --> 00:14:25,940
But this was gonna be the thing
that finally made Bully Buckers 

262
00:14:26,070 --> 00:14:28,150
a national organization.

263
00:14:28,530 --> 00:14:29,950
Oh,You gonna cry?

264
00:14:30,070 --> 00:14:34,410
It will look bad
with your Captain Kangaroo haircut.

265
00:14:41,750 --> 00:14:43,750
K cool, Hold it right there.
Cool.

266
00:14:43,880 --> 00:14:45,550
Excuse me! What are you doing to my locker?

267
00:14:45,840 --> 00:14:48,470
We're putting up a movie poster.
The premiere's tomorrow.

268
00:14:48,590 --> 00:14:51,220
But since you walked out on the video,
you don't get to come.

269
00:14:51,890 --> 00:14:54,100
I don't wanna go
to your stupid movie premiere.

270
00:14:55,100 --> 00:14:56,850
And don't tape that to my locker!

271
00:15:00,730 --> 00:15:01,770
Oh, God.

272
00:15:02,270 --> 00:15:05,110
What's up, KYLE?
Why trash-talk our theatrical release?

273
00:15:05,230 --> 00:15:07,190
You really think
this is good for BUTTERS?

274
00:15:07,320 --> 00:15:10,570
To have his face put all over signs
as the poster child for bullying?

275
00:15:10,910 --> 00:15:13,950
BUTTERS is fine with it.
Yeah, well, BUTTERS is ten.

276
00:15:14,070 --> 00:15:16,450
He doesn't know what's best for him,
and neither do you.

277
00:15:16,620 --> 00:15:19,330
Alrite KYLE, That's enough.
I'm not gonna be bullied by you.
Okhey

278
00:15:19,710 --> 00:15:21,750
You were waiting for me
in the bathroom.
Oh yeah.

279
00:15:23,000 --> 00:15:26,250
This is all getting way too big.
Tell the studio you aren't selling video to them.

280
00:15:26,420 --> 00:15:28,710
This video can change
how people think about bullying.

281
00:15:28,840 --> 00:15:30,760
It needs to be seen by everybody. KYLE

282
00:15:30,880 --> 00:15:34,800
If it needs to be seen by everybody,
why not put it on the Internet for free?

283
00:15:40,140 --> 00:15:42,600
What's the question again?
If you really think

284
00:15:42,730 --> 00:15:45,690
every kid in America
should see your anti-bullying movie,

285
00:15:45,810 --> 00:15:48,860
why don't you put it
on the Internet for free?

286
00:15:49,570 --> 00:15:52,150
I'm going to the bathroom.
Fine.

287
00:15:53,030 --> 00:15:56,490
But when youre naked and jacking it
in San Diego, don't ask me for help.

288
00:15:58,080 --> 00:15:59,660
Why does he keep saying that?

289
00:16:04,420 --> 00:16:06,670
We all know that bullying
has become an epidemic.

290
00:16:06,790 --> 00:16:10,590
Like AIDS, bullying is escalating,
and it spreads mostly by penises.

291
00:16:10,710 --> 00:16:12,760
But a few kids
are trying to make a difference

292
00:16:12,880 --> 00:16:15,090
with a video
to make bullying kill itself.

293
00:16:15,640 --> 00:16:19,520
I'm joined by the director
and the main subject of the film.

294
00:16:19,770 --> 00:16:21,310
STAN, Congratulations on your success.

295
00:16:21,480 --> 00:16:23,560
I was tired of seeing kids like BUTTERS
getting pushed around

296
00:16:23,690 --> 00:16:24,980
and i knew I had to do something about it.

297
00:16:26,860 --> 00:16:30,320
And how about you, what would you like to say
to your bully out there?

298
00:16:30,900 --> 00:16:31,690
Nothing.

299
00:16:32,030 --> 00:16:33,900
This is for everyone
who's been a victim.

300
00:16:34,030 --> 00:16:36,370
What do you want to say to bullies
all across America?

301
00:16:36,490 --> 00:16:38,530
Go ahead, right now, say it.

302
00:16:38,660 --> 00:16:43,460
Stop trying to make me say things
on your TV show.

303
00:16:43,580 --> 00:16:44,670
Do you hear that?

304
00:16:44,790 --> 00:16:47,210
Stop making kids say things
on your TV shows.

305
00:16:49,210 --> 00:16:52,130
What else do you wanna say? BUTTERS.
Please, Leave me alone.

306
00:16:52,260 --> 00:16:53,510
Leave him alone.

307
00:16:53,630 --> 00:16:56,050
But that doesn't work. Does it BUTTERS
No, it doesn't.

308
00:16:56,180 --> 00:16:58,470
Tell us in graphic detail
what the bully does for you.

309
00:16:58,600 --> 00:17:00,890
Stop it!
Come on, This is for America.

310
00:17:01,020 --> 00:17:02,980
Do you realize that in America,

311
00:17:03,480 --> 00:17:06,650
over 200,000 students are afraid
to go to school because of bullying?

312
00:17:06,770 --> 00:17:08,690
Don't you care?
You better care.

313
00:17:22,370 --> 00:17:24,080
What the hell were you thinking?

314
00:17:24,200 --> 00:17:28,500
Everyone just saw that the bully victim
in your movie is a violent psychopath.

315
00:17:28,920 --> 00:17:31,500
I didn't know.
This could kill our box office.

316
00:17:31,630 --> 00:17:34,420
People are gonna come after us
saying we didn't check our facts.

317
00:17:34,720 --> 00:17:36,550
I didn't beat up Dr. Oz.
Don't be mad at me.

318
00:17:36,800 --> 00:17:38,640
It's your fucking movie!

319
00:17:38,760 --> 00:17:40,970
Now, I have to work overtime
with marketing

320
00:17:41,100 --> 00:17:43,350
to make sure people
still sympathize with our victim.

321
00:17:43,770 --> 00:17:46,850
Get the fuck out of here.
I got to go to the bathroom.

322
00:17:50,730 --> 00:17:53,690
My child, have you ever heard
of a place called hell?

323
00:17:53,820 --> 00:17:57,150
It's eternal fire,
and it's gonna hurt real bad.

324
00:17:59,870 --> 00:18:01,910
What are you gonna do, cry now?

325
00:18:06,250 --> 00:18:08,870
Grandma, I did it.

326
00:18:09,370 --> 00:18:11,130
I finally stood up for myself.

327
00:18:11,250 --> 00:18:14,130
I got real mean,
and I beat the snot out of Dr. Oz.

328
00:18:14,800 --> 00:18:16,760
I can't lie, it felt kind of good,

329
00:18:16,880 --> 00:18:17,930
at first.

330
00:18:18,220 --> 00:18:21,390
But since then,
I have this dark empty feeling.

331
00:18:22,470 --> 00:18:23,850
Then I realized

332
00:18:24,310 --> 00:18:26,770
that's how you must feel,
all the time.

333
00:18:27,770 --> 00:18:29,190
Poor old Grandma.

334
00:18:29,310 --> 00:18:31,810
I've been given lots of advice
on how to deal with you.

335
00:18:31,940 --> 00:18:34,070
Stand up to you, tell on you�

336
00:18:34,530 --> 00:18:37,150
But I realized
there are people like you out there,

337
00:18:37,530 --> 00:18:39,030
all over the place.

338
00:18:39,610 --> 00:18:42,950
When youre a kid,
things seem like they'll last forever,

339
00:18:43,080 --> 00:18:44,200
but they won't.

340
00:18:44,450 --> 00:18:47,580
Life changes.
You won't always be around.

341
00:18:48,160 --> 00:18:49,870
Someday, you're gonna die.

342
00:18:51,040 --> 00:18:52,580
Someday pretty soon.

343
00:18:53,500 --> 00:18:55,670
And when you're laying
in that hospital bed,

344
00:18:55,800 --> 00:18:57,630
with tubes up your nose,

345
00:18:57,760 --> 00:19:00,130
and that little pan
under your butt to pee in,

346
00:19:00,260 --> 00:19:01,800
I'll come visit you.

347
00:19:01,930 --> 00:19:04,640
I'll come just to show you
I'm alive and happy.

348
00:19:04,890 --> 00:19:06,060
And you'll die,

349
00:19:06,810 --> 00:19:08,390
being nothing but you.

350
00:19:10,270 --> 00:19:11,400
Good night.

351
00:19:20,360 --> 00:19:21,240
Come on.

352
00:19:21,910 --> 00:19:23,780
What's going on?
You didn't hear?

353
00:19:23,910 --> 00:19:26,370
Now that America
knows BUTTERS is a violent psychopath,

354
00:19:26,490 --> 00:19:28,250
they say you made a bullcrap video.

355
00:19:29,290 --> 00:19:31,870
What did the studio say about it?
The studio backed out.

356
00:19:32,040 --> 00:19:33,790
The producer had a change of heart.

357
00:19:35,000 --> 00:19:37,380
You made us
look like uncaring idiots.

358
00:19:38,880 --> 00:19:41,630
How was I supposed to know
BUTTERS was a violent psychopath?

359
00:19:43,090 --> 00:19:45,390
Everyone loved me sixteen hours ago.

360
00:19:46,930 --> 00:19:49,470
STAN, STAN, 
Mkay ABC called, and Dr. Oz is suing you

361
00:19:49,600 --> 00:19:51,100
and our entire school.

362
00:19:51,350 --> 00:19:52,640
What are you gonna do?

363
00:19:53,980 --> 00:19:56,940
There's only one thing left
for me to do.

364
00:20:06,780 --> 00:20:10,580
<i>I'm gonna jack it
where the sun always shines</i>

365
00:20:11,000 --> 00:20:14,960
<i>Been spreading the word,
and now I need to ease my mind</i>

366
00:20:15,250 --> 00:20:18,750
<i>Been planting them apple seeds,
and while the apples grow</i>

367
00:20:19,250 --> 00:20:23,340
<i>I'm gonna go out
jacking it in San Diego</i>

368
00:20:23,630 --> 00:20:25,590
<i>Jacking it, jacking it,
jackety-jack</i>

369
00:20:25,720 --> 00:20:28,310
<i>Spanking it, jacking it,
spankety-smack</i>

370
00:20:28,470 --> 00:20:32,020
<i>I don't need no shirt, no
Gonna to take them pants right off</i>

371
00:20:32,600 --> 00:20:36,400
<i>On such a bright day,
who needs underwear or socks</i>

372
00:20:36,860 --> 00:20:40,530
<i>Been around God’s country,
and there’s one thing I know</i>

373
00:20:40,780 --> 00:20:44,530
<i>There's no better place
for jacking it than San Diego</i>

374
00:20:45,200 --> 00:20:47,120
<i>Jack it, jack it,
jackety-jack</i>

375
00:20:47,240 --> 00:20:49,330
<i>Spanking it, spanking it,
smackety-smack</i>

376
00:20:49,450 --> 00:20:51,370
<i>Jacking it, jacking it,
jackety-jack</i>

377
00:20:51,500 --> 00:20:53,870
<i>Jacking for the Lord!</i>

378
00:20:56,080 --> 00:20:58,670
Come to San Diego.
There's so much to see.

379
00:20:58,790 --> 00:21:02,420
From the waters of Mission Bay
to the warm tortillas of Old Town.

380
00:21:02,550 --> 00:21:04,050
And after a day of sightseeing,

381
00:21:04,170 --> 00:21:06,840
why not try spanking it
in one of our charming city streets?

382
00:21:07,390 --> 00:21:10,310
San Diego.
Come, take a load off.

383
00:21:10,930 --> 00:21:12,810
<i>Jacking it, jacking it,
jackety-jack</i>

384
00:21:12,930 --> 00:21:14,770
<i>Spanking it, spanking it,
smackety-smack</i>

385
00:21:15,100 --> 00:21:17,190
<i>Whacking it, whacking it,
whackety-whack</i>

386
00:21:17,350 --> 00:21:19,060
<i>Spanking it, jerking it,
smackety-smack</i>

387
00:21:19,690 --> 00:21:22,730
<i>The cars are passing me by
They honk and say hello</i>

388
00:21:22,860 --> 00:21:24,110
That guys jacking it!

389
00:21:24,280 --> 00:21:27,240
<i>From his window,
there’s a guy shooting video</i>

390
00:21:28,240 --> 00:21:31,870
<i>And if the good Lord Jesus
comes knocking on my door</i>

391
00:21:31,990 --> 00:21:36,290
<i>Just tell him
that I'm jacking it in San Diego</i>

392
00:21:36,620 --> 00:21:38,540
<i>Jacking it, jacking it,
jackety-jack</i>

393
00:21:38,670 --> 00:21:41,090
<i>Spanking it, spanking it,
smackety-smack</i>

394
00:21:41,210 --> 00:21:44,590
<i>Jacking it Jack, jacking it Jack
Whack, whacking it, whacking it</i>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
