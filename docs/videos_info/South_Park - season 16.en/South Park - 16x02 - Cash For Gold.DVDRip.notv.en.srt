1
00:00:34,794 --> 00:00:37,421
Well, dad, it was really great seeing you.

2
00:00:37,422 --> 00:00:38,780
We would love to stay for dinner

3
00:00:38,781 --> 00:00:41,114
but the food here gives Sharon diarrhea.

4
00:00:41,115 --> 00:00:42,582
What, Randy.

5
00:00:42,689 --> 00:00:44,824
Just trying to leave without being rude.

6
00:00:44,825 --> 00:00:47,369
Hold on, hold on just a second.

7
00:00:47,370 --> 00:00:49,805
I got a present for my grandson.

8
00:00:49,983 --> 00:00:51,265
Come here, Billy.

9
00:00:51,718 --> 00:00:53,285
You have have grown up, Billy.

10
00:00:53,286 --> 00:00:56,593
It's time for you to have
something expensive and lashy

11
00:00:56,594 --> 00:00:58,708
to impress all the ladies.

12
00:00:59,567 --> 00:01:01,267
Go ahead and open it, Stan.

13
00:01:03,890 --> 00:01:05,232
Oh, look at that.

14
00:01:05,233 --> 00:01:06,997
A bolo tie.

15
00:01:06,998 --> 00:01:09,298
Isn't that beautiful, Stan.

16
00:01:09,299 --> 00:01:13,760
That's 14-carat gold with
turquoise and real diamonds.

17
00:01:13,761 --> 00:01:15,754
Grandpa, how much did you spend on that?

18
00:01:15,755 --> 00:01:17,142
$6000.

19
00:01:17,143 --> 00:01:18,769
Six thousand?

20
00:01:18,770 --> 00:01:20,666
It's worth 50,000.

21
00:01:20,667 --> 00:01:23,278
The jewels and gem show said so.

22
00:01:23,279 --> 00:01:27,153
Dad, you shouldn't spend your
retirement money on frivolous things.

23
00:01:27,154 --> 00:01:29,314
You should save it for when you die.

24
00:01:29,315 --> 00:01:30,753
That's our money.

25
00:01:30,871 --> 00:01:32,897
It's gorgeous, dad. Thank you.

26
00:01:32,898 --> 00:01:34,583
Tomorrow is picture day at school.

27
00:01:34,584 --> 00:01:36,652
Stan can wear it for his photos.

28
00:01:36,653 --> 00:01:40,434
Oh that's wonderful.
That will make me feel really good.

29
00:01:40,461 --> 00:01:41,780
Who is Stan?

30
00:01:49,553 --> 00:01:51,205
Nice bolo tie, Stan.

31
00:01:51,603 --> 00:01:52,846
Thanks.

32
00:01:53,968 --> 00:01:56,458
Bolo ties are in now.

33
00:01:56,459 --> 00:01:58,009
It's cool you have one.

34
00:01:58,384 --> 00:02:00,082
Look it was a gift from my grandpa, okay.

35
00:02:00,083 --> 00:02:01,450
It cost a lot of money.

36
00:02:02,041 --> 00:02:04,049
No dude it's bad ass.

37
00:02:04,080 --> 00:02:06,612
It happens to be worth $6000.

38
00:02:06,613 --> 00:02:08,304
That was six grand?

39
00:02:08,305 --> 00:02:12,154
Yeah, dude, it's a recreation of
the bolo tie worn by king Henry V.

40
00:02:14,351 --> 00:02:16,236
Dude it's gay.

41
00:02:16,634 --> 00:02:17,737
I know.

42
00:02:17,738 --> 00:02:20,795
I wish grandpa would just give the money.

43
00:02:20,796 --> 00:02:22,736
Take it to one of those pawn places.

44
00:02:22,737 --> 00:02:25,750
Every two blocks you see a guy with a sign
that says cash for gold and jewelry.

45
00:02:25,751 --> 00:02:27,419
There must be a lot of people doing it.

46
00:02:39,793 --> 00:02:41,495
You didn't steal this, did you?

47
00:02:41,515 --> 00:02:43,508
No my grandpa gave it to me as a present.

48
00:02:43,509 --> 00:02:45,719
It's worth much and I
feel bad accepting it.

49
00:02:45,720 --> 00:02:47,210
I wan the cash.

50
00:02:47,553 --> 00:02:49,978
14-carat gold, diamonds,

51
00:02:49,979 --> 00:02:52,728
turquoise...
I'll give you 15$.

52
00:02:53,927 --> 00:02:57,078
What?
That cost my grandpa $6000.

53
00:02:57,079 --> 00:02:58,859
Those are real diamonds on the outside.

54
00:02:58,860 --> 00:03:01,076
I can't make anything on the diamonds.

55
00:03:01,077 --> 00:03:02,623
I have to send it to the smelter.

56
00:03:02,624 --> 00:03:06,181
I can make a 10-dollar profit.

57
00:03:06,182 --> 00:03:07,535
15 bucks?

58
00:03:07,536 --> 00:03:08,745
This guy is trying to rook us.

59
00:03:08,746 --> 00:03:09,675
Let's go somewhere else.

60
00:03:09,676 --> 00:03:11,594
Yeah, I'm not getting taken advantage of.

61
00:03:11,595 --> 00:03:13,622
You can suck our collective balls, sir.

62
00:03:14,813 --> 00:03:18,030
Don't worry. There must be another
cash for gold place around here.

63
00:03:18,393 --> 00:03:19,747
Here is one.

64
00:03:21,335 --> 00:03:23,141
These are real diamonds, right?

65
00:03:23,142 --> 00:03:24,725
Yeah, dude you can test them.

66
00:03:25,298 --> 00:03:27,499
Okay. $8.

67
00:03:28,691 --> 00:03:30,277
This is the same bolo tie

68
00:03:30,278 --> 00:03:32,324
worn by king Henry V.

69
00:03:33,816 --> 00:03:35,145
$9.

70
00:03:37,880 --> 00:03:38,965
Welcome to taco bell.

71
00:03:38,966 --> 00:03:41,440
Would you like to try our
doritos locos tacos.

72
00:03:41,441 --> 00:03:42,286
I want to see how much will

73
00:03:42,287 --> 00:03:44,345
you give me for this gold and diamond bolo.

74
00:03:47,400 --> 00:03:48,480
14-carat gold.

75
00:03:48,481 --> 00:03:50,115
$14 a gram.

76
00:03:50,116 --> 00:03:53,368
On the open market got some 4 grams here.

77
00:03:53,411 --> 00:03:55,199
Not really worth my time.

78
00:03:55,200 --> 00:03:58,304
I guess I can give you a
6 layer burrito for it.

79
00:03:58,670 --> 00:04:00,159
A six layer burrito?

80
00:04:00,160 --> 00:04:02,563
You don't even make a six layer burrito.

81
00:04:02,649 --> 00:04:04,533
Alright, a 7 layer burrito,

82
00:04:04,534 --> 00:04:06,304
that's as high as I'm going!

83
00:04:09,415 --> 00:04:11,904
Dude, my grandpa paid $6000

84
00:04:11,905 --> 00:04:13,785
for something barely worth anything.

85
00:04:13,795 --> 00:04:16,855
How, how does something like this happen?

86
00:04:18,996 --> 00:04:22,770
Okay. Folks we're -- half way complete

87
00:04:22,771 --> 00:04:24,548
with today's broadcast.

88
00:04:24,671 --> 00:04:27,149
You want to get on in on
these deals call now.

89
00:04:27,490 --> 00:04:29,232
Next item is --

90
00:04:29,334 --> 00:04:30,071
this is item

91
00:04:30,072 --> 00:04:34,519
number 457-8111.

92
00:04:35,431 --> 00:04:37,907
Look at these stunning earrings.

93
00:04:37,908 --> 00:04:42,129
These are genuine faux sapphire earrings.

94
00:04:42,130 --> 00:04:46,892
14 carat gold, 86 carat faux sapphire,

95
00:04:47,091 --> 00:04:49,094
foe is a French word it has a

96
00:04:49,095 --> 00:04:51,749
"X" in it you don't pronounce the X.

97
00:04:51,750 --> 00:04:53,925
How do you like that for prestigious.

98
00:04:53,926 --> 00:04:57,621
These earrings go for $6 million.

99
00:04:57,622 --> 00:05:00,220
We're selling these today for --

100
00:05:01,799 --> 00:05:03,874
$320.

101
00:05:03,875 --> 00:05:04,844
That's a steal.

102
00:05:04,845 --> 00:05:05,937
Now there go the phones.

103
00:05:05,938 --> 00:05:07,278
They're lighting up.

104
00:05:07,279 --> 00:05:08,558
I believe we have a sale.

105
00:05:08,559 --> 00:05:09,705
Do we have a sale?

106
00:05:09,706 --> 00:05:11,424
Yep, let's get her on the line.

107
00:05:11,425 --> 00:05:12,148
Hello.

108
00:05:12,149 --> 00:05:13,566
Who am I speaking with?

109
00:05:13,690 --> 00:05:14,962
Hello.

110
00:05:15,169 --> 00:05:17,747
My name is Vivian.

111
00:05:17,748 --> 00:05:19,666
Vivian, you got a heck of a deal.

112
00:05:19,667 --> 00:05:21,581
What's your last name sweetheart?

113
00:05:22,378 --> 00:05:25,658
I can't remember.

114
00:05:25,659 --> 00:05:26,689
You can't remember.

115
00:05:26,690 --> 00:05:29,439
Well can you remember
your credit card number?

116
00:05:32,096 --> 00:05:33,695
3715 --

117
00:05:33,696 --> 00:05:37,157
hold on, Vivian, we will get you
on with a rep for the number.

118
00:05:37,158 --> 00:05:38,492
Thank you for shopping with us.

119
00:05:38,493 --> 00:05:43,128
Congratulations on the 14-carat
faux sapphire earrings.

120
00:05:43,193 --> 00:05:46,242
At that price, you practically
stole them from us.

121
00:05:47,390 --> 00:05:48,775
That's terrible.

122
00:05:48,776 --> 00:05:50,625
I told you guys,
I have been watching all day.

123
00:05:50,626 --> 00:05:52,206
How do they get away with that?

124
00:05:52,207 --> 00:05:54,381
This is a new time, a new era of science

125
00:05:54,382 --> 00:05:56,148
that only the smartest can comprehend.

126
00:05:56,149 --> 00:05:57,454
What are you talking about?

127
00:05:57,738 --> 00:06:00,543
For centuries, alchemists have
tried to come up with a formula

128
00:06:00,544 --> 00:06:01,913
to make gold.

129
00:06:01,914 --> 00:06:04,456
Whoever could do it would
have become rich and now

130
00:06:04,457 --> 00:06:06,858
the chemical equation is before our eyes.

131
00:06:08,418 --> 00:06:10,408
That's the chemical equation for gold?

132
00:06:10,484 --> 00:06:12,703
That's right. Guys with cash for gold signs

133
00:06:12,704 --> 00:06:14,817
get you peoples unwanted crappy jewelry.

134
00:06:14,818 --> 00:06:17,208
Which when added to a cable
base shopping network

135
00:06:17,209 --> 00:06:20,193
divided by demented old people equals

136
00:06:20,331 --> 00:06:21,704
gold.

137
00:06:22,167 --> 00:06:23,778
Kenny, will you tell Carlton to shut up?

138
00:06:23,779 --> 00:06:25,808
Shut up, you're an asshole.

139
00:06:25,809 --> 00:06:27,630
Oh, I'm an asshole for doing that?

140
00:06:27,631 --> 00:06:30,086
Oh my God, can you believe this?

141
00:06:30,089 --> 00:06:31,470
Someone is about to get this

142
00:06:31,471 --> 00:06:36,323
20,000-dollar topaz and
copper ring for $4000.

143
00:06:36,324 --> 00:06:37,960
We've got our buyer on the line.

144
00:06:37,961 --> 00:06:39,956
Are you buying this as a gift, sir?

145
00:06:39,971 --> 00:06:42,135
No, I'm buying this as a gift

146
00:06:42,136 --> 00:06:44,052
for my grandson, Billy.

147
00:06:44,673 --> 00:06:45,936
Grandpa.

148
00:06:49,331 --> 00:06:50,360
How about that, folks.

149
00:06:50,361 --> 00:06:52,647
That's Brazilian emerald.

150
00:06:52,718 --> 00:06:54,459
Finest emerald available.

151
00:06:54,642 --> 00:06:56,289
We're letting this go for

152
00:06:56,784 --> 00:06:59,461
$1495 e-z pay.

153
00:07:00,035 --> 00:07:01,021
E-z pay.

154
00:07:01,022 --> 00:07:03,385
We call it that to save you time.

155
00:07:03,604 --> 00:07:07,069
E-z is an a abbreviation for easy.

156
00:07:07,070 --> 00:07:09,778
$1495 e-z pay.

157
00:07:10,271 --> 00:07:11,578
What's that?

158
00:07:11,597 --> 00:07:14,518
Okay, I got word we're dropping

159
00:07:14,519 --> 00:07:16,976
the "Z" from e-Z pay.

160
00:07:16,977 --> 00:07:19,267
It's now just e-pay.

161
00:07:19,572 --> 00:07:21,545
By using the word e-pay instead

162
00:07:21,546 --> 00:07:24,370
of taking that time to say e-z pay

163
00:07:24,980 --> 00:07:27,106
we're saving you a second of time.

164
00:07:27,107 --> 00:07:28,831
Those add up.

165
00:07:28,934 --> 00:07:32,434
Go ahead and try it.
Say e-pay five thousand times.

166
00:07:32,435 --> 00:07:35,568
That's 5000 seconds, nine hours

167
00:07:35,569 --> 00:07:38,951
we just saved you on the
J & G shopping network.

168
00:07:38,993 --> 00:07:40,402
Not wasting your time here.

169
00:07:40,403 --> 00:07:42,265
You can't afford not to buy this one.

170
00:07:42,266 --> 00:07:45,013
You don't have a lot of
time left, literally.

171
00:07:45,014 --> 00:07:47,529
Pass this to your kids and grand kids.

172
00:07:47,530 --> 00:07:49,303
Show them your life had meaning.

173
00:07:49,304 --> 00:07:50,677
Grandpa.

174
00:07:50,981 --> 00:07:55,407
See that Billy, that's an
emerald on 14 carat gold.

175
00:07:55,408 --> 00:07:57,760
Think your sister would like that?

176
00:07:58,002 --> 00:07:59,688
She doesn't like jewelry, grandpa.

177
00:08:00,135 --> 00:08:01,580
She will one day.

178
00:08:01,581 --> 00:08:03,204
She will appreciate.

179
00:08:03,347 --> 00:08:05,744
She's just a baby after all.

180
00:08:06,397 --> 00:08:07,512
She's not a baby, grandpa.

181
00:08:07,513 --> 00:08:08,759
She's 13.

182
00:08:09,559 --> 00:08:10,958
Sally is 13?

183
00:08:12,184 --> 00:08:13,393
Right.

184
00:08:13,460 --> 00:08:14,722
Right. Boy.

185
00:08:17,154 --> 00:08:20,924
Billy, did I ever tell you I
use to have a border collie

186
00:08:20,925 --> 00:08:22,340
named Patches.

187
00:08:22,341 --> 00:08:23,582
Yes, grandpa.

188
00:08:23,750 --> 00:08:25,285
I loved that dog.

189
00:08:25,510 --> 00:08:27,856
She always made me so happy.

190
00:08:28,036 --> 00:08:31,411
When she died I, I didn't
let myself get too sad.

191
00:08:31,412 --> 00:08:35,162
I thought, I thought I would
always have the memory of her

192
00:08:35,163 --> 00:08:36,908
slobbering happy face.

193
00:08:39,065 --> 00:08:41,640
I can't remember what
she looked like, Billy.

194
00:08:48,271 --> 00:08:51,152
Don't worry, grandpa, I'm
going to take care of.

195
00:08:57,174 --> 00:08:58,794
Hey, Craig, what's going on?

196
00:08:59,498 --> 00:09:03,013
Bet your mom has some more jewelry
she won't notice missing.

197
00:09:03,681 --> 00:09:05,895
You have rhinestones in those earrings.

198
00:09:05,896 --> 00:09:07,354
How about some walking cash?

199
00:09:07,355 --> 00:09:09,267
I can probably offer you...

200
00:09:09,899 --> 00:09:11,139
What the fuck.

201
00:09:11,140 --> 00:09:12,689
Sorry my arm's hurt.

202
00:09:12,690 --> 00:09:13,585
My arm's is hurt.

203
00:09:13,586 --> 00:09:16,043
Pick the sign, Butters this is
a business.

204
00:09:17,990 --> 00:09:20,087
How much will you give me for this?

205
00:09:20,088 --> 00:09:21,781
Oh, um, 3$.

206
00:09:21,782 --> 00:09:23,397
Okay.

207
00:09:23,538 --> 00:09:25,345
We got crappy jewelry, Butters.

208
00:09:25,381 --> 00:09:27,752
Now all we need are some old people.

209
00:09:30,919 --> 00:09:33,301
That's it, we just sold this

210
00:09:33,302 --> 00:09:36,171
bracelet to miss Marsha tubbs.

211
00:09:36,918 --> 00:09:39,422
Marsha, thank you for your call.

212
00:09:39,423 --> 00:09:42,640
You got yourself a heck
of a deal on this one.

213
00:09:42,641 --> 00:09:44,042
You there, Marsha?

214
00:09:46,325 --> 00:09:47,800
I have lost --

215
00:09:47,944 --> 00:09:50,566
I'm lost walking on the freeway.

216
00:09:50,567 --> 00:09:52,838
Alright, you're lost walking on a freeway.

217
00:09:52,839 --> 00:09:56,496
Enjoy the tiger's eye aquamarine bracelet.

218
00:09:56,592 --> 00:09:59,126
What should we do next?

219
00:09:59,590 --> 00:10:01,325
Oh, I see one.
Here is a good one.

220
00:10:01,445 --> 00:10:02,464
Let me...

221
00:10:03,543 --> 00:10:04,182
Let me --

222
00:10:04,316 --> 00:10:06,496
let me, let me set stage for you here.

223
00:10:07,118 --> 00:10:10,334
You are going to that
senior's cocktail party.

224
00:10:10,700 --> 00:10:11,791
It's bingo night.

225
00:10:11,792 --> 00:10:13,053
You're looking for something to wear.

226
00:10:14,061 --> 00:10:18,129
How about a 13-carat panzotopanzanite ring.

227
00:10:18,560 --> 00:10:21,095
This is...
We have a caller already on this one.

228
00:10:21,199 --> 00:10:24,536
Hello, sir, you must be a
fan of panzotopanzanite.

229
00:10:24,842 --> 00:10:25,941
Yes, hi.

230
00:10:26,061 --> 00:10:27,005
You should kill yourself.

231
00:10:28,668 --> 00:10:29,315
What is that?

232
00:10:29,634 --> 00:10:31,057
I said you should kill yourself.

233
00:10:31,647 --> 00:10:33,546
What you do is unjustifiable.

234
00:10:34,241 --> 00:10:35,541
You know it's unjustifiable.

235
00:10:36,447 --> 00:10:37,415
You don't care.

236
00:10:38,184 --> 00:10:39,371
You are the definition of evil.

237
00:10:40,595 --> 00:10:41,392
Kill yourself.

238
00:10:43,019 --> 00:10:47,054
Okay. We're going to sell
this ring for just $3795.

239
00:10:47,291 --> 00:10:48,106
How is that?

240
00:10:48,730 --> 00:10:51,523
I just read the day shopping
networks make their money

241
00:10:51,524 --> 00:10:54,102
on the day seniors pick up
their social security checks.

242
00:10:54,694 --> 00:10:55,437
Kill yourself.

243
00:10:55,774 --> 00:10:58,673
Alright, you shouldn't say
things like that, cause

244
00:10:58,827 --> 00:11:01,349
a host of a jewelry
channel may up and do it.

245
00:11:01,350 --> 00:11:02,687
You would feel bad.

246
00:11:03,165 --> 00:11:03,860
No, I wouldn't.

247
00:11:04,079 --> 00:11:05,079
Yes, you would.

248
00:11:05,713 --> 00:11:07,718
No, because I really want
you to kill yourself.

249
00:11:08,009 --> 00:11:12,306
How about this, if a jewelry host
network show host goes home tonight

250
00:11:12,307 --> 00:11:13,613
and blows his brains out.

251
00:11:14,044 --> 00:11:15,404
You might be liable.

252
00:11:15,405 --> 00:11:17,212
That's a lawsuit worth

253
00:11:17,761 --> 00:11:20,108
$2.7 million.

254
00:11:20,228 --> 00:11:21,303
How does that sound?

255
00:11:21,604 --> 00:11:22,945
I don't care what happens to me.

256
00:11:22,946 --> 00:11:24,547
I care about my grandfather.

257
00:11:24,647 --> 00:11:27,836
You morally empty corrupted maggot.

258
00:11:31,848 --> 00:11:36,080
Okay. I tell you stuff.
I will bring the lawsuit down to 2939 --

259
00:11:36,081 --> 00:11:38,258
it doesn't matter the price
you put on anything.

260
00:11:38,432 --> 00:11:41,439
Your only chance to right the
wrongs you've done and repay

261
00:11:41,440 --> 00:11:42,911
the elderly peoples whose lives

262
00:11:42,912 --> 00:11:44,829
you have destroyed is to kill yourself.

263
00:11:48,742 --> 00:11:50,134
You think that's funny.

264
00:11:50,135 --> 00:11:52,048
That's calling someone and

265
00:11:52,049 --> 00:11:54,791
telling them to kill themselves.
That's not a joke.

266
00:11:55,312 --> 00:11:56,109
I'm not joking.

267
00:11:57,955 --> 00:11:58,735
Do it.

268
00:12:02,280 --> 00:12:03,424
Okay, next item.

269
00:12:03,702 --> 00:12:04,814
Next item we're gonna do is

270
00:12:04,815 --> 00:12:08,706
355216775..

271
00:12:09,030 --> 00:12:10,789
This is... oh my God look at this, guys.

272
00:12:11,032 --> 00:12:14,126
200-carat Brazilian emrand
and plastecine ring.

273
00:12:14,262 --> 00:12:18,138
I'm gonna start the bidding on
this ring at, say, $8 billion.

274
00:12:18,641 --> 00:12:20,065
$8 billion opening bid.

275
00:12:20,213 --> 00:12:21,503
We have to sell this ring today.

276
00:12:22,046 --> 00:12:26,506
We are going to drop the price to $75.95.

277
00:12:26,507 --> 00:12:30,595
This price is not gonna last...
Oh, we have a call alright, Butters?

278
00:12:31,304 --> 00:12:34,318
We sold it.
Do we have the buyer on the line.

279
00:12:34,319 --> 00:12:35,795
Hello. Hello.

280
00:12:36,036 --> 00:12:38,044
You bought this lovely 200-carat ring.

281
00:12:38,045 --> 00:12:39,482
How do you feel, Mrs.?

282
00:12:39,803 --> 00:12:43,390
Should is Mrs. apple bee.

283
00:12:43,425 --> 00:12:44,902
Can I ask you something Mrs. apple bee.

284
00:12:44,903 --> 00:12:46,253
Do you like fucking little boys?

285
00:12:49,166 --> 00:12:49,995
I'm sorry.

286
00:12:50,472 --> 00:12:52,029
Just wondering if you
fuck kids all the time?

287
00:12:52,030 --> 00:12:53,703
That's what you did with this deal.

288
00:12:53,719 --> 00:12:56,622
You just got an 8
billion-dollar ring for $79.95.

289
00:12:56,623 --> 00:12:58,932
You fucked me good, Mrs. apple bee.
Congratulations, ma'am.

290
00:12:59,019 --> 00:12:59,721
Thank you.

291
00:12:59,984 --> 00:13:02,243
***

292
00:13:02,244 --> 00:13:03,833
thinking I'd like to fuck that kid?

293
00:13:05,609 --> 00:13:08,609
I thought it would be a lovely gift
for my granddaughter, Jessica.

294
00:13:08,811 --> 00:13:10,325
She's captain of the debate team

295
00:13:10,326 --> 00:13:11,572
at Jefferson high school.

296
00:13:11,573 --> 00:13:13,174
Okay. Thank you for shopping with us.

297
00:13:13,175 --> 00:13:14,286
I have to get the taste of old

298
00:13:14,287 --> 00:13:15,314
lady dick out of my mouth.

299
00:13:16,710 --> 00:13:18,303
Alright.

300
00:13:18,506 --> 00:13:19,612
Man that's good acting.

301
00:13:19,697 --> 00:13:20,673
I should get an award.

302
00:13:36,277 --> 00:13:37,772
Do you have any idea what it

303
00:13:37,773 --> 00:13:39,530
would feel like to lose your memories?

304
00:13:39,531 --> 00:13:41,214
No, you don't have someone in

305
00:13:41,215 --> 00:13:42,718
your life suffering from Alzheimers.

306
00:13:42,719 --> 00:13:43,533
Well I do.

307
00:13:43,796 --> 00:13:44,623
Look, kid.

308
00:13:44,745 --> 00:13:46,183
If you have a beef with the system.

309
00:13:46,184 --> 00:13:47,546
You're talking to the wrong people.

310
00:13:47,932 --> 00:13:49,929
We smelter what we get from the

311
00:13:49,930 --> 00:13:51,218
cash for gold places.

312
00:13:51,523 --> 00:13:54,242
Yeah? Well, there is a old Hindu saying

313
00:13:54,656 --> 00:13:56,582
"who ever smelt it dealt it."

314
00:13:56,855 --> 00:13:58,467
Ya.

315
00:13:58,527 --> 00:14:00,417
We are not the ones denying

316
00:14:00,418 --> 00:14:02,252
you what your worthy is worth.

317
00:14:02,554 --> 00:14:06,969
The Hindu saying is ***.

318
00:14:07,152 --> 00:14:08,195
What does that mean?

319
00:14:08,381 --> 00:14:11,308
Who ever denied it supplied it.

320
00:14:15,015 --> 00:14:16,805
You, are the scums of the earth.

321
00:14:17,176 --> 00:14:19,414
Old people are victimized
by shopping networks.

322
00:14:19,415 --> 00:14:21,486
You kickback in your fat Kat

323
00:14:21,487 --> 00:14:22,710
magses making billions.

324
00:14:24,458 --> 00:14:26,452
We aren't making that much.

325
00:14:27,549 --> 00:14:28,234
You're not?

326
00:14:28,375 --> 00:14:29,297
Why don't you yell at the

327
00:14:29,298 --> 00:14:30,808
people who melt the gold down.

328
00:14:31,188 --> 00:14:32,827
The Hindu saying is who ever

329
00:14:32,828 --> 00:14:34,283
smelt it dealt it.

330
00:14:36,539 --> 00:14:38,228
No who ever denied it supplied it.

331
00:14:38,301 --> 00:14:39,416
You got it all wrong.

332
00:14:39,785 --> 00:14:41,252
The jewelry that those shopping

333
00:14:41,253 --> 00:14:43,128
networks sell don't come from us.

334
00:14:43,809 --> 00:14:44,965
It's made in India

335
00:14:45,502 --> 00:14:47,168
where the Hindu rhymes come from.

336
00:14:47,517 --> 00:14:48,864
What you saying, Gustov?

337
00:14:49,323 --> 00:14:50,449
Do you mean --

338
00:14:50,689 --> 00:14:52,660
That's right whoever made

339
00:14:52,661 --> 00:14:54,303
the rhyme did the crime.

340
00:15:00,367 --> 00:15:01,038
Hello.

341
00:15:01,732 --> 00:15:02,690
Oh, welcome.

342
00:15:03,110 --> 00:15:04,919
Welcome to discounted jewelry store.

343
00:15:05,501 --> 00:15:06,218
Yeah, listen.

344
00:15:06,274 --> 00:15:07,536
I'm running a resale business.

345
00:15:07,766 --> 00:15:08,877
I can't get enough of peoples

346
00:15:08,878 --> 00:15:10,519
unwanted crappy jewelry to keep up.

347
00:15:10,520 --> 00:15:11,611
I want some of yours.

348
00:15:12,092 --> 00:15:14,752
You are so clever.

349
00:15:15,261 --> 00:15:15,976
I get that.

350
00:15:16,163 --> 00:15:19,877
I need gold necklaces, diamond
bracelets and emerald earrings.

351
00:15:20,342 --> 00:15:21,835
What you like?

352
00:15:22,291 --> 00:15:23,906
That ring. There.

353
00:15:24,136 --> 00:15:25,458
So good choice.

354
00:15:26,062 --> 00:15:27,341
It's beautiful.

355
00:15:27,342 --> 00:15:28,436
It's beautiful.

356
00:15:30,279 --> 00:15:32,228
And maybe that one for 300.

357
00:15:32,261 --> 00:15:33,460
That's best one.

358
00:15:33,473 --> 00:15:34,632
You so clever.

359
00:15:34,957 --> 00:15:36,786
You take advantage of my low prices.

360
00:15:37,043 --> 00:15:39,309
How about that bracelet for $995.

361
00:15:39,589 --> 00:15:41,982
You got good eye. You so clever.

362
00:15:42,084 --> 00:15:45,398
You like Asian lady.

363
00:15:48,727 --> 00:15:49,426
What you say.

364
00:15:49,574 --> 00:15:54,183
I know you say there nice
Asian lady I think I go

365
00:15:54,184 --> 00:15:57,310
her you Asian lady you.

366
00:15:57,555 --> 00:16:00,033
How much do you pay for this stuff?

367
00:16:00,075 --> 00:16:02,809
I pay thousands.
You come here and fuck me.

368
00:16:03,083 --> 00:16:05,221
Quit the act. I am not fucking you
and you know it.

369
00:16:05,341 --> 00:16:09,118
No, no, you fuck me.
No, no you fuck me. Fuck you.

370
00:16:25,156 --> 00:16:26,412
Oh, sorry. Sorry.

371
00:16:38,490 --> 00:16:40,503
You see I'm looking to
cut out the middle man.

372
00:16:40,767 --> 00:16:43,338
I want to buy my jewelry direct from you.

373
00:16:43,961 --> 00:16:45,022
Motherfucker.

374
00:16:45,393 --> 00:16:47,328
You should be ashamed of
the people in America

375
00:16:47,329 --> 00:16:48,491
you're exploiting.

376
00:16:48,937 --> 00:16:50,067
How dare you take advantage of

377
00:16:50,068 --> 00:16:51,150
those less fortunate.

378
00:16:51,254 --> 00:16:53,237
You dirty double crossing ass holes.

379
00:16:53,571 --> 00:16:54,995
You try to cut me out..

380
00:16:55,106 --> 00:16:58,149
You stole my formula and tried to fuck me.
Butters.

381
00:16:58,269 --> 00:16:59,244
Sorry.

382
00:17:00,343 --> 00:17:02,420
You tried to fuck me out of your business--

383
00:17:02,673 --> 00:17:05,282
we're not fucking you.
They're fucking Stan's grandpa.

384
00:17:05,416 --> 00:17:07,452
No they're getting fucked
from an Asian lady.

385
00:17:07,684 --> 00:17:09,418
Someone is at the head of this.

386
00:17:09,432 --> 00:17:10,584
Someone has to pay.

387
00:17:10,968 --> 00:17:13,219
I wand God damn retribution for

388
00:17:13,220 --> 00:17:14,599
my God damn grandpa.

389
00:17:16,657 --> 00:17:19,018
No, not a diamond and gold necklace!

390
00:17:23,751 --> 00:17:24,596
What's he doing?

391
00:19:14,549 --> 00:19:15,761
Oh, ya.

392
00:19:16,431 --> 00:19:18,095
Actually this might kind of work.

393
00:19:21,705 --> 00:19:23,118
So, then we went to India

394
00:19:23,119 --> 00:19:24,454
which is pretty cool, I guess.

395
00:19:24,525 --> 00:19:25,444
Never been there before.

396
00:19:25,605 --> 00:19:27,204
We learned that who ever smelt

397
00:19:27,205 --> 00:19:29,073
it denied it and rhymed it.

398
00:19:29,310 --> 00:19:31,296
Sounds like a fun weekend.

399
00:19:31,763 --> 00:19:33,765
I guess . So grandpa, I

400
00:19:34,038 --> 00:19:35,513
I wanted to give you something.

401
00:19:36,049 --> 00:19:37,223
For me?

402
00:19:39,306 --> 00:19:41,405
Oh, my God.

403
00:19:42,143 --> 00:19:43,026
There she is.

404
00:19:43,710 --> 00:19:45,105
Ole Patches.

405
00:19:45,756 --> 00:19:47,942
There is that slobbering happy face.

406
00:19:48,786 --> 00:19:49,757
Thank you, Billy.

407
00:19:50,551 --> 00:19:51,760
That means a lot.

408
00:19:54,028 --> 00:19:57,017
Billy, that, that bolo tie

409
00:19:57,018 --> 00:19:57,810
you're wearing.

410
00:19:58,103 --> 00:19:58,742
Ya.

411
00:19:58,927 --> 00:20:00,000
I don't know where you got

412
00:20:00,001 --> 00:20:02,451
that but it's gay as fuck.

413
00:20:03,148 --> 00:20:05,000
Cool, I won't wear it anymore.

414
00:20:05,402 --> 00:20:06,672
That's a good idea.

415
00:20:10,053 --> 00:20:11,351
Folks, these are not your

416
00:20:11,352 --> 00:20:13,797
average earrings.

417
00:20:13,798 --> 00:20:16,154
These are 18-carat gold.

418
00:20:17,164 --> 00:20:19,171
We got -- we have a buyer on the line.

419
00:20:19,205 --> 00:20:19,877
Hello.

420
00:20:21,273 --> 00:20:22,515
What are you waiting for,

421
00:20:22,918 --> 00:20:24,027
kill yourself.

422
00:20:24,670 --> 00:20:26,887
Alright, God damn it another comedian.

423
00:20:27,795 --> 00:20:28,880
Ever since that little kid

424
00:20:28,881 --> 00:20:30,614
called up everyone wants to call

425
00:20:30,615 --> 00:20:32,299
and tell me to kill myself.

426
00:20:32,896 --> 00:20:33,808
He was right.

427
00:20:33,877 --> 00:20:34,582
Do it.

428
00:20:34,670 --> 00:20:36,702
Folks, this is an 800 number.

429
00:20:36,824 --> 00:20:39,339
Every time you call and tell me

430
00:20:39,340 --> 00:20:41,711
to kill myself it's costing us

431
00:20:42,469 --> 00:20:45,597
$2.36.

432
00:20:46,049 --> 00:20:49,138
So now how about a caller
that wants to buy jewelry.

433
00:20:49,391 --> 00:20:50,660
Yes, hello, sir?

434
00:20:51,112 --> 00:20:53,179
You're too scared to do it, aren't.

435
00:20:53,645 --> 00:20:55,102
You don't have the balls.

436
00:20:55,472 --> 00:20:57,424
God damn it I'm not scared to do it.

437
00:20:57,602 --> 00:20:58,894
You're scared.

438
00:20:59,370 --> 00:21:01,667
You, you got lady balls.

439
00:21:05,811 --> 00:21:06,665
Hello.

440
00:21:07,156 --> 00:21:09,948
I'm calling about the paradot earnings.

441
00:21:10,149 --> 00:21:11,164
Yes, ma'am.

442
00:21:11,402 --> 00:21:13,381
They would look good on your dead body.

443
00:21:13,391 --> 00:21:15,030
Kill yourself.

444
00:21:15,423 --> 00:21:16,667
Alright that's that.

445
00:21:16,884 --> 00:21:20,895
That's the straw that
broke the camel's back.

446
00:21:21,568 --> 00:21:22,763
I have a gun right here.

447
00:21:22,764 --> 00:21:23,936
What do you think about that.

448
00:21:25,235 --> 00:21:26,711
Put it against your temple

449
00:21:26,712 --> 00:21:27,815
and pull the trigger.

450
00:21:30,484 --> 00:21:41,496
<font color="#ec14bd">Sync & corrections by honeybunny</font>
<font color="#ec14bd">www.addic7ed.com</font>

9999
00:00:0,500 --> 00:00:2,00
<font color="#ffff00" size=14>www.tvsubtitles.net</font>
